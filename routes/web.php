<?php

/**
 * Authentication
 */

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', [
    'as' => 'auth.logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

// Allow registration routes only if registration is enabled.
if (settings('reg_enabled')) {
    Route::get('register', 'Auth\AuthController@getRegister');
    Route::post('register', 'Auth\AuthController@postRegister');
    Route::get('register/confirmation/{token}', [
        'as' => 'register.confirm-email',
        'uses' => 'Auth\AuthController@confirmEmail'
    ]);
}

// Register password reset routes only if it is enabled inside website settings.
if (settings('forgot_password')) {
    Route::get('password/remind', 'Auth\PasswordController@forgotPassword');
    Route::post('password/remind', 'Auth\PasswordController@sendPasswordReminder');
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');
}

/**
 * Two-Factor Authentication
 */
if (settings('2fa.enabled')) {
    Route::get('auth/two-factor-authentication', [
        'as' => 'auth.token',
        'uses' => 'Auth\AuthController@getToken'
    ]);

    Route::post('auth/two-factor-authentication', [
        'as' => 'auth.token.validate',
        'uses' => 'Auth\AuthController@postToken'
    ]);
}

/**
 * Social Login
 */
Route::get('auth/{provider}/login', [
    'as' => 'social.login',
    'uses' => 'Auth\SocialAuthController@redirectToProvider',
    'middleware' => 'social.login'
]);

Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');

Route::get('auth/twitter/email', 'Auth\SocialAuthController@getTwitterEmail');
Route::post('auth/twitter/email', 'Auth\SocialAuthController@postTwitterEmail');

Route::group(['middleware' => 'auth'], function () {

    /**
     * Dashboard
     */

    Route::get('/', [
        'as' => 'dashboard',
        'uses' => 'DashboardController@index'
    ]);

    Route::get('timeline', 'TimelineController@index')->name('timeline');
    Route::get('ajax_timeline', 'TimelineController@ajax_timeline')->name('ajax_timeline');

    Route::get('client_projects', 'ClientController@index')->name('client_projects');

    Route::get('projects', 'ProjectController@index')->name('projects');

    Route::get('add_service', 'ServiceController@addService')->name('add_service');

    Route::get('mail', 'UserMailController@index')->name('mail');
    Route::post('mail', 'UserMailController@send');

    Route::get('mail_with_file', 'UserMailDocumentController@index')->name('mail_with_file');
    Route::post('mail_with_file', 'UserMailDocumentController@send');

    Route::get('sendfile', 'AdminMailController@index')->name('sendfile');
    Route::post('sendfile', 'AdminMailController@send');

    Route::get('add_group', 'GroupController@index')->name('add_group');
    Route::post('add_group', 'GroupController@store');
    Route::post('add_group_type', 'GroupController@add_group_type')->name('add_group_type');

    Route::get('add_comment_company', 'CompanyCommentController@index')->name('add_comment_company');
    Route::post('add_comment_company', 'CompanyCommentController@store');

    Route::get('phone_call', 'PhoneCallController@index')->name('phone_call');
    Route::post('phone_call', 'PhoneCallController@store');

    Route::get('attach_files', 'AttachFileController@index')->name('attach_files');
    Route::post('attach_files', 'AttachFileController@store');

    Route::get('client/edit', 'EditController@index')->name('edit');

    Route::post('save_service', 'ServiceController@saveService')->name('save_service');

    Route::get('edit_service_stage', 'StageController@editStage')->name('edit_service_stage');

    Route::post('save_service_stage', 'StageController@saveStage')->name('save_service_stage');

    Route::get('select_deliverables', 'DeliverableController@addServiceDeliverable')->name('select_deliverables');

    Route::post('save_deliverables', 'DeliverableController@saveServiceDeliverable')->name('save_deliverables');

    Route::post('create_deliverables', 'DeliverableController@createServiceDeliverable')->name('create_deliverables');

    Route::get('edit_timeline_of_service', 'TimelineController@editTimeline')->name('edit_timeline_of_service');

    Route::post('save_timeline_of_service', 'TimelineController@saveTimeline')->name('save_timeline_of_service');

    Route::get('edit_financial_cost', 'FinanceController@editFinancialCost')->name('edit_financial_cost');

    Route::post('save_financial_cost', 'FinanceController@saveFinancialCost')->name('save_financial_cost');

    Route::get('edit_payment_terms', 'FinanceController@editPaymentTerms')->name('edit_payment_terms');

    Route::post('save_payment_terms', 'FinanceController@savePaymentTerms')->name('save_payment_terms');

    Route::get('follow', 'FollowController@index')->name('follow');

    Route::post('sent_follow', 'FollowController@sent')->name('sent_follow');

    Route::get('info/{type}', 'InfoController@index')->where('type', 'companies|contacts|group_contact|group_company|all_group|vandour|gt')->name('info');



    // Added by freelancer
    Route::get('contacts/create', 'ContactsController@create'); // creating contact
    Route::post('contacts/store', 'ContactsController@store');  // store contact_id, type, value, default_no in contact_detail table
    Route::get('contacts/{id}/edit', 'ContactsController@edit');// edit type, value,default_no 
    Route::post('contacts/{id}', 'ContactsController@update');  // store and redirect to info

    Route::get('companies/create', 'CompaniesController@create');
    Route::post('companies/store', 'CompaniesController@store');
    Route::get('companies/{id}/edit', 'CompaniesController@edit');
    Route::post('companies/{id}', 'CompaniesController@update');



    Route::post('load_contacts/{id}', 'InfoController@loadContacts')->where('id', '[0-9]+')->name('load_contacts');

    Route::post('load_contact_details/{id}', 'InfoController@loadContactDetails')->where('id', '[0-9]+')->name('load_contact_details');

    Route::post('add_watch/{id}', 'InfoController@addWatch')->where('id', '[0-9]+')->name('add_watch');

    Route::get('add_comment', 'FollowController@addComment')->name('add_comment');

    Route::post('save_comment', 'FollowController@saveComment')->name('save_comment');



    Route::get('task/create', 'TaskController@create')->name('task.create');

    Route::post('task', 'TaskController@store')->name('task.store');

    Route::get('task', 'TaskController@index')->name('task.index');

    Route::get('task_details', 'TaskController@show')->where('task', '[0-9]+')
        ->name('task.show');

    Route::post('task_completed', 'TaskController@completed')->name('task.completed');

    Route::get('add_comment_for_task', 'TaskController@addComment')->name('add_comment_for_task.create');

    Route::post('add_comment_for_task', 'TaskController@createComment')->name('add_comment_for_task.store');

    Route::post('task/delete', 'TaskController@delete')->name('task.delete');

    Route::post('task/delete', 'TaskController@delete')->name('task.delete');

    Route::get('task/edit', 'TaskController@edit')->name('task.edit');

    Route::post('task/save', 'TaskController@save')->name('task.save');




    /**
     * User Profile
     */

    Route::get('profile', [
        'as' => 'profile',
        'uses' => 'ProfileController@index'
    ]);

    Route::get('profile/activity', [
        'as' => 'profile.activity',
        'uses' => 'ProfileController@activity'
    ]);

    Route::put('profile/details/update', [
        'as' => 'profile.update.details',
        'uses' => 'ProfileController@updateDetails'
    ]);

    Route::post('profile/avatar/update', [
        'as' => 'profile.update.avatar',
        'uses' => 'ProfileController@updateAvatar'
    ]);

    Route::post('profile/avatar/update/external', [
        'as' => 'profile.update.avatar-external',
        'uses' => 'ProfileController@updateAvatarExternal'
    ]);

    Route::put('profile/login-details/update', [
        'as' => 'profile.update.login-details',
        'uses' => 'ProfileController@updateLoginDetails'
    ]);

    Route::post('profile/two-factor/enable', [
        'as' => 'profile.two-factor.enable',
        'uses' => 'ProfileController@enableTwoFactorAuth'
    ]);

    Route::post('profile/two-factor/disable', [
        'as' => 'profile.two-factor.disable',
        'uses' => 'ProfileController@disableTwoFactorAuth'
    ]);

    Route::get('profile/sessions', [
        'as' => 'profile.sessions',
        'uses' => 'ProfileController@sessions'
    ]);

    Route::delete('profile/sessions/{session}/invalidate', [
        'as' => 'profile.sessions.invalidate',
        'uses' => 'ProfileController@invalidateSession'
    ]);

    /**
     * User Management
     */
    Route::get('user', [
        'as' => 'user.list',
        'uses' => 'UsersController@index'
    ]);

    Route::get('user/create', [
        'as' => 'user.create',
        'uses' => 'UsersController@create'
    ]);

    Route::post('user/create', [
        'as' => 'user.store',
        'uses' => 'UsersController@store'
    ]);

    Route::get('user/{user}/show', [
        'as' => 'user.show',
        'uses' => 'UsersController@view'
    ]);

    Route::get('user/{user}/edit', [
        'as' => 'user.edit',
        'uses' => 'UsersController@edit'
    ]);

    Route::put('user/{user}/update/details', [
        'as' => 'user.update.details',
        'uses' => 'UsersController@updateDetails'
    ]);

    Route::put('user/{user}/update/login-details', [
        'as' => 'user.update.login-details',
        'uses' => 'UsersController@updateLoginDetails'
    ]);

    Route::delete('user/{user}/delete', [
        'as' => 'user.delete',
        'uses' => 'UsersController@delete'
    ]);

    Route::post('user/{user}/update/avatar', [
        'as' => 'user.update.avatar',
        'uses' => 'UsersController@updateAvatar'
    ]);

    Route::post('user/{user}/update/avatar/external', [
        'as' => 'user.update.avatar.external',
        'uses' => 'UsersController@updateAvatarExternal'
    ]);

    Route::get('user/{user}/sessions', [
        'as' => 'user.sessions',
        'uses' => 'UsersController@sessions'
    ]);

    Route::delete('user/{user}/sessions/{session}/invalidate', [
        'as' => 'user.sessions.invalidate',
        'uses' => 'UsersController@invalidateSession'
    ]);

    Route::post('user/{user}/two-factor/enable', [
        'as' => 'user.two-factor.enable',
        'uses' => 'UsersController@enableTwoFactorAuth'
    ]);

    Route::post('user/{user}/two-factor/disable', [
        'as' => 'user.two-factor.disable',
        'uses' => 'UsersController@disableTwoFactorAuth'
    ]);

    /**
     * Roles & Permissions
     */

    Route::get('role', [
        'as' => 'role.index',
        'uses' => 'RolesController@index'
    ]);

    Route::get('role/create', [
        'as' => 'role.create',
        'uses' => 'RolesController@create'
    ]);

    Route::post('role/store', [
        'as' => 'role.store',
        'uses' => 'RolesController@store'
    ]);

    Route::get('role/{role}/edit', [
        'as' => 'role.edit',
        'uses' => 'RolesController@edit'
    ]);

    Route::put('role/{role}/update', [
        'as' => 'role.update',
        'uses' => 'RolesController@update'
    ]);

    Route::delete('role/{role}/delete', [
        'as' => 'role.delete',
        'uses' => 'RolesController@delete'
    ]);


    Route::post('permission/save', [
        'as' => 'permission.save',
        'uses' => 'PermissionsController@saveRolePermissions'
    ]);

    Route::resource('permission', 'PermissionsController');

    /**
     * Settings
     */

    Route::get('settings', [
        'as' => 'settings.general',
        'uses' => 'SettingsController@general',
        'middleware' => 'permission:settings.general'
    ]);

    Route::post('settings/general', [
        'as' => 'settings.general.update',
        'uses' => 'SettingsController@update',
        'middleware' => 'permission:settings.general'
    ]);

    Route::get('settings/auth', [
        'as' => 'settings.auth',
        'uses' => 'SettingsController@auth',
        'middleware' => 'permission:settings.auth'
    ]);

    Route::post('settings/auth', [
        'as' => 'settings.auth.update',
        'uses' => 'SettingsController@update',
        'middleware' => 'permission:settings.auth'
    ]);

// Only allow managing 2FA if AUTHY_KEY is defined inside .env file
    if (env('AUTHY_KEY')) {
        Route::post('settings/auth/2fa/enable', [
            'as' => 'settings.auth.2fa.enable',
            'uses' => 'SettingsController@enableTwoFactor',
            'middleware' => 'permission:settings.auth'
        ]);

        Route::post('settings/auth/2fa/disable', [
            'as' => 'settings.auth.2fa.disable',
            'uses' => 'SettingsController@disableTwoFactor',
            'middleware' => 'permission:settings.auth'
        ]);
    }

    Route::post('settings/auth/registration/captcha/enable', [
        'as' => 'settings.registration.captcha.enable',
        'uses' => 'SettingsController@enableCaptcha',
        'middleware' => 'permission:settings.auth'
    ]);

    Route::post('settings/auth/registration/captcha/disable', [
        'as' => 'settings.registration.captcha.disable',
        'uses' => 'SettingsController@disableCaptcha',
        'middleware' => 'permission:settings.auth'
    ]);

    Route::get('settings/notifications', [
        'as' => 'settings.notifications',
        'uses' => 'SettingsController@notifications',
        'middleware' => 'permission:settings.notifications'
    ]);

    Route::post('settings/notifications', [
        'as' => 'settings.notifications.update',
        'uses' => 'SettingsController@update',
        'middleware' => 'permission:settings.notifications'
    ]);

    /**
     * Activity Log
     */

    Route::get('activity', [
        'as' => 'activity.index',
        'uses' => 'ActivityController@index'
    ]);

    Route::get('activity/user/{user}/log', [
        'as' => 'activity.user',
        'uses' => 'ActivityController@userActivity'
    ]);

});


/**
 * Installation
 */

$router->get('install', [
    'as' => 'install.start',
    'uses' => 'InstallController@index'
]);

$router->get('install/requirements', [
    'as' => 'install.requirements',
    'uses' => 'InstallController@requirements'
]);

$router->get('install/permissions', [
    'as' => 'install.permissions',
    'uses' => 'InstallController@permissions'
]);

$router->get('install/database', [
    'as' => 'install.database',
    'uses' => 'InstallController@databaseInfo'
]);

$router->get('install/start-installation', [
    'as' => 'install.installation',
    'uses' => 'InstallController@installation'
]);

$router->post('install/start-installation', [
    'as' => 'install.installation',
    'uses' => 'InstallController@installation'
]);

$router->post('install/install-app', [
    'as' => 'install.install',
    'uses' => 'InstallController@install'
]);

$router->get('install/complete', [
    'as' => 'install.complete',
    'uses' => 'InstallController@complete'
]);

$router->get('install/error', [
    'as' => 'install.error',
    'uses' => 'InstallController@error'
]);