@extends('layouts.app')

@section('title') Add comment@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Add phone call</h1>
            <h3>{{ $title }}</h3>
        </div>
    </div>
    @if(session('status'))
       <div class="alert alert-success">
           {{ session('status') }}
       </div>
    @endif
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('phone_call') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="comment">Contact Name:</label>
                    <div>
                        <input name="contact_name" class="form-control" id="comment" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment">Contact Title:</label>
                    <div>
                        <input name="contact_title" class="form-control" id="comment" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment">Call Minutes:</label>
                    <div>
                        <textarea name="call_minutes" class="form-control" id="comment" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Add comment" />
                </div>
                <input type="hidden" name="client_id" value="{{ request()->client_id }}">
                <input type="hidden" name="type" value="{{ request()->type }}">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <table class="table table-bordered table-condensed table_clients">
                <thead>
                <tr id="client_table_head">
                    <th>Contact Name</th>
                    <th>Contact Title</th>
                    <th>Call Minutes</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($phone_calls as $phone_call)
                    <tr>
                        <td>{{ $phone_call->contact_name }}</td>
                        <td>{{ $phone_call->contact_title }}</td>
                        <td>{{ $phone_call->call_minutes }}</td>
                        <td>{{ $phone_call->created_at->format('H:i d.m.Y') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection