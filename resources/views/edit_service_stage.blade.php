@extends('layouts.app')

@section('title') Edit service stage (#{{ app('request')->input('service_id') }})@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Edit Service Stage (#{{ app('request')->input('service_id') }})</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('save_service_stage') }}" method="POST">
                {{ csrf_field() }}
                <?php $errors = session('errors'); ?>
                <div class="form-group @if (isset($errors['stage'])) has-error has-danger @endif">
                    <label for="stage">Stage:</label>
                    <div>
                        <?php
                            $stages = [
                                1 => 'Proposal Requested',
                                2 => 'Submit proposal',
                                3 => 'Revise proposal',
                                4 => 'Accept proposal',
                                5 => 'Final customer approval',
                                6 => 'Project delivery',
                                7 => 'Close and archive',
                            ];
                        ?>
                        <select name="stage" id="stage" class="form-control">
                            @if ($stages)
                                <?php $stage = (app('request')->input('stage')) ? : $project->stage ?>
                                @foreach ($stages as $stageKey => $stageValue)
                                    <option @if ($stageKey == $stage) selected @endif value="{{ $stageKey }}">{{ $stageValue }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors && isset($errors['stage']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['stage'] as $errorStage)
                                        <li>{{ $errorStage }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection