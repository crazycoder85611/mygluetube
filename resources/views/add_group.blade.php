@extends('layouts.app')

@section('title') Mail @endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            <h1>Add group</h1>
            <h3>
            {{ $title }}
            </h3>
        </div>
    </div>
   <div class="row">
       <div class="col-md-6">

           @if(session('status_add_in_group'))
               <div class="alert alert-success">
                   {!! session('status_add_in_group') !!}
               </div>
           @endif

           @if(session('status_add_in_group_error'))
               <div class="alert alert-danger">
                   {!! session('status_add_in_group_error') !!}
               </div>
           @endif
           <form style="margin-top: 20px;" method="POST" action="{{ route('add_group_type') }}">
               {{ csrf_field() }}
               <div class="form-group">
                   @if (request()->type == 'contact')
                       <label class="radio-inline"><input type="radio" name="type" value="contact" checked>Contact</label>
                   @else
                       <label class="radio-inline"><input type="radio" name="type" value="client" checked>Client</label>
                   @endif
               </div>
               <div class="form-group">
                   <label>Groups</label>
                   <select name="group_id" class="form-control" required>
                       @foreach($groups as $group)
                           <option value="{{ $group->id }}">{{ $group->name }}</option>
                       @endforeach
                   </select>
               </div>
               @if (request()->type == 'contact')
                   <div class="form-group contacts">
                       <label>Contacts</label>
                       <select name="contact_id" class="form-control" required>
                           @foreach($contacts as $contact)
                               <option value="{{ $contact->contact_id }}">{{ $contact->name }}</option>
                           @endforeach
                       </select>
                   </div>
               @endif
               <div class="form-group">
                   <input type="submit" class="btn btn-info" value="Submit">
               </div>
               <input type="hidden" name="client_id" value="{{ request()->client_id }}">
           </form>
       </div>
       <div class="col-md-6">

           @if(session('status_add_group'))
               <div class="alert alert-success">
                   {!! session('status_add_group') !!}
               </div>
           @endif

           @if(session('status_add_group_error'))
               <div class="alert alert-danger">
                   {!! session('status_add_group_error') !!}
               </div>
           @endif
           <form style="margin-top: 20px;" method="POST">
               {{ csrf_field() }}
               <div class="form-group">
                   <label>Name</label>
                   <input type="text" class="form-control" name="name" required>
               </div>
               <div class="form-group">
                   <input type="submit" class="btn btn-info" value="Add new group">
               </div>
           </form>
       </div>
   </div>
@endsection

@section('scripts')
    <script>
        @if (request()->type == 'contact')
            $('select[name="group_id"] option[value="4"]').hide();
        @else
            $('select[name="group_id"] option[value="3"]').hide();
        @endif
        $('[name="type"]').on('change', function() {
            if ($(this).val() != 'client') {
                $('.contacts').fadeIn();
                $('select[name="group_id"] option[value="3"]').show();
                $('select[name="group_id"] option[value="4"]').hide();
            } else {
                $('.contacts').fadeOut();
                $('select[name="group_id"] option[value="3"]').hide();
                $('select[name="group_id"] option[value="4"]').show();
            }
        });
    </script>
@endsection