@extends('layouts.app')

@section('title') Mail @endsection

@section('content')
   <div class="row">
       <div class="col-md-6">
           @include('partials/messages')
           <form style="margin-top: 20px;" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="form-group">
                   <label>Title</label>
                   <input type="text" class="form-control" name="title" required>
               </div>
               <div class="form-group">
                   <label>Attach file</label>
                   <input type="file" class="form-control" name="file" required>
               </div>
               <div class="form-group">
                   <input type="submit" class="btn btn-info" value="Submit">
               </div>
           </form>
       </div>
   </div>
   <div class="row">
       <div class="col-lg-10">
           <table class="table table-bordered table-condensed table_clients">
               <thead>
                    <tr id="client_table_head">
                        <th>Title</th>
                        <th>File name</th>
                        <th>Upload date</th>
                    </tr>
               </thead>
               <tbody>
                    @foreach($user_mail_documents as $user_mail_document)
                        <tr>
                            <td>{{ $user_mail_document->title }}</td>
                            <td><a href="{{ $user_mail_document->path }}" target="_blank">{{ $user_mail_document->path }}</a></td>
                            <td>{{ $user_mail_document->created_at->format('H:i d.m.Y') }}</td>
                        </tr>
                    @endforeach
               </tbody>
           </table>
       </div>
   </div>
@endsection