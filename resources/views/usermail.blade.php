@extends('layouts.app')

@section('title') Mail @endsection

@section('content')
   <div class="row">
       <div class="col-md-6">
           @include('partials/messages')
           <form style="margin-top: 20px;" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="form-group">
                   <label>Type</label>
                   <select name="type" class="form-control" required>
                       <option value="Reimbursement Request">Reimbursement Request</option>
                       <option value="Cash Advance">Cash Advance</option>
                       <option value="Id Letter Request">Id Letter Request</option>
                   </select>
               </div>
               <div class="form-group">
                   <label>Title</label>
                   <input type="text" class="form-control" name="title" required>
               </div>
               <div class="form-group">
                   <label>Description</label>
                   <textarea class="form-control" name="description" required></textarea>
               </div>
               <div class="form-group">
                   <label>Attach file</label>
                   <input type="file" class="form-control" name="file">
               </div>
               <div class="form-group">
                   <input type="submit" class="btn btn-info" value="Submit">
               </div>
           </form>
       </div>
   </div>
@endsection