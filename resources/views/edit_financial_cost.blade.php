@extends('layouts.app')

@section('title') Edit financial cost (#{{ app('request')->input('service_id') }})@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Edit financial cost (#{{ app('request')->input('service_id') }})</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('save_financial_cost') }}" method="POST">
                {{ csrf_field() }}
                <?php $errors = session('errors'); ?>
                <div class="form-group @if (isset($errors['estimate_cost'])) has-error has-danger @endif">
                    <label for="estimate_cost">Financial cost:</label>
                    <div>
                        <input type="text" name="estimate_cost" class="form-control" id="estimate_cost" value="{{ $project->estimate_cost ? : '' }}" />
                        @if ($errors && isset($errors['estimate_cost']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['estimate_cost'] as $errorsEstimateCost)
                                        <li>{{ $errorsEstimateCost }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection