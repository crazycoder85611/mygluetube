@extends('layouts.app')

@section('title') Clients @endsection

@section('content')
    <div class="row" style="margin: 10px 0;">
        <div class="col-lg-10 col-lg-offset-1">
            <form action="" method="GET" class="stage_form">
                <div class="form-group">
                    <label>Stage:</label>
                    <select name="stage">
                        <option value=""{{ request('stage') == null ? ' selected' : '' }}>All</option>
                        <option value="1"{{ request('stage') == 1 ? ' selected' : '' }}>1</option>
                        <option value="2"{{ request('stage') == 2 ? ' selected' : '' }}>2</option>
                        <option value="3"{{ request('stage') == 3 ? ' selected' : '' }}>3</option>
                        <option value="4"{{ request('stage') == 4 ? ' selected' : '' }}>4</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <form action="#" method="POST">
                <table class="table table-bordered table-condensed table_clients">
                    <thead>
                        <tr id="client_table_head">
                            <th style="width: 5%"></th>
                            <th style="width: 10%">ID
                                @if (app('request')->input('sort') != 'id' || (app('request')->input('sort') == 'id' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'id']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'id', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 20%">Service
                                @if (app('request')->input('sort') != 'title' || (app('request')->input('sort') == 'title' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'title']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'title', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 20%">Timeline
                                @if (app('request')->input('sort') != 'project_duration' || (app('request')->input('sort') == 'project_duration' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'project_duration']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'project_duration', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 10%">Value
                                @if (app('request')->input('sort') != 'estimate_cost' || (app('request')->input('sort') == 'estimate_cost' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'estimate_cost']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'estimate_cost', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 10%">Due Date
                                @if (app('request')->input('sort') != 'final_delivery_date' || (app('request')->input('sort') == 'final_delivery_date' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'final_delivery_date']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('projects', ['stage' => app('request')->input('stage'), 'id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'final_delivery_date', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 15%">
                                Company name
                            </th>
                            <th style="width: 10%">
                               Comment
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($projects)
                            @foreach ($projects as $project)
                                <tr>
                                    <td><input class="project_checkbox" type="checkbox" value="{{ $project->id }}"></td>
                                    <td>{{ $project->id }}</td>
                                    <td><a href="{{ route('client_projects', ['id' => $project->client_id]) }}" target="_blank">{{ $project->title }}</a></td>
                                    <td>{{ ($project->project_duration) ? : '' }}</td>
                                    <td>{{ ($project->estimate_cost) ? : '' }}</td>
                                    <td>{{ ($project->final_delivery_date) ? date('Y-m-d', $project->final_delivery_date) : '' }}</td>
                                    <td>{{ $project->company->company_name }}</td>
                                    <td>{{ isset($comments[$project->id]) ? $comments[$project->id] : '' }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    @if ($projects->lastPage() > 1)
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <ul class="pagination pagination-sm">
                    <?php
                        $showOnTheSides = 2;

                        $firstPage = (($projects->currentPage() - $showOnTheSides) > 0) ? $projects->currentPage() - $showOnTheSides : 1;

                        $lastPage = (($projects->currentPage() + $showOnTheSides) > $projects->lastPage()) ?
                            $projects->lastPage() :
                            $projects->currentPage() + $showOnTheSides;

                        function getUrlPage($page)
                        {
                            return route('client_projects', [
                                'id'        => app('request')->input('id'),
                                'sort'      => app('request')->input('sort'),
                                'sort_type' => app('request')->input('sort_type'),
                                'page'      => ($page != 1) ? $page : null,
                            ]);
                        }
                    ?>
                    <li><a href="{{ getUrlPage(1) }}"><<</a></li>
                        @for ($i = $firstPage; $i <= $lastPage; $i++)
                            <li @if ($i == $projects->currentPage()) class="active" @endif ><a href="{{ getUrlPage($i) }}">{{ $i }}</a></li>
                        @endfor
                    <li><a href="{{ getUrlPage($projects->lastPage()) }}">>></a></li>
                </ul>
            </div>
        </div>
    @endif
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('select[name="stage"]').on('change', function() {
                $('.stage_form').trigger('submit');
            });
        });
    </script>
@endsection