@extends('layouts.app')

@section('title') Tasks show @endsection

@section('body_id') id="info_page" @endsection

@section('js') <script src="{{ asset('js/info.js') }}"></script> @endsection

@section('content')
    @if ($task)
		<br/>
        <div class="row row-eq-height">
            <div class="info_block col-md-12">
                Title - {{ $task->entry_title }}
                <br />
                <br />
                Status - {{ $task->entry_status }}
                <br />
                <br />
                Date created - {{ $task->start_date }}
                <br />
                <br />
                Date end - {{ $task->start_date }}
                <br />
                <br />

                <form action="{{ route('task.completed') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $task->entry_id }}">
                    <div class="form-group">
                        <input type="submit" value="Mark completed" class="btn btn-primary">
                    </div>
                </form>

                <a class="btn btn-success" href="{{ route('add_comment_for_task.create', ['task_id' => app('request')->input('task_id')]) }}">Add comment</a>

                <br />
                <br />
                @if ($role == '3' || $role == '1')
                <form action="{{ route('task.delete') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="entry_id" value="{{ app('request')->input('task_id') }}">
                    <div class="form-group">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>

                <a class="btn btn-success" href="{{ route('task.edit', ['task_id' => $task->entry_id]) }}">Edit</a>

                @endif

                @if ($task->entriesComments)
                    <h3>Comments</h3>
                    @foreach ($task->entriesComments as $comment)
                        <div>{{ $comment->ec_comment }} ({{ $comment->ec_date }})</div>
                        <br />
                    @endforeach
                @endif
            </div>
        </div>
    @endif
@endsection