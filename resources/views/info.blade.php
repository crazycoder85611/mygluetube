@extends('layouts.app')

@section('title') Companies @endsection

@section('body_id') id="info_page" @endsection

@section('js') <script src="{{ asset('js/info.js') }}"></script> @endsection

@section('content')
    <div class="row row-eq-height">
        <div class="col-md-4 sidebar">
            <div class="row">
                <div class="sidebar_buttons clearfix">
                    <div class="pull-left">
					&nbsp;
                        <a href="{{ route('info', 'companies') }}" title="Companies" class="sidebar_button {{ ($type == 'companies') ? 'active' : '' }}">
                            <img src="/img/1.jpg">
                        </a>
                        <a href="{{ route('info', 'contacts') }}" title="Contacts" class="sidebar_button {{ ($type == 'contacts') ? 'active' : '' }}">
                            <img src="/img/2.jpg">
                        </a>
                        <a href="{{ route('info', 'vandour') }}" title="Contacts" class="sidebar_button {{ ($type == 'vandour') ? 'active' : '' }}">
                            <img src="/img/3.jpg">
                        </a>
                        <a href="{{ route('info', 'gt') }}" title="Contacts" class="sidebar_button {{ ($type == 'gt') ? 'active' : '' }}">
                            <img src="/img/4.jpg">
                        </a>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('info', 'group_contact') }}" title="Contacts" class="sidebar_button {{ ($type == 'group_contact') ? 'active' : '' }}">
                            <img src="/img/5.jpg">
                        </a>
                        <a href="{{ route('info', 'group_company') }}" title="Contacts" class="sidebar_button {{ ($type == 'group_company') ? 'active' : '' }}">
                            <img src="/img/6.jpg">
                        </a>
                        <a href="{{ route('info', 'all_group') }}" title="Contacts" class="sidebar_button {{ ($type == 'all_group') ? 'active' : '' }}">
                            <img src="/img/7.jpg">
                        </a>
                    </div>
                </div>
                @if ($data)
                    <ul>
                        @foreach ($data as $d)
                            <?php
                            if ($type == 'companies' || $type == 'group_company') {
                                $url = route('info', ['type' => $type, 'company_id' => $d->company_id]);
                                $id = $d->company_id;
                                $name = $d->company_name;
                            } else if ($type == 'all_group') {
                                $url = route('info', ['type' => 'all_group', 'group_id' => $d->id]);
                                $id = $d->id;
                                $name = $d->name;
                            }else {
                                $url = route('info', ['type' => $type, 'company_id' => $d->company_id, 'selectedContactID' => $d->contact_id]);

                                $id = $d->contact_id;
                                $name = $d->name;
                            }
                            $class = '';
                            if (($type == 'companies' || $type == 'group_company') and request()->company_id == $d->company_id) {
                                $class = 'highlighted';
                            }
                            if ($type == 'all_group' and request()->group_id == $d->id) {
                                $class = 'highlighted';
                            }
                            if (request()->selectedContactID == $id) {
                                $class = 'highlighted';
                            }
                            ?>
                            <li class="{{ $class }}">
                                <a href="{{ $url }}" data-id="{{ $id }}" data-type="{{ $type }}">{{ $name }}</a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <ul>
                        @if ($type == 'all_group')
                            No group found
                        @else
                            Group has no contacts
                        @endif
                    </ul>
                @endif
            </div>
        </div>
        <div class="info_block col-md-8">
            @if (isset($contacts))
                <div @if (isset($contact_details)) style="display: none"@endif class="details_show">
                    @if ($contacts->count())
                        <h1>Contacts</h1>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Job Title</th>
                                @if ($details)
                                    @foreach(current($details) as $nameDetail => $v)
                                        <th>{{ ucfirst($nameDetail) }}</th>
                                    @endforeach
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contacts as $contact)
                                <tr>
                                    <td><a class="" data-id="{{ $contact->contact_id }}" href="{{  route('info', ['type' => 'contacts', 'client_id' => $contact->contact_id]) }}">{{ $contact->name }}</a></td>
                                    <td>{{ $contact->job_title }}</td>
                                    @if ($details)
                                        @foreach($details[$contact->contact_id] as $detail)
                                            <td>{{ $detail }}</td>
                                        @endforeach
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <h3>Company has no contacts</h3>
                    @endif
                </div>
            @endif
            @if (isset($contact_details))
                <div id="contact_details_block">
                    @if ($title || $contact_details || $projects)
                        <h2>Customer information @if ($type == 'companies')<button class="btn btn-default back_contacts pull-right">Back</button>@endif </h2>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <td>
                                    Customer
                                    <div class="lead">{{ $title }}</div>
                                </td>
                                <td>
                                    @if ($contact_details)
                                        @foreach ($contact_details as $contactDetail)
                                            <div><strong>{{ ucfirst($contactDetail->type) }}: </strong>{{ $contactDetail->value }}</div>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <div class="row info_buttons">
                            <div class="col-md-12">
                                <div class="table">
                                    <div class="cell"><a href="{{ route('add_service', ['client_id' => app('request')->input('company_id')]) }}" class="btn btn-block btn-primary">START NEW PROJECT</a></div>
                                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">EDIT</a></div>
                                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">NEW CONTACT</a></div>
                                    <div class="cell"><a href="{{ route('add_group', ['client_id' => request()->company_id, 'type' => 'company']) }}" class="select_project btn btn-block btn-primary">GROUP ADD/REMOVE</a></div>
                                    <div class="cell"><a href="{{ route('add_watch', ['id' => app('request')->input('company_id'), 'type' => 'company']) }}" class="add_watch btn btn-block btn-primary">ADD TO WATCH LIST</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="row info_buttons">
                            <div class="col-md-12">
                                <div class="table">
                                    <div class="cell"><a href="{{ route('add_comment_company', ['client_id' => request()->company_id, 'type' => 'company']) }}" class="btn btn-block btn-primary">COMMENT</a></div>
                                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">NEW ORGANIZATION</a></div>
                                    <div class="cell"><a href="{{ route('phone_call', ['client_id' => request()->company_id, 'type' => 'company']) }}" class="select_project btn btn-block btn-primary">PHONE CALL</a></div>
                                    <div class="cell"><a href="{{ route('attach_files', ['client_id' => request()->company_id, 'type' => 'company']) }}" class="select_project btn btn-block btn-primary">ATTACH FILE</a></div>
                                </div>
                            </div>
                        </div>
                        <h2>Customer Finance</h2>
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                                <th>Invoice</th>
                                <th>Value</th>
                                <th>Due Date</th>
                                <th>Cycle</th>
                                <th>Status</th>
                                <th>Comment</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12,500</td>
                                    <td>22,000</td>
                                    <td>18,000</td>
                                    <td>G-5254</td>
                                    <td>13,600</td>
                                    <td>12 FEB 17</td>
                                    <td>Once</td>
                                    <td>D23JUL17</td>
                                    <td>Follow up call on THU</td>
                                </tr>
                                <tr>
                                    <td>12,500</td>
                                    <td>22,000</td>
                                    <td>18,000</td>
                                    <td>G-5254</td>
                                    <td>13,600</td>
                                    <td>12 FEB 17</td>
                                    <td>Once</td>
                                    <td>D23JUL17</td>
                                    <td>Follow up call on THU</td>
                                </tr>
                                <tr>
                                    <td>12,500</td>
                                    <td>22,000</td>
                                    <td>18,000</td>
                                    <td>G-5254</td>
                                    <td>13,600</td>
                                    <td>12 FEB 17</td>
                                    <td>Once</td>
                                    <td>D23JUL17</td>
                                    <td>Follow up call on THU</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row info_buttons">
                            <div class="col-md-12">
                                <div class="table">
                                    <div class="cell"><a href="" class="btn btn-block btn-primary disabled">ACCOUNT STATEMENT</a></div>
                                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">ISSUE INVOICE</a></div>
                                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">INVOICE DELIVERY</a></div>
                                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">COLLECTION CLAIM</a></div>
                                    <div class="cell"><a href="" class="add_watch btn btn-block btn-primary disabled">PAYMENT RECEIPT</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="row info_buttons">
                            <div class="col-md-12">
                                <div class="table">
                                    <div class="cell"><a href="" class="btn btn-block btn-primary disabled">3RD PARTY COST</a></div>
                                </div>
                            </div>
                        </div>
                        <h2>Customer Projects</h2>
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Project Title</th>
                                <th>Value</th>
                                <th>Balance</th>
                                <th>Next Action</th>
                                <th>Next Task</th>
                                <th>Due Date</th>
                                <th>Comment</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($projects->count())
                                @foreach ($projects as $project)
                                    <tr>
                                        <td></td>
                                        <td><a href="{{ route('client_projects', ['id' => $project->id]) }}">{{ $project->title }}</a></td>
                                        <td>{{ $project->estimate_cost }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $project->date_time_created ? date('Y-m-d', $project->date_time_created) : '' }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">No projects found</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div class="row info_buttons">
                            <div class="col-md-12">
                                <div class="table">
                                    <div class="cell"><a href="{{ route('add_service', ['client_id' => app('request')->input('company_id')]) }}" class="btn btn-block btn-primary">START NEW PROJECT</a></div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endif

            @if (isset($customer_contacts))
                <div id="contact_details_block">
                    <h2>Customer contacts</h2>
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Job Title</th>
                            <th>Email</th>
                            <th>Fav tel</th>
                            <th>Tel no</th>
                            <th>Comment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($customer_contacts)
                            @foreach ($customer_contacts as $customer_contact)
                                <tr class="{{ request()->selectedContactID == $customer_contact->contact_id ? 'highlighted' : '' }}">
                                    <td><a href="{{ route('info', ['type' => 'contacts', 'company_id' => request()->company_id, 'selectedContactID' => $customer_contact->contact_id]) }}">{{ $customer_contact->name }}</a></td>
                                    <td>{{ $customer_contact->job_title }}</td>
                                    <td>{{ $customer_contact->contact_details->where('type', 'email')->last() ? $customer_contact->contact_details->where('type', 'email')->last()->value : '' }}</td>
                                    <td>{{ $customer_contact->contact_details->where('type', 'telephone')->last() ? $customer_contact->contact_details->where('type', 'telephone')->last()->value : '' }}</td>
                                    <td>{{ $customer_contact->contact_details->where('type', 'mobile')->last() ? $customer_contact->contact_details->where('type', 'mobile')->last()->value : '' }}</td>
                                    <td>{{ $customer_contact->comments->last() ? $customer_contact->comments->last()->comment : '' }}</td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No projects found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @if(request()->has('selectedContactID'))
                    <div class="row info_buttons">
                        <div class="col-md-12">
                            <div class="table">
                                <div class="cell"><a href="#" class="btn btn-block btn-primary">NEW CONTACT</a></div>
                                <div class="cell"><a href="{{ route('add_group', ['client_id' => request()->selectedContactID, 'type' => 'contact']) }}" class="select_project btn btn-block btn-primary">GROUP ADD/REMOVE</a></div>
                                <div class="cell"><a href="{{ route('add_watch', ['id' =>app('request')->input('selectedContactID'), 'type' => 'contact']) }}" class="add_watch btn btn-block btn-primary">ADD TO WATCH LIST</a></div>
                                <div class="cell"><a href="{{ route('add_comment_company', ['client_id' => request()->selectedContactID, 'type' => 'contact']) }}" class="btn btn-block btn-primary">COMMENT</a></div>
                                <div class="cell"><a href="{{ route('attach_files', ['client_id' => request()->selectedContactID, 'type' => 'contact']) }}" class="select_project btn btn-block btn-primary">ATTACH FILE</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="row info_buttons">
                        <div class="col-md-12">
                            <div class="table">
                                <div class="cell"><a href="{{ route('sendfile') }}" class="select_project btn btn-block btn-primary">SEND FILE</a></div>
                                <div class="cell"><a href="{{ route('phone_call', ['client_id' => request()->selectedContactID, 'type' => 'contact']) }}" class="select_project btn btn-block btn-primary">PHONE CALL</a></div>
                                <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">SCHEDULE MEETING</a></div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row info_buttons">
                        <div class="col-md-12">
                            <div class="table">
                                <div class="cell"><a href="" class="btn btn-block btn-primary">NEW CONTACT</a></div>
                                <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">GROUP ADD/REMOVE</a></div>
                                <div class="cell"><a href="" class="add_watch btn btn-block btn-primary disabled">ADD TO WATCH LIST</a></div>
                                <div class="cell"><a href="" class="btn btn-block btn-primary disabled">COMMENT</a></div>
                                <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">ATTACH FILE</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="row info_buttons">
                        <div class="col-md-12">
                            <div class="table">
                                <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">SEND FILE</a></div>
                                <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">PHONE CALL</a></div>
                                <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">SCHEDULE MEETING</a></div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif

            @if (isset($projects))
                {{--<table class="table table-bordered table-condensed" style="margin-top: 20px">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>Task title</th>--}}
                        {{--<th>Task comment</th>--}}
                        {{--<th>Assign task</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                        {{--@if ($projects->count())--}}
                            {{--@foreach ($projects as $p)--}}
                                {{--@if ($p->projectEntries)--}}
                                    {{--@foreach ($p->projectEntries as $entry)--}}
                                        {{--<tr>--}}
                                            {{--<td>--}}
                                                {{--{{ $entry->entry_title }}--}}
                                            {{--</td>--}}
                                            {{--<td>--}}
                                                {{--@if ($entry->entriesComments)--}}
                                                    {{--{{ $e = $entry->entriesComments->first()['ec_comment'] }}--}}
                                                {{--@endif--}}
                                            {{--</td>--}}
                                            {{--<td><a href="{{ route('task.edit', ['task_id' => $entry->entry_id]) }}">Edit Task</a></td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    {{--</tbody>--}}
                {{--</table>--}}
            @endif

            @if (isset($group_details))
                <div class="details_show">
                    @if ($group_details)
                        <h1>{{ request()->group_id ? 'Contacts' : 'My contacts'}}</h1>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Job Title</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($group_details as $contact)
                                <tr>
                                    <td><a class="" data-id="{{ $contact->contact_id }}" href="{{  route('info', ['type' => 'contacts', 'company_id' => $contact->company_id, 'selectedContactID' => $contact->contact_id]) }}">{{ $contact->name }}</a></td>
                                    <td>{{ $contact->job_title }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <h3>Group has no contacts</h3>
                    @endif
                </div>
            @endif

        </div>
    </div>

@endsection