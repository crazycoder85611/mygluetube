@extends('layouts.app')

@section('title') Attach File @endsection

@section('content')
    <h1>Attach File</h1>
    <h3>
        {{ $title }}
    </h3>
   <div class="row">
       <div class="col-md-6">
           <form style="margin-top: 20px;" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="form-group">
                   <label>Title</label>
                   <input type="text" class="form-control" name="title" required>
               </div>
               <div class="form-group">
                   <label>Attach file</label>
                   <input type="file" class="form-control" name="file" required>
               </div>
               <div class="form-group">
                   <input type="submit" class="btn btn-info" value="Submit">
               </div>
               <input type="hidden" name="company_id" value="{{ request()->client_id }}">
           </form>
       </div>
   </div>
   <div class="row">
       <div class="col-lg-10">
           <table class="table table-bordered table-condensed table_clients">
               <thead>
                    <tr id="client_table_head">
                        <th>Title</th>
                        <th>File name</th>
                        <th>Upload date</th>
                    </tr>
               </thead>
               <tbody>
                    @foreach($attach_files as $attach_file)
                        <tr>
                            <td>{{ $attach_file->title }}</td>
                            <td><a href="{{ $attach_file->path }}" target="_blank">{{ $attach_file->path }}</a></td>
                            <td>{{ $attach_file->created_at->format('H:i d.m.Y') }}</td>
                        </tr>
                    @endforeach
               </tbody>
           </table>
       </div>
   </div>
@endsection