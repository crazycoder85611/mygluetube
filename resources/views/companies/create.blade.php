@extends('layouts.app')

@section('title') Add a Contact @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Add a Contact</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ url('companies/store') }}" method="post" enctype="multipart/form-data" id="addContactForm">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="company_logo">Company Logo:</label>
                    <input type="file" name="company_logo">
                </div>

                <div class="form-group">
                    <label for="customer_name">Customer Name:</label>
                    <input type="text" name="customer_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="industry">Industry:</label>
                    <input type="text" name="industry" class="form-control">
                </div>

                <div class="form-group">
                    <label for="pob">P. O. Box:</label>
                    <input type="text" name="pob" class="form-control">
                </div>

                <div class="form-group">
                    <label for="city">City:</label>
                    <input type="text" name="city" class="form-control">
                </div>

                <div class="ContactNumbers">
                    <div class="conLabel">
                        <label>Contact Numbers:</label>                     
                    </div>
                    <div class="addContactNumGroup">
                        <div class="contactNumDiv">
                            <input type="checkbox" name="chkDefaultType" class="chkDefaultType">
                            <select name="" class="contact_type" onchange="chkContactType(this)">
                                <option value="">--Select--</option>
                                <option value="telephone">Telephone</option>
                                <option value="fax">Fax</option>
                                <option value="mobile">Mobile</option>
                                <option value="email">Email</option>
                                <option value="website">Website</option>
                            </select>
                            <input type="text" name="" class="contactNumber form-control">
                            <span><img src="{{ url('assets/img/add_btn.jpg') }}" class="addImg" onclick="addContactNum(this)"></span>
                        </div>
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="zip">Zip Code:</label>
                    <input type="text" name="zip" class="form-control">
                </div>


                <div class="form-group">
                    <label for="website">Website:</label>
                    <input type="text" name="website" class="form-control">
                </div>

                <div class="form-group">
                    <label for="location">Location:</label>
                    <input type="text" name="location" class="form-control">
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Add a Company" />
                </div>

                {{-- <input type="hidden" name="company_id" value="{{ request()->client_id }}"> --}}
                <input type="hidden" name="type" value="{{ request()->type }}">

                <input type="hidden" name="company_id"  value="{{ request()->company_id }}" id="company_id">
                <input type="hidden" name="selectedContactID"  value="{{ request()->selectedContactID }}" id="selectedContactID">
                <input type="hidden" name="defaultNum"  value="" id="defaultNum">
                <input type="hidden" name="conNumCount" value="" id="conNumCount">
                <input type="hidden" name="contactType" value="" id="contactType">
                <input type="hidden" name="contactNums" value="" id="contactNums">
            </form>
        </div>
    </div>
    <style type="text/css">
        
        .ContactNumbers{
            width: 100%;
            height: auto;
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-start;
        }

        .conLabel{
            display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        }


        .addContactNumGroup{
            width: 100%;
            height: auto;
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: center;
        }

        .contactNumDiv{
            width: 100%;
            height: 34px;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }

        .chkDefaultType{
            visibility: hidden;
            width: 4%;
        }

        .contact_type{
            width: 35%;
            height: 100%;
            padding: 5px 10px;
            border: 1px solid #ccc;
            border-radius: 4px 4px; 
        }

        .contactNumber{
            width: 50%;
            height: 100%;
            padding: 5px 10px;
        }

        .contactNumDiv span{
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
            align-items: center;
            width: 5%;
            cursor: pointer;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">

        // adding contacts numbers
        function addContactNum(e)
        {
            var k = e.parentElement.parentElement;
            if(k.children[1].value == '')
            {
                alert("Please select a contact type");
                k.children[1].focus();
            }
            else if(k.children[2].value == '')
            {
                alert("Please provide the contact number");
                k.children[2].focus();
            }
            else
            {
                var con = `
                        <div class="contactNumDiv">
                            <input type="checkbox" name="chkDefaultType" class="chkDefaultType">
                            <select name="contact_type" class="contact_type" onchange="chkContactType(this)">
                                <option value="">--Select--</option>
                                <option value="telephone">Telephone</option>
                                <option value="fax">Fax</option>
                                <option value="mobile">Mobile</option>
                                <option value="email">Email</option>
                                <option value="website">Website</option>
                            </select>
                            <input type="text" name="contactNumber" class="contactNumber form-control">
                            <span><img src="{{ url('assets/img/add_btn.jpg') }}" class="addImg" onclick="addContactNum(this)"></span>
                        </div>`;
                $(k.parentElement).append(con);
                $(k.children[3]).css("visibility","hidden");// hiding +sign 
            }
        }

        // cheing and setting a visibility for checkbox
        function chkContactType(e)
        {
            // debugger;
            var k = e.parentElement.children[0];
            if ((e.value == "mobile") || (e.value == "telephone"))
            {
                $(k).css("visibility", "visible");
            }
            else
            {
                $(k).css("visibility", "hidden");
                e.parentElement.children[0].checked = false;
            }
        }

        // checking and submiting the form
        $("#addContactForm").on("submit", function(e){
            e.preventDefault();
            var contactType = [];
            var contactNums = [];
            var x = e.currentTarget.children[6].children[1].children;
            $("#conNumCount").val(x.length); // getting a count of the contact numbers  
            for (var i = 0; i < x.length; i++) 
            {
                contactType[i] = x[i].children[1].value;
                contactNums[i] = x[i].children[2].value;
                if(x[i].children[0].checked == true)
                {
                    $("#defaultNum").val(i);
                }
            }
            $("#contactType").val(contactType);
            $("#contactNums").val(contactNums);
            this.submit();
        });

        // setting only one check box to be true
        $("body").on("click", ".chkDefaultType", function(e){
            var h = document.getElementsByClassName("chkDefaultType");
            var k = e.currentTarget.parentElement.parentElement.children;
            for (var i = 0; i < k.length; i++) 
            {
                if($(k[i].children[0]).css("visibility") == "visible")
                {
                    h[i].checked = false;
                }
            }
            e.currentTarget.checked = true;
        });

    </script>
@endsection