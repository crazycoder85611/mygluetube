@extends('layouts.app')

@section('title') Send file @endsection

@section('body_id') id="info_page" @endsection

@section('content')
   <div class="row">
       <div class="col-md-6">
           <form style="margin-top: 20px;" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="form-group">
                   <label>From</label>
                   <input type="email" class="form-control" name="from" required>
               </div>
               <div class="form-group">
                   <label>To</label>
                   <input type="email" class="form-control" name="to" required>
               </div>
               <div class="form-group">
                   <label>Title</label>
                   <input type="text" class="form-control" name="title" required>
               </div>
               <div class="form-group">
                   <label>Text</label>
                   <textarea class="form-control" name="text" required></textarea>
               </div>
               <div class="form-group">
                   <label>Attach file</label>
                   <input type="file" class="form-control" name="file">
               </div>
               <div class="form-group">
                   <input type="submit" class="btn btn-info" value="Submit">
               </div>
           </form>
       </div>
   </div>
@endsection