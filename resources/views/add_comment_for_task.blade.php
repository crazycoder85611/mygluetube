@extends('layouts.app')

@section('title') Tasks show @endsection

@section('body_id') id="info_page" @endsection

@section('js') <script src="{{ asset('js/info.js') }}"></script> @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1>Add comment</h1>
        </div>
    </div>
    <div class="row row-eq-height">
        <div class="info_block col-md-4">
            <form action="{{ route('add_comment_for_task.store') }}" method="POST">
                {{ csrf_field() }}
                <label for="title">Comment:</label>
                <div class="form-group">
                    <textarea class="form-control" name="comment"></textarea>
                    <?php $errors = session('errors'); ?>
                    @if ($errors && isset($errors['comment']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['comment'] as $errorComment)
                                    <li>{{ $errorComment }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <input type="hidden" name="entry_id" value="{{ app('request')->input('task_id') }}" />
                <input type="submit" value="Add" class="btn btn-success">
            </form>
        </div>
    </div>
@endsection