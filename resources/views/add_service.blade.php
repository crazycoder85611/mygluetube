@extends('layouts.app')

@section('title') Select service @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Select service</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('save_service') }}" method="POST">
                {{ csrf_field() }}
                <?php $errors = session('errors'); ?>
                <div class="form-group @if (isset($errors['title'])) has-error has-danger @endif">
                    <label for="title">Title:</label>
                    <div>
                        <input type="text" name="title" class="form-control" id="title" />
                        @if ($errors && isset($errors['title']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['title'] as $errorTitle)
                                        <li>{{ $errorTitle }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group @if (isset($errors['service_id'])) has-error has-danger @endif">
                    <label for="service">Service:</label>
                    <div>
                        <select name="service_id" id="service" class="form-control">
                            @if ($services)
                                @foreach ($services as $service)
                                    <option value="{{ $service->id }}">{{ $service->title }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors && isset($errors['service_id']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['service_id'] as $errorServiceId)
                                        <li>{{ $errorServiceId }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection