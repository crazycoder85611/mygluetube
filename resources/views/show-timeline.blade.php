@extends('layouts.app')

@section('title') Timeline @endsection

@section('content')
    <div class="row">
        <div class="col-md-12 block" style="margin-top: 15px;">
            @foreach($activities as $activity)
                <div class="panel panel-default">
                    <div class="panel-heading">{!! $activity->name !!}
                        <div class="pull-right">
                            <div>
                                {{ $activity->company_name }}
                            </div>
                            <div>
                                {{ $activity->project_title }}
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                {{ $activity->date }}<br>
                                {{ $activity->const }}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var page = 1;
        $(window).scroll(function () {
            if($(window).height() + $(window).scrollTop() >= $(document).height()) {
                page++;
                $.ajax({
                    url:"{{ route('ajax_timeline') }}",
                    type:"GET",
                    data:"page="+page,
                    success:function(response) {
                        for (var i = 0; i < response['data'].length; i++) {
                           $('.block').append('\n' +
                               '                <div class="panel panel-default">\n' +
                               '                    <div class="panel-heading">' + response['data'][i]['name'] +
                               '                        <div class="pull-right">\n' +
                               '                            <div>\n' + response['data'][i]['company_name'] +
                               '                            </div>\n' +
                               '                            <div>\n' +
                                                              response['data'][i]['project_title'] +
                               '                            </div>\n' +
                               '                        </div>\n' +
                               '                    </div>\n' +
                               '                    <div class="panel-body">\n' +
                               '                        <div class="row">\n' +
                               '                            <div class="col-md-12">\n' +
                               response['data'][i]['date']  + '<br>'+
                               response['data'][i]['const'] +
                               '                            </div>\n' +
                               '                        </div>\n' +
                               '                    </div>\n' +
                               '                </div>')
                        }
                    }
                });
            }
        });
    </script>
@endsection