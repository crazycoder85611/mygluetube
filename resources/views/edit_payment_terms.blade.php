@extends('layouts.app')

@section('title') Edit payment terms (#{{ app('request')->input('service_id') }})@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Edit payment terms (#{{ app('request')->input('service_id') }})</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('save_payment_terms') }}" method="POST">
                {{ csrf_field() }}
                <?php $errors = session('errors'); ?>
                <div class="form-group @if (isset($errors['payment_terms'])) has-error has-danger @endif">
                    <label for="payment_terms">Payment terms:</label>
                    <div>
                        <textarea name="payment_terms" class="form-control" id="payment_terms">{{ $project->payment_terms ? : '' }}</textarea>
                        @if ($errors && isset($errors['payment_terms']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['payment_terms'] as $errorsPaymentTerms)
                                        <li>{{ $errorsPaymentTerms }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection