@extends('layouts.app')

@section('title') Edit a Contact @endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1>Edit a Contact</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-8">
			<form action="{!!url('contacts', $contact->contact_id)!!}" method="post" id="editContactForm">
				{{csrf_field()}}
				<div class="form-group">
					<label for="name">Contact Name:</label>
					<input type="text" name="name" class="form-control" value="{{$contact->name}}">
				</div>

				<div class="form-group">
					<label for="job_title">Job Title:</label>
					<input type="text" name="job_title" class="form-control" value="{{$contact->job_title}}">
				</div>

				<div class="form-group">
					<label for="department">Department:</label>
					<input type="text" name="department" class="form-control" value="{{$contact->department}}">
				</div>

				<div class="form-group">
					<label for="city">City:</label>
					<input type="text" name="city" class="form-control" value="{{$contact->city}}">
				</div>

				<div class="ContactNumbers">
					<div class="conLabel">
						<label>Contact Numbers:</label>                     
					</div>
					<div class="addContactNumGroup">

					@foreach ($contact_num as $contactNumber)
						<div class="contactNumDiv">
							<input type="checkbox" name="chkDefaultType" class="chkDefaultType" 
								@if ($contactNumber->default_no == 1) checked 
								@endif
								@if(($contactNumber->type == "telephone") || ($contactNumber->type == "mobile"))
									style = "visibility: visible;"
								@else
									style = "visibility: hidden;"
								@endif>
							<select name="" class="contact_type" onchange="chkContactType(this)">
								<option value="{{$contactNumber->type}}">{{$contactNumber->type}}</option>
								<option value="telephone">Telephone</option>
								<option value="fax">Fax</option>
								<option value="mobile">Mobile</option>
								<option value="email">Email</option>
								<option value="website">Website</option>
							</select>
							<input type="text" name="" class="contactNumber form-control" value="{{$contactNumber->value}}">
						</div>
					@endforeach

					</div>
				</div>
				<br>

				<div class="form-group">
					<label for="location">Location:</label>
					<input type="text" name="location" class="form-control" value="{{$contact->location}}">
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Edit Contact" />
				</div>
				<input type="hidden" name="company_id" value="{{ request()->client_id }}">
				<input type="hidden" name="type" value="{{ request()->type }}">

				<input type="hidden" name="company_id"  value="{{ request()->company_id }}" id="company_id">
				<input type="hidden" name="selectedContactID"  value="{{ $contact->contact_id }}" id="selectedContactID">				
				<input type="hidden" name="defaultNum"  value="" id="defaultNum">
				<input type="hidden" name="conNumCount" value="" id="conNumCount">
				<input type="hidden" name="contactType" value="" id="contactType">
				<input type="hidden" name="contactNums" value="" id="contactNums">
			</form>
		</div>
	</div>
	<style type="text/css">
		
		.ContactNumbers{
			width: 100%;
			height: auto;
			display: flex;
			flex-direction: column;
			justify-content: flex-start;
			align-items: flex-start;
		}

		.conLabel{
			display: inline-block;
    	max-width: 100%;
    	margin-bottom: 5px;
    	font-weight: 700;
		}


		.addContactNumGroup{
			width: 100%;
			height: auto;
			display: flex;
			flex-direction: column;
			justify-content: flex-start;
			align-items: center;
		}

		.contactNumDiv{
			width: 100%;
			height: 34px;
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			align-items: center;
			margin-bottom: 20px;
		}

		.chkDefaultType{
			visibility: hidden;
			width: 4%;
		}

		.contact_type{
			width: 35%;
			height: 100%;
			padding: 5px 10px;
			border: 1px solid #ccc;
			border-radius: 4px 4px; 
		}

		.contactNumber{
			width: 50%;
			height: 100%;
			padding: 5px 10px;
		}

		.contactNumDiv span{
			display: flex;
			flex-direction: row;
			justify-content: flex-end;
			align-items: center;
			width: 5%;
			cursor: pointer;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript">

		// cheing and setting a visibility for checkbox
		function chkContactType(e)
		{
			// debugger;
			var k = e.parentElement.children[0];
			if ((e.value == "mobile") || (e.value == "telephone"))
			{
				$(k).css("visibility", "visible");
			}
			else
			{
				$(k).css("visibility", "hidden");
				e.parentElement.children[0].checked = false;
			}
		}

		// checking and submiting the form
		$("#editContactForm").on("submit", function(e){
			e.preventDefault();
			// debugger;
			var contactType = [];
			var contactNums = [];
			var x = e.currentTarget.children[5].children[1].children;
			$("#conNumCount").val(x.length); // getting a count of the contact numbers  
			for (var i = 0; i < x.length; i++) 
			{
				contactType[i] = x[i].children[1].value;
				contactNums[i] = x[i].children[2].value;
				if(x[i].children[0].checked == true)
				{
					$("#defaultNum").val(i);
				}
			}
			$("#contactType").val(contactType);
			$("#contactNums").val(contactNums);
			this.submit();
		});

		// setting only one check box to be true
		$("body").on("click", ".chkDefaultType", function(e){
			var h = document.getElementsByClassName("chkDefaultType");
			var k = e.currentTarget.parentElement.parentElement.children;
			for (var i = 0; i < k.length; i++) 
			{
				if($(k[i].children[0]).css("visibility") == "visible")
				{
					h[i].checked = false;
				}
			}
			e.currentTarget.checked = true;
		});



	</script>

@endsection