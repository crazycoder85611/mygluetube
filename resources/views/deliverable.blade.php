@extends('layouts.app')

@section('title') Select deliverables (#{{ app('request')->input('service_id') }})@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Select deliverables (#{{ app('request')->input('service_id') }})</h1>
        </div>
    </div>
    <?php $errors = session('errors'); ?>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('create_deliverables') }}" method="POST">
                {{ csrf_field() }}
                <div class="input-group @if (isset($errors['title'])) has-error has-danger @endif">
                    <input type="text" name="title" class="form-control" placeholder="New deliverable">
                    <span class="input-group-btn">
                    <input type="submit" class="btn btn-default" value="Create" />
                    </span>
                </div>
                <div class="input-group @if (isset($errors['title'])) has-error has-danger @endif">
                    @if ($errors && isset($errors['title']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['title'] as $errorTitle)
                                    <li>{{ $errorTitle }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </form>
            <form action="{{ route('save_deliverables') }}" method="POST">
                {{ csrf_field() }}
                @if ($deliverables)
                    <div class="form-group @if (isset($errors['deliverables'])) has-error has-danger @endif">
                        @foreach ($deliverables as $deliverable)
                            <div>
                                <input name="deliverables[]" type="checkbox" id="deliverable_{{ $deliverable->id }}" value="{{ $deliverable->id }}" @if (isset($selectDeliverables[$deliverable->id])) checked @endif>
                                <label for="deliverable_{{ $deliverable->id }}">{{ $deliverable->title }}</label>
                            </div>
                        @endforeach
                        @if ($errors && isset($errors['deliverables']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['deliverables'] as $errorDeliverable)
                                        <li>{{ $errorDeliverable }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                @endif
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection