@extends('layouts.app')

@section('title') Edit timeline (#{{ app('request')->input('service_id') }})@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Edit timeline (#{{ app('request')->input('service_id') }})</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('save_timeline_of_service') }}" method="POST">
                {{ csrf_field() }}
                <?php $errors = session('errors'); ?>
                @if ($deliverables)
                    @foreach ($deliverables as $deliverable)
                        <div class="form-group @if (isset($errors['delivery_date'])) has-error has-danger @endif">
                            <label for="delivery_date_{{ $deliverable->deliverable_id }}">{{ isset($allDeliverables[$deliverable->deliverable_id]) ? $allDeliverables[$deliverable->deliverable_id]->title .' (Timeline)' : '' }}</label>
                            <div>
                                <input type="delivery_date" id="delivery_date_{{ $deliverable->deliverable_id }}" name="delivery_date[{{ $deliverable->deliverable_id }}]" class="form-control delivery_date" value="{{ isset($timeline[$deliverable->deliverable_id]) ? date('Y-m-d', $timeline[$deliverable->deliverable_id]->delivery_date) : '' }}" />
                            </div>
                        </div>
                    @endforeach
                    @if ($errors && isset($errors['delivery_date']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['delivery_date'] as $errorDeliveryDate)
                                    <li>{{ $errorDeliveryDate }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @endif
                <div class="form-group @if (isset($errors['final_delivery'])) has-error has-danger @endif">
                    <label for="final_delivery">Final delivery:</label>
                    <div>
                        <input type="final_delivery" id="final_delivery" name="final_delivery_date" class="form-control" value="{{ $project->final_delivery_date ? date('Y-m-d', $project->final_delivery_date) : '' }}" />
                        @if ($errors && isset($errors['final_delivery']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['final_delivery'] as $errorFinalDelivery)
                                        <li>{{ $errorFinalDelivery }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group @if (isset($errors['final_delivery'])) has-error has-danger @endif">
                    <label for="project_duration">Project duration:</label>
                    <div>
                        <input type="project_duration" id="project_duration" name="project_duration" class="form-control" value="{{ $project->project_duration ? : '' }}" />
                        @if ($errors && isset($errors['project_duration']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['project_duration'] as $errorProjectDuration)
                                        <li>{{ $errorProjectDuration }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection