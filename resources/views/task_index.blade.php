
@extends('layouts.app')

@section('title') Tasks @endsection

@section('body_id') id="info_page" @endsection

@section('js') <script src="{{ asset('js/info.js') }}"></script> @endsection

@section('content')
<br/>
    <div class="row row-eq-height">
        <div class="info_block col-md-12">
            @if ($tasks)
            <table class="table">
                @foreach($tasks as $task)
                    <tr>
                        <td>{{ $task->entry_title }}</td>
                        <td>{{ $task->start_date }}</td>
                        <td>{{ $task->end_date }}</td>
                        <td>{{ $task->user->username }}</td>
                        <td><a href="{{ route('task.show', ['task_id' => $task->entry_id]) }}">To task</a></td>
                    </tr>
                @endforeach
            </table>
            @endif
		</div>
    </div>
@endsection