@extends('layouts.app')

@section('page-title', trans('app.dashboard'))

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            @lang('app.welcome') <?= Auth::user()->username ?: Auth::user()->first_name ?>!
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li class="active">@lang('app.dashboard')</li>
                </ol>
            </div>
        </h1>
    </div>
</div>

<div class="row">
<div class="col-md-12">
        <a href="#" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <h4 class="title">Attendance</h4>
                    <input type="text" class="form-control" value="" id="att_comment" name="att_comment" placeholder="Add Comments if any." style="width:100%">
                    <br/>
                    <button name="att_comment_btn" id="att_comment_btn" type="submit" value="start" class="btn btn-success">START</button>
                    <button name="att_comment_btn2" id="att_comment_btn2" type="submit" value="stop" class="btn btn-danger">STOP</button>
                </div>
            </div>
        </a>
    </div>
</div>




<div class="row">
    <div class="col-md-3">
        <a href="{{ route('profile') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <p class="lead">@lang('app.update_profile')</p>
                </div>
            </div>
        </a>
    </div>
    @if (config('session.driver') == 'database')
        <div class="col-md-3">
            <a href="{{ route('profile.sessions') }}" class="panel-link">
                <div class="panel panel-default dashboard-panel">
                    <div class="panel-body">
                        <div class="icon">
                            <i class="fa fa-list"></i>
                        </div>
                        <p class="lead">@lang('app.my_sessions')</p>
                    </div>
                </div>
            </a>
        </div>
    @endif
    <div class="col-md-3">
        <a href="{{ route('profile.activity') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                    <p class="lead">@lang('app.activity_log')</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3">
        <a href="{{ route('auth.logout') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-sign-out"></i>
                    </div>
                    <p class="lead">@lang('app.logout')</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{ route('mail') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-sign-out"></i>
                    </div>
                    <p class="lead">Reimburse expense</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{ route('mail') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-sign-out"></i>
                    </div>
                    <p class="lead">Cash advance</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{ route('mail') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-sign-out"></i>
                    </div>
                    <p class="lead">ID letter request</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3">
        <a href="{{ route('mail_with_file') }}" class="panel-link">
            <div class="panel panel-default dashboard-panel">
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-sign-out"></i>
                    </div>
                    <p class="lead">Documents</p>
                </div>
            </div>
        </a>
    </div>

    
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('app.activity') (@lang('app.last_two_weeks')</div>
            <div class="panel-body">
                <div>
                    <canvas id="myChart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    <script>
        var labels = {!! json_encode(array_keys($activities)) !!};
        var activities = {!! json_encode(array_values($activities)) !!};

        $("#att_comment_btn").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                data: {comment: $('#att_comment').val(),attn_type:$('#att_comment_btn').val()},
                url: "{{ url('oldgt/add_attendance.php') }}",
                success: function(data){

                    alert(data);

                },
                error: function(data) {
                    console.log(data); //error message
                }
            });
        });

        $("#att_comment_btn2").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                data: {comment: $('#att_comment').val(),attn_type:$('#att_comment_btn2').val()},
                url: "{{ url('oldgt/add_attendance.php') }}",
                success: function(data){

                    alert(data);

                },
                error: function(data) {
                    console.log(data); //error message
                }
            });
        });
    </script>
    {!! HTML::script('assets/js/chart.min.js') !!}
    {!! HTML::script('assets/js/as/dashboard-default.js') !!}
@stop