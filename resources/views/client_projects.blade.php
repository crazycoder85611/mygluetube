@extends('layouts.app')

@section('title') Clients @endsection

@section('content')
    <div class="row">
        <div id="contact_details_project_block">
            @if ($title || $contact_details)
                <table class="table table-bordered table-condensed">
                    <tr>
                        <td>
                            Customer
                            <div class="lead">{{ $title }}</div>
                        </td>
                        <td>
                            @if ($contact_details)
                                @foreach ($contact_details as $contactDetail)
                                    <div><strong>{{ ucfirst($contactDetail->type) }}: </strong>{{ $contactDetail->value }}</div>
                                @endforeach
                            @endif
                        </td>
                    </tr>
                </table>
            @endif
            <div class="row row_client_button_project">
                <div class="col-lg-2 col-lg-offset-1"><a href="{{ route('add_service', ['client_id' => app('request')->input('id')]) }}" class="btn btn-block btn-primary">START NEW PROJECT</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">EDIT</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">NEW CONT</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">GROUP ADD/REMOVE</a></div>
                <div class="col-lg-2"><a href="{{ route('add_watch', app('request')->input('id')) }}" class="add_watch btn btn-block btn-primary">ADD TO WATCH LIST</a></div>
            </div>
            <div class="row row_client_button_project info_buttons_project">
                <div class="col-lg-2 col-lg-offset-1"><a href="" class="btn btn-block btn-primary disabled">COMMENT</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">NEW ORGANIZATION</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">SEND FILE</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">PHONE CALL</a></div>
                <div class="col-lg-2"><a href="" class="btn btn-block btn-primary disabled">ATTACH FILE</a></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <form action="#" method="POST">
                <table class="table table-bordered table-condensed table_clients">
                    <thead>
                        <tr id="client_table_head">
                            <th style="width: 5%"></th>
                            <th style="width: 10%">ID
                                @if (app('request')->input('sort') != 'id' || (app('request')->input('sort') == 'id' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'id']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'id', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 20%">Service
                                @if (app('request')->input('sort') != 'title' || (app('request')->input('sort') == 'title' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'title']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'title', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 20%">Timeline
                                @if (app('request')->input('sort') != 'project_duration' || (app('request')->input('sort') == 'project_duration' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'project_duration']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'project_duration', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 10%">Value
                                @if (app('request')->input('sort') != 'estimate_cost' || (app('request')->input('sort') == 'estimate_cost' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'estimate_cost']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'estimate_cost', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 10%">Due Date
                                @if (app('request')->input('sort') != 'final_delivery_date' || (app('request')->input('sort') == 'final_delivery_date' && app('request')->input('sort_type') == 'asc'))
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'final_delivery_date']) }}">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                @else
                                    <a href="{{ route('client_projects', ['id' => app('request')->input('id'), 'page' => app('request')->input('page'), 'sort' => 'final_delivery_date', 'sort_type' => 'asc']) }}">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </th>
                            <th style="width: 25%">
                               Comment
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($projects)
                            @foreach ($projects as $project)
                                <tr>
                                    <td><input class="project_checkbox" type="checkbox" value="{{ $project->id }}"></td>
                                    <td>{{ $project->id }}</td>
                                    <td>{{ $project->title }}</td>
                                    <td>{{ ($project->project_duration) ? : '' }}</td>
                                    <td>{{ ($project->estimate_cost) ? : '' }}</td>
                                    <td>{{ ($project->final_delivery_date) ? date('Y-m-d', $project->final_delivery_date) : '' }}</td>
                                    <td>{{ isset($comments[$project->id]) ? $comments[$project->id] : '' }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    @if ($projects->lastPage() > 1)
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <ul class="pagination pagination-sm">
                    <?php
                        $showOnTheSides = 2;

                        $firstPage = (($projects->currentPage() - $showOnTheSides) > 0) ? $projects->currentPage() - $showOnTheSides : 1;

                        $lastPage = (($projects->currentPage() + $showOnTheSides) > $projects->lastPage()) ?
                            $projects->lastPage() :
                            $projects->currentPage() + $showOnTheSides;

                        function getUrlPage($page)
                        {
                            return route('client_projects', [
                                'id'        => app('request')->input('id'),
                                'sort'      => app('request')->input('sort'),
                                'sort_type' => app('request')->input('sort_type'),
                                'page'      => ($page != 1) ? $page : null,
                            ]);
                        }
                    ?>
                    <li><a href="{{ getUrlPage(1) }}"><<</a></li>
                        @for ($i = $firstPage; $i <= $lastPage; $i++)
                            <li @if ($i == $projects->currentPage()) class="active" @endif ><a href="{{ getUrlPage($i) }}">{{ $i }}</a></li>
                        @endfor
                    <li><a href="{{ getUrlPage($projects->lastPage()) }}">>></a></li>
                </ul>
            </div>
        </div>
    @endif
    <div class="row row_client_button">
        <div class="col-lg-2 col-lg-offset-1"><a href="{{ route('add_service', ['client_id' => app('request')->input('id')]) }}" class="btn btn-block btn-primary">SELECT SERVICE</a></div>
        <div class="col-lg-2"><a href="" class="select_project btn btn-block btn-primary disabled">DEVELOP BRIEF</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">REQUEST PROPOSAL</a></div>
        <div class="col-lg-2"><a href="{{ route('select_deliverables', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">DELIVERABLES</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_timeline_of_service', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">TIMELINE</a></div>
    </div>

    <div class="row row_client_button">
        <div class="col-lg-2 col-lg-offset-1"><a href="{{ route('edit_financial_cost', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">FINANCIAL COST</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_payment_terms', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">PAYMENT TERMS</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id'), 'stage' => 2]) }}" class="select_project btn btn-block btn-primary disabled">SUBMIT PROPOSAL</a></div>
        <div class="col-lg-2"><a href="{{ route('follow', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">PROPOSAL FOLLOW UP</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id'), 'stage' => 3]) }}" class="select_project btn btn-block btn-primary disabled">REVISE PROPOSAL</a></div>
    </div>

    <div class="row row_client_button">
        <div class="col-lg-2 col-lg-offset-1"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id'), 'stage' => 4]) }}" class="select_project btn btn-block btn-primary disabled">ACCEPT PROPOSAL</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id'), 'stage' => 5]) }}" class="select_project btn btn-block btn-primary disabled">FINAL CUSTOMER APPROVAL</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id'), 'stage' => 6]) }}" class="select_project btn btn-block btn-primary disabled">PROJECT DELIVERY</a></div>
        <div class="col-lg-2"><a href="{{ route('edit_service_stage', ['client_id' => app('request')->input('id'), 'stage' => 7]) }}" class="select_project btn btn-block btn-primary disabled">CLOSE & ARCHIVE</a></div>
        <div class="col-lg-2"><a href="{{ route('add_comment', ['client_id' => app('request')->input('id')]) }}" class="select_project btn btn-block btn-primary disabled">ADD COMMENT</a></div>
    </div>
@endsection