@extends('layouts.app')

@section('title') Proposal follow up (#{{ app('request')->input('service_id') }})@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Proposal follow up (#{{ app('request')->input('service_id') }})</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('sent_follow') }}" method="POST">
                {{ csrf_field() }}
                <?php $errors = session('errors'); ?>
                <div class="form-group">
                    <label for="email">To:</label>
                    <div>
                        <input type="email" name="email" value="{{ $email }}" id="email" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="from">From:</label>
                    <div>
                        <input type="text" name="from" id="from" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="title">Title:</label>
                    <div>
                        <input type="text" name="title" id="title" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="body">Body:</label>
                    <div>
                        <textarea name="body" id="body" class="form-control" required ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
                    @if ($errors && isset($errors['client_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['client_id'] as $errorClientId)
                                    <li>{{ $errorClientId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="hidden" name="service_id" value="{{ app('request')->input('service_id') }}" />
                    @if ($errors && isset($errors['service_id']))
                        <div class="help-block with-errors">
                            <ul class="list-unstyled">
                                @foreach ($errors['service_id'] as $errorServiceId)
                                    <li>{{ $errorServiceId }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Send" />
                    <a href="{{ route('client_projects') . '?id=' . app('request')->input('client_id') }}" class="btn btn-default button_back">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection