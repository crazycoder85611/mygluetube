@if (isset($contacts))
    <div class="details_show">
        @if ($contacts->count())
            <h1>Contacts</h1>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Job Title</th>
                        @if ($details)
                            @foreach(current($details) as $nameDetail => $v)
                                <th>{{ ucfirst($nameDetail) }}</th>
                            @endforeach
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td><a class="load_contact_details" data-id="{{ $contact->contact_id }}" href="{{ route('load_contact_details', $contact->contact_id) }}">{{ $contact->name }}</a></td>
                            <td>{{ $contact->job_title }}</td>
                            @if ($details)
                                @foreach($details[$contact->contact_id] as $detail)
                                    <td>{{ $detail }}</td>
                                @endforeach
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h3>Company has no contacts</h3>
        @endif
    </div>
@elseif (isset($contact_details))
    <div id="contact_details_block">
        @if ($title || $contact_details || $projects)
            <h2>Customer information <button class="btn btn-default back_contacts pull-right">Back</button></h2>
            <table class="table table-bordered table-condensed">
                <tr>
                    <td>
                        Customer
                        <div class="lead">{{ $title }}</div>
                    </td>
                    <td>
                        @if ($contact_details)
                            @foreach ($contact_details as $contactDetail)
                                <div><strong>{{ ucfirst($contactDetail->type) }}: </strong>{{ $contactDetail->value }}</div>
                            @endforeach
                        @endif
                    </td>
                </tr>
            </table>

            <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Project Title</th>
                        <th>Value</th>
                        <th>Balance</th>
                        <th>Next Action</th>
                        <th>Next Task</th>
                        <th>Due Date</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($projects->count())
                        @foreach ($projects as $project)
                            <tr>
                                <td></td>
                                <td><a href="{{ route('client_projects', ['id' => $project->id]) }}">{{ $project->title }}</a></td>
                                <td>{{ $project->estimate_cost }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $project->date_time_created ? date('Y-m-d', $project->date_time_created) : '' }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">No projects found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        @endif
        <div class="row info_buttons">
            <div class="col-md-12">
                <div class="table">
                    <div class="cell"><a href="{{ route('add_service', ['client_id' => $client_id]) }}" class="btn btn-block btn-primary">START NEW PROJECT</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">EDIT</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">NEW CONT</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">GROUP ADD/REMOVE</a></div>
                    <div class="cell"><a href="{{ route('add_watch', $client_id) }}" class="add_watch btn btn-block btn-primary">ADD TO WATCH LIST</a></div>
                </div>
            </div>
        </div>
        <div class="row info_buttons">
            <div class="col-md-12">
                <div class="table">
                    <div class="cell"><a href="" class="btn btn-block btn-primary disabled">COMMENT</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">NEW ORGANIZATION</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">SEND FILE</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">PHONE CALL</a></div>
                    <div class="cell"><a href="" class="select_project btn btn-block btn-primary disabled">ATTACH FILE</a></div>
                </div>
            </div>
        </div>
    </div>
@endif
