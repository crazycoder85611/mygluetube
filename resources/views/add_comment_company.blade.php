@extends('layouts.app')

@section('title') Add comment@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Add comment</h1>
            <h3>{{ $title }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <form action="{{ route('add_comment_company') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <div>
                        <textarea name="comment" class="form-control" id="comment" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Add comment" />
                </div>
                <input type="hidden" name="company_id" value="{{ request()->client_id }}">
                <input type="hidden" name="type" value="{{ request()->type }}">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <table class="table table-bordered table-condensed table_clients">
                <thead>
                <tr id="client_table_head">
                    <th>Comment</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                    <tr>
                        <td>{{ $comment->comment }}</td>
                        <td>{{ $comment->created_at->format('H:i d.m.Y') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection