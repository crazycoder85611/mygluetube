@extends('layouts.app')

@section('title') Task create @endsection

@section('body_id') id="info_page" @endsection

@section('js') <script src="{{ asset('js/index.js') }}"></script> @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>Task create</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-8">
            Title - {{ $project->title }}
            <br />
            Company - {{ $project->contact->name }}
        </div>
    </div>
    <div class="row row-eq-height">
        <div class="info_block col-md-4">
            <form action="{{ route('task.store') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="project_id" value="{{ app('request')->input('project_id') }}" />
                <?php $errors = session('errors'); ?>

                <div class="form-group">
                    <label for="title">Title:</label>
                    <div>
                        <input type="text" name="title" value="" id="title" class="form-control" required />
                        @if ($errors && isset($errors['title']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['title'] as $errorTitle)
                                        <li>{{ $errorTitle }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="status">Status:</label>
                    <div>
                        <select id="status" name="entry_status" class="form-control">
                            @foreach($entry_status as $status)
                                <option value="{{ $status }}">{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="end_date">Due Date:</label>
                    <div>
                        <input type="text" name="end_date" value="" id="end_date" class="form-control" required />
                        @if ($errors && isset($errors['end_date']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['end_date'] as $errorEndDate)
                                        <li>{{ $errorEndDate }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="users">Users:</label>
                    <div>
                        <select name="users[]" multiple class="form-control" id="users">
                            @if ($users)
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->username }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors && isset($errors['users.0']))
                            <div class="help-block with-errors">
                                <ul class="list-unstyled">
                                    @foreach ($errors['users.0'] as $errorUser)
                                        <li>{{ $errorUser }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                @if (session('success'))
                    {{ session('success') }}
                @endif
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Send" />
                </div>
            </form>
        </div>
    </div>
@endsection