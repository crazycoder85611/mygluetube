<?php

use Illuminate\Database\Seeder;
use Vanguard\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'name' => 'gt',
            'public_or_not' => true,
        ]);

        Group::create([
            'name' => 'Vandour',
            'public_or_not' => true,
        ]);

        Group::create([
            'name' => 'My Contacts',
            'public_or_not' => false,
        ]);

        Group::create([
            'name' => 'My Companies',
            'public_or_not' => false,
        ]);
    }
}
