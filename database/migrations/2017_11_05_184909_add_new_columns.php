<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('watch_list', function (Blueprint $table) {
            $table->enum('type', ['contact', 'company'])->default('contact');
        });

        Schema::table('company_comments', function (Blueprint $table) {
            $table->enum('type', ['contact', 'company'])->default('contact');
        });

        Schema::table('attach_files', function (Blueprint $table) {
            $table->enum('type', ['contact', 'company'])->default('contact');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('watch_list', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('company_comments', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('attach_files', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
