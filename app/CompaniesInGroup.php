<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class CompaniesInGroup extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function company()
    {
        return $this->hasOne(Company::class, 'company_id', 'company_id');
    }
}
