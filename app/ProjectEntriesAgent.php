<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class ProjectEntriesAgent extends Model
{
    protected $table = 'project_entries_agents';

    public $timestamps = false;

    protected $fillable = [
        'entry_id',
        'agent_id',
        'project_read',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'agent_id');
    }
}
