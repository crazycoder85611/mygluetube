<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class ContactsInGroup extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function contact()
    {
        return $this->hasOne(Contact::class, 'contact_id', 'contact_id');
    }
}
