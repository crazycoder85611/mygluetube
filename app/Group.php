<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
