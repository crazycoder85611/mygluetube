<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Contact;
use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;
use Auth;

use Cookie;
use Tracker;
use Session;
use DB;

class InfoController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;

    }

    public function index($type, Request $request)
    {
        $this->checkRole('ABC');
        $data = $this->project->getCompaniesOrContacts($type, $request->company_id, $request->selectedContactID);
        $data['type'] = $type;
        session()->put('message', 'Success');

        return view('info', $data);
    }

    public function loadContacts($id)
    {
        $this->checkRole('ABC');
        $contacts = $this->project->getContactsById($id);

        $html = view('info_part', [
            'contacts' => $contacts,
            'details'  => $this->project->getContactDetails($contacts),
        ])->render();

        return response()->json(['html' => $html]);
    }

    public function loadContactDetails($id)
    {
        $this->checkRole('ABC');
        list($title, $details) = $this->project->getContactDetail($id);

        $html = view('info_part', [
            'contact_details' => $details,
            'title'           => $title,
            'projects'        => $this->project->getProjectForInfo($id),
            'client_id'       => $id,
        ])->render();

        return response()->json(['html' => $html]);
    }

    public function addWatch($id)
    {
        $this->checkRole('ABC');
        $messages = $this->project->addWatch($id, Auth::user()->id,  request()->type);

        return response()->json(['messages' => ucfirst($messages)]);
    }

    private function checkRole($role)
    {
        if (Auth::user()->role->name != $role) {
            abort(403);
        }
    }

    public function create(){
        return view('contacts/create');
    }
}
