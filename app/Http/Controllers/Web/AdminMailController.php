<?php

namespace Vanguard\Http\Controllers\Web;

use Illuminate\Support\Facades\Storage;
use Vanguard\Activity2;
use Vanguard\Http\Controllers\Controller;
use Vanguard\Mail\AdminSendFileMail;

class AdminMailController extends Controller
{
    public function index()
    {
        $this->checkRole('ABC');

        return view('sendfile');
    }

    public function send()
    {
        $path = null;
        if (request()->hasFile('file'))
        {
            $path = request()->file('file')->store('public/admin_files');
            $path = url(Storage::url($path));
        }

        \Mail::to(request('to'))->send(new AdminSendFileMail(request('from'), request('title'), request('text'), $path));

        Activity2::create([
            'date_time' => time(),
            'user_id' => auth()->user()->id,
            'action_type_id' => Activity2::file_sent_to_client,
        ]);

        return redirect(route('sendfile'))->with('status', 'Email was successfully sent.');
    }

    private function checkRole($role)
    {
        if (\Auth::user()->role->name != $role) {
            abort(403);
        }
    }
}
