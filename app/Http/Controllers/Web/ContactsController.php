<?php

namespace Vanguard\Http\Controllers\Web;

use League\OAuth1\Client\Tests;
use Illuminate\Http\Request;
use Vanguard\Http\Controllers\Controller;
use Vanguard\Contact;
use Input;
use DB;

 
class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Code
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_id = Input::get('company_id');
        $selectedContactID = Input::get('selectedContactID');

        return view('contacts/create',compact('company_id','selectedContactID'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new Contact();
        $contact->name = Input::get('name');
        $contact->job_title = Input::get('job_title');
        $contact->department = Input::get('department');
        $contact->city = Input::get('city');
        $contact->location = Input::get('location');
        $contact->company_id = Input::get('company_id');

        DB::table('contact')->insert(
            [
                'name' => $contact->name,
                'job_title' => $contact->job_title,
                'department' => $contact->department,
                'city' => $contact->city,
                'department' => $contact->department,
                'location' => $contact->location,
                'company_id' => $contact->company_id
            ]
        );

        // getting data of the contact numbers from client
        $contact->contact_id = Input::get('selectedContactID');
        $contact->conNumCount = Input::get('conNumCount');
        $contact->contactType = explode(",",Input::get('contactType'));
        $contact->contactNums = explode(",",Input::get('contactNums'));
        $contact->defaultNum  = Input::get('defaultNum');

        // inserting contact_datails
        for ($i=0; $i < $contact->conNumCount; $i++) { 
            // getting default_no
            if($contact->defaultNum == $i)
            {
                $defNum = 1;
            }
            else
            {
                $defNum = 0;                
            }
            //insert data
            DB::table('contact_details')->insert(
                [
                    'contact_id' => $contact->contact_id,
                    'type'       => $contact->contactType[$i],
                    'value'      => $contact->contactNums[$i],
                    'default_no' => $defNum
                ]
            );            
        }

        return redirect()->route('info', ['type' => 'contacts', 'company_id' => $contact->company_id, 'selectedContactID' => $contact->contact_id]);        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::findOrFail($id);

        $company_id = request()->company_id;// getting a company_id from ../{id}/edit?company_id=xxx
        $contact_id = $id;// getting a company_id from ../{id}/edit?company_id=xxx
        $userId = auth()->user()->id;   //getting the logged in user id

        //update company_id in contact database.
        $update = array(
            'company_id' => $company_id,
            'agent_id' => $userId
            );
            

        DB::table('contact')
            ->where('contact_id', $id)
            ->update($update);

        $contact_num = DB::table('contact_details')
                     ->select('*')
                     ->where('contact_id', '=', $id)
                     ->get();

        return view('contacts/edit', compact('contact','contact_num','company_id','contact_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->update($request->all());

        // getting data of the contact numbers from client
        $contact->conNumCount = Input::get('conNumCount');
        $contact->contactType = explode(",",Input::get('contactType'));
        $contact->contactNums = explode(",",Input::get('contactNums'));
        $contact->defaultNum  = Input::get('defaultNum');       

        $contact_num = DB::table('contact_details')
                     ->select('id')
                     ->where('contact_id', '=', $id)
                     ->get();        
        $get_id[]='';

        foreach($contact_num as $con_id)
        {
            $get_id[] = $con_id->id;
        }

        for ($i=0; $i < $contact->conNumCount; $i++) 
        { 
            // getting default_no
            if($contact->defaultNum == $i)
            {
                $defNum = 1;
            }
            else
            {
                $defNum = 0;                
            }

            //update data
            $update = array(
                'type' => $contact->contactType[$i],
                'value' => $contact->contactNums[$i],
                'default_no' => $defNum);

            DB::table('contact_details')
                ->where('id', $get_id[$i+1])
                ->update($update);
        }

        $company_id = request()->company_id;

        return redirect()->route('info', ['type' => 'contacts', 'company_id' => $company_id, 'selectedContactID' => $contact->contact_id]);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
