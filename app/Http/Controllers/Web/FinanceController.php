<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;
use Vanguard\Activity2;
use Auth;

class FinanceController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;
    }

    public function editFinancialCost(Request $request)
    {
        return view('edit_financial_cost', [
            'project' => $this->project->getProject($request->service_id),
        ]);
    }

    public function saveFinancialCost(Request $request)
    {
        if ($errors = $this->validatorFinancialCost($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->updateFinancialCost($request->all());

        $this->project->addActivity(Activity2::FINANCIAL_COST_UPDATED, $request->all(), Auth::user()->id);

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Financial cost changed successfully');
    }

    private function validatorFinancialCost($data)
    {
        $errors = Validator::make($data, [
            'estimate_cost' => 'required|numeric',
            'service_id'    => 'required|exists:projects,id',
            'client_id'     => 'required|integer',
        ])->errors();

        return $errors->messages();
    }

    public function editPaymentTerms(Request $request)
    {
        return view('edit_payment_terms', [
            'project' => $this->project->getProject($request->service_id),
        ]);
    }

    public function savePaymentTerms(Request $request)
    {
        if ($errors = $this->validatorPaymentTerms($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->updatePaymentTerms($request->all());

        $this->project->addActivity(Activity2::PAYMENT_TERMS_UPDATED, $request->all(), Auth::user()->id);

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Payment terms changed successfully');
    }

    private function validatorPaymentTerms($data)
    {
        $errors = Validator::make($data, [
            'payment_terms' => 'required|string|min:1|max:255',
            'service_id'    => 'required|exists:projects,id',
            'client_id'     => 'required|integer',
        ])->errors();

        return $errors->messages();
    }
}
