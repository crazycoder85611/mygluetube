<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Company;
use Vanguard\Contact;
use Vanguard\Http\Controllers\Controller;
use Vanguard\PhoneCall;

class PhoneCallController extends Controller
{
    public function index()
    {
        $phone_calls = PhoneCall::where(['client_id' => request()->client_id, 'type' => request()->type])->orderBy('id', 'desc')->get();

        if (request()->type == 'company') {
            $title = 'Company Name: ' . Company::where('company_id', request()->client_id)->first()->company_name;
        } else {
            $title = 'Contact Name: ' . Contact::where('contact_id', request()->client_id)->first()->name;
        }

        return view('add_phone_call', compact('phone_calls', 'title'));
    }

    public function store() {
        PhoneCall::create(request()->all());

        return redirect()->route('phone_call', ['client_id' => request()->client_id, 'type' => request()->type])->with('status', 'Phone call has been successfully added.');
    }
}
