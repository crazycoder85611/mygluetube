<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Auth;

class ClientController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        if (!$request->id) {
            die('No client ID');
        }

        $this->project = $project;
    }

    public function index(Request $request)
    {
        $this->checkRole('ABC');
        list($title, $detals) = $this->project->getContactDetail($request->id);

        $projects = $this->project->getProjects($request->id, $request->sort, $request->sort_type);

        return view('client_projects', [
            'projects'         => $projects,
            'title'            => $title,
            'contact_details'  => $detals,
            'comments'         => $this->project->getComments($projects),
        ]);
    }

    private function checkRole($role)
    {
        if (Auth::user()->role->name != $role) {
            abort(403);
        }
    }
}
