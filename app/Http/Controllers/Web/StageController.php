<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;
use Vanguard\Activity2;
use Auth;

class StageController extends Controller
{
    private $project;
    private $stages = [
        1 => Activity2::STATUS_CHANGED_TO_PROPOSAL_REQUESTED,
        2 => Activity2::STATUS_CHANGED_TO_SUBMIT_PROPOSAL,
        3 => Activity2::STATUS_CHANGED_TO_REVISE_PROPOSAL,
        4 => Activity2::STATUS_CHANGED_TO_ACCEPT_PROPOSAL,
        5 => Activity2::STATUS_CHANGED_TO_FINAL_CUSTOMER_APPROVAL,
        6 => Activity2::STATUS_CHANGED_TO_PROJECT_DELIVERY,
        7 => Activity2::STATUS_CHANGED_TO_CLOSE_AND_ARCHIVE,
    ];

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;
    }

    public function editStage(Request $request)
    {
        return view('edit_service_stage', [
            'project' => $this->project->getProject($request->service_id),
        ]);
    }

    public function saveStage(Request $request)
    {
        if ($errors = $this->validatorSaveStage($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->updateProjectStage($request->service_id, $request->all());

        $this->project->addActivity($this->stages[$request->stage], $request->all(), Auth::user()->id);

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Stage changed successfully');
    }

    private function validatorSaveStage($data)
    {
        $errors = Validator::make($data, [
            'stage'      => 'required|integer',
            'service_id' => 'required|exists:projects,id',
            'client_id'  => 'required|integer',
        ])->errors();

        return $errors->messages();
    }
}
