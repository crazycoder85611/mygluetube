<?php

namespace Vanguard\Http\Controllers\Web;
use Vanguard\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Vanguard\Company;
use Input;
use DB;


class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_id = Input::get('company_id');
        $selectedContactID = Input::get('selectedContactID');

        return view('companies/create',compact('company_id','selectedContactID'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
/*        
        $company->mobile_number = Input::get('mobile_number');
        $company->location = Input::get('location');
        $company->website = Input::get('website');
        $company->tel = Input::get('tel');
        $company->email = Input::get('email');
        $company->zip = Input::get('zip');
        $company->city = Input::get('city');
        $company->pob = Input::get('pob');
        $company->industry = Input::get('industry');
        $company->customer_name = Input::get('customer_name');
        $company->company_id = Input::get('company_id');

        $file = Input::file('company_logo');
        $destinationPath = public_path() . '\companylogos\\';
        $filename = $file->getClientOriginalName();

        Input::file('company_logo')->move($destinationPath, $filename);
        
        $company->company_logo = '/companylogos/' . $filename;

        DB::table('company')->insert(
            [
                'company_name' => $company->customer_name,
                'industry' => $company->industry,
                'pob' => $company->pob,
                'city' => $company->city,
                'zip' => $company->zip,
                'email' => $company->email,
                'tel' => $company->tel,
                'website' => $company->website,
                'location' => $company->location,
                'mobile_number' => $company->mobile_number,
                'company_img' => $company->company_logo,
                'company_id' => $company->company_id
            ]
        );*/

        // getting data of the contact numbers from client
        $company->company_id = Input::get('company_id');
        $company->contact_id = Input::get('selectedContactID');

        $company->conNumCount = Input::get('conNumCount');
        $company->contactType = explode(",",Input::get('contactType'));
        $company->contactNums = explode(",",Input::get('contactNums'));
        $company->defaultNum  = Input::get('defaultNum');

        // inserting contact_datails
        for ($i=0; $i < $company->conNumCount; $i++) {
            // getting default_no
            if($company->defaultNum == $i)
            {
                $defNum = 1;
            }
            else
            {
                $defNum = 0;                
            }
            //insert data
            DB::table('company_details')->insert(
                [
                    'company_id' => $company->company_id,
                    'type'       => $company->contactType[$i],
                    'value'      => $company->contactNums[$i],
                    'default_no' => $defNum
                ]
            );
        }

        return redirect()->route('info', ['type' => 'contacts', 'company_id' => $company->company_id, 'selectedContactID' => $company->contact_id]);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);

        $selectedContactID = request()->selectedContactID;// getting a company_id from ../{id}/edit?company_id=xxx
        $userId = auth()->user()->id;   //getting the logged in user id


        //update company_id in contact database.
        $update = array(
            'agent_id' => $userId
            );
            

        DB::table('company')
            ->where('company_id', $id)
            ->update($update);


        $contact_num = DB::table('company_details')
                     ->select('*')
                     ->where('company_id', '=', $id)
                     ->get();        

        $company_id = $id;

        return view('companies/edit', compact('company','contact_num','company_id','selectedContactID'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        $company->mobile_number = Input::get('mobile_number');
        $company->location = Input::get('location');
        $company->website = Input::get('website');
        $company->tel = Input::get('tel');
        $company->email = Input::get('email');
        $company->zip = Input::get('zip');
        $company->city = Input::get('city');
        $company->pob = Input::get('pob');
        $company->industry = Input::get('industry');
        $company->company_name = Input::get('customer_name');
        $company->where('company_id', '=', $company->company_id)->first()->update($request->all());
        if ($file = Input::file('company_logo')) {
            $destinationPath = public_path() . '\companylogos\\';
            $filename = $file->getClientOriginalName();
            Input::file('company_logo')->move($destinationPath, $filename);
            $company->company_img = '/companylogos/' . $filename;
        }
        $company->save();


        // getting data of the contact numbers from client

        $company_id = Input::get('company_id');
        $selectedContactID = Input::get('selectedContactID');

        $company->conNumCount = Input::get('conNumCount');
        $company->contactType = explode(",",Input::get('contactType'));
        $company->contactNums = explode(",",Input::get('contactNums'));
        $company->defaultNum  = Input::get('defaultNum');       

        $contact_num = DB::table('company_details')
                     ->select('id')
                     ->where('company_id', '=', $company_id)
                     ->get();        
        $get_id[]='';

        foreach($contact_num as $con_id)
        {
            $get_id[] = $con_id->id;
        }

        for ($i=0; $i < $company->conNumCount; $i++) 
        { 
            // getting default_no
            if($company->defaultNum == $i)
            {
                $defNum = 1;
            }
            else
            {
                $defNum = 0;                
            }

            //update data
            $update = array(
                'company_id' => $company_id,
                'type' => $company->contactType[$i],
                'value' => $company->contactNums[$i],
                'default_no' => $defNum);

            DB::table('company_details')
                ->where('id', $get_id[$i+1])
                ->update($update);
        }

        return redirect()->route('info', ['type' => 'contacts', 'company_id' => $company_id, 'selectedContactID' => $selectedContactID]);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
