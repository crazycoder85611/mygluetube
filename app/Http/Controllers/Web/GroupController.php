<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\CompaniesInGroup;
use Vanguard\Company;
use Vanguard\Contact;
use Vanguard\ContactsInGroup;
use Vanguard\Group;
use Vanguard\Http\Controllers\Controller;

class GroupController extends Controller
{

    public function index()
    {
        $groups = Group::where('group_creator_id', auth()->user()->id)->orWhere('public_or_not', 0)->orderBy('id', 'desc')->get();

        if (request()->type == 'company') {
            $title = 'Company Name: ' . Company::where('company_id', request()->client_id)->first()->company_name;
        } else {
            $title = 'Contact Name: ' . Contact::where('contact_id', request()->client_id)->first()->name;
        }
        $company = Contact::where('contact_id', request()->client_id)->first();

        $contacts = Contact::where('company_id', $company->company_id)->get();

        return view('add_group', compact('groups', 'contacts', 'title'));
    }

    public function store()
    {
        $check = Group::where(['name' => request()->name, 'group_creator_id' => auth()->user()->id])->get();
        if (count($check) == 0) {
            Group::create([
                'group_creator_id' => auth()->user()->id,
                'public_or_not' => true,
                'name' => request()->name,
            ]);
        } else {
            return back()->with('status_add_group_error', 'Group with same name already exist.');
        }

        return back()->with('status_add_group', 'New group successfully added.');;
    }

    public function add_group_type()
    {
        if (request()->type == 'contact') {

            $check = ContactsInGroup::where(['group_id' => request()->group_id, 'contact_id' => request()->contact_id, 'user_id' => auth()->user()->id])->get();
            if (count($check) == 0) {
                ContactsInGroup::create([
                    'group_id' => request()->group_id,
                    'contact_id' => request()->contact_id,
                    'user_id' => auth()->user()->id,
                ]);
            } else {
                return back()->with('status_add_in_group_error', 'This contact is already in this group.');
            }
        } else {
            $check = CompaniesInGroup::where(['group_id' => request()->group_id, 'company_id' => request()->client_id, 'user_id' => auth()->user()->id])->get();
            if (count($check) == 0) {
                CompaniesInGroup::create([
                    'group_id' => request()->group_id,
                    'company_id' => request()->client_id,
                    'user_id' => auth()->user()->id,
                ]);
            } else {
                return back()->with('status_add_in_group_error', 'This company is already in this group.');
            }
        }

        return back()->with('status_add_in_group', 'Contact successfully added.');
    }
}
