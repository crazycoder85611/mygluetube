<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Activity2;
use Vanguard\Http\Controllers\Controller;
use Vanguard\Http\Requests\UserMailDocumentRequest;
use Vanguard\Mail\UserMail;
use Vanguard\UserMailDocument;

class UserMailDocumentController extends Controller
{
    public function index()
    {
        $user_mail_documents = auth()->user()->user_mail_document;

        return view('usermail_with_file', compact('user_mail_documents'));
    }

    public function send(UserMailDocumentRequest $request)
    {
        $path = request()->file('file')->storeAs('public/user_documents', request()->file('file')->getClientOriginalName());
        $path = \Storage::url($path);

        UserMailDocument::create([
            'user_id' => auth()->user()->id,
            'path' => $path,
            'title' => $request->title,
        ]);

        Activity2::create([
            'date_time' => time(),
            'user_id' => auth()->user()->id,
            'action_type_id' => Activity2::documents_uploaded,
        ]);

        \Mail::to('andreysych@gmail.com')->send(new UserMail(request('title'), '', '', url($path)));

        return redirect('mail_with_file')->with('success', 'You successfully sended a mail.');
    }
}