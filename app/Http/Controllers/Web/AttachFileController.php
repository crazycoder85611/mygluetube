<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\AttachFile;
use Vanguard\Company;
use Vanguard\Contact;
use Vanguard\Http\Controllers\Controller;

class AttachFileController extends Controller
{
    public function index()
    {
        $attach_files = AttachFile::where(['type' => request()->type, 'company_id' => request()->client_id])->orderBy('id', 'desc')->get();

        if (request()->type == 'company') {
            $title = 'Company Name: ' . Company::where('company_id', request()->client_id)->first()->company_name;
        } else {
            $title = 'Contact Name: ' . Contact::where('contact_id', request()->client_id)->first()->name;
        }

        return view('attach_file', compact('attach_files', 'title'));
    }

    public function store()
    {
        $path = request()->file('file')->storeAs('public/attach_files', request()->file('file')->getClientOriginalName());
        $path = \Storage::url($path);

        AttachFile::create([
            'user_id' => auth()->user()->id,
            'company_id' => request()->company_id,
            'path' => $path,
            'title' => request()->title,
            'type' => request()->type,
        ]);

        return redirect()->route('attach_files', ['client_id' => request()->company_id, 'type' => request()->type]);
    }
}
