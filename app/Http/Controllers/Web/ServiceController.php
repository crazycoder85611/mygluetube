<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;
use Vanguard\Activity2;
use Auth;

class ServiceController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;
    }

    public function addService()
    {
        return view('add_service', [
            'services' => $this->project->getServices(),
        ]);
    }

    public function saveService(Request $request)
    {
        if ($errors = $this->validatorService($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->createProject($request->all());

        $this->project->addActivity(Activity2::NEW_SERVICE_CREATED, $request->all(), Auth::user()->id);

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Service successfully added');
    }

    private function validatorService($data)
    {
        $errors = Validator::make($data, [
            'title'      => 'required|string|min:1|max:255|unique:projects',
            'service_id' => 'required|exists:services,id',
            'client_id'  => 'required|integer',
        ])->errors();

        return $errors->messages();
    }
}
