<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Company;
use Vanguard\CompanyComment;
use Vanguard\Contact;
use Vanguard\Http\Controllers\Controller;

class CompanyCommentController extends Controller
{
    public function index()
    {
        $comments = CompanyComment::where(['company_id' => request()->client_id, 'type' => request()->type])->orderBy('id', 'desc')->get();

        if (request()->type == 'company') {
            $title = 'Company Name: ' . Company::where('company_id', request()->client_id)->first()->company_name;
        } else {
            $title = 'Contact Name: ' . Contact::where('contact_id', request()->client_id)->first()->name;
        }

        return view('add_comment_company', compact('comments', 'title'));
    }

    public function store()
    {
        CompanyComment::create([
           'user_id' => auth()->user()->id,
           'company_id' => request()->company_id,
           'comment' => request()->comment,
           'type' => request()->type,
        ]);

        return redirect()->route('add_comment_company', ['client_id' => request()->company_id, 'type' => request()->type]);
    }
}
