<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;
use Vanguard\Activity2;
use Auth;

class TimelineController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;
    }

    public function index()
    {
        $activities = Activity2::orderBy('id', 'desc')->paginate(20);

        return view('show-timeline', compact('activities'));
    }

    public function ajax_timeline()
    {
        return Activity2::orderBy('id', 'desc')->paginate(20);
    }

    public function editTimeline(Request $request)
    {
        return view('timeline', [
            'deliverables'    => $this->project->getServiceDeliverables($request->service_id),
            'allDeliverables' => $this->project->getDeliverablesKeyId(),
            'timeline'        => $this->project->getTimlineByServiceId($request->service_id),
            'project'         => $this->project->getProject($request->service_id),
        ]);
    }

    public function saveTimeline(Request $request)
    {
        $request['delivery_date'] = $request->input('delivery_date', []);

        if ($errors = $this->validatorSaveTimeline($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->saveTimeline($request->all());

        $this->project->addActivity(Activity2::TIMELINE_UPDATED, $request->all(), Auth::user()->id);

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Timeline change successfully');
    }

    private function validatorSaveTimeline($data)
    {
        $errors = Validator::make($data, [
            'service_id'          => 'required|exists:projects,id',
            'client_id'           => 'required|integer',
            'final_delivery_date' => 'date|nullable',
            'project_duration'    => 'string|nullable|max:255',
        ])->errors();

        $messages = $errors->messages();

        if ($data['delivery_date']) {
            foreach ($data['delivery_date'] as $id => $date) {
                $array = ['id' => $id, 'date' => $date];
                $er = Validator::make($array, [
                    'id'    => 'required|exists:deliverables,id',
                    'date'  => 'date|nullable',
                ])->errors();
                if ($er->messages()) {
                    $messages['delivery_date'][0] = 'Timeline is not valid';
                    break;
                }
            }
        }

        return $messages;
    }
}