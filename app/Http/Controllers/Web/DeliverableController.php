<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;

class DeliverableController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;
    }

    public function addServiceDeliverable(Request $request)
    {
        return view('deliverable', [
            'deliverables'       => $this->project->getDeliverables(),
            'selectDeliverables' => $this->project->getServiceDeliverables($request->service_id),
        ]);
    }

    public function saveServiceDeliverable(Request $request)
    {
        $request['deliverables'] = $request->input('deliverables', []);

        if ($errors = $this->validatorServiceDeliverable($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->saveServiceDeliverables($request->all());

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Deliverables add successfully');
    }

    private function validatorServiceDeliverable($data)
    {
        $errors = Validator::make($data, [
            'service_id'   => 'required|exists:projects,id',
            'client_id'    => 'required|integer',
        ])->errors();

        $messages = $errors->messages();

        if ($deliverable = $this->project->validationDeliverable($data['deliverables'])) {
            $messages['deliverables'][0] = $deliverable;
        }

        return $messages;
    }

    public function createServiceDeliverable(Request $request)
    {
        if ($errors = $this->validatorCreateServiceDeliverable($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->createDeliverable($request->all());

        return Redirect::back();
    }

    private function validatorCreateServiceDeliverable($data)
    {
        $errors = Validator::make($data, [
            'title'      => 'required|unique:deliverables,title',
            'service_id' => 'required|exists:projects,id',
            'client_id'  => 'required|integer',
        ])->errors();

        return $errors->messages();
    }
}
