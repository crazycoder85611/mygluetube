<?php

namespace Vanguard\Http\Controllers\Web;
use Vanguard\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Vanguard\Company;
use Input;
use DB;


class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        $company->mobile_number = Input::get('mobile_number');
        $company->location = Input::get('location');
        $company->website = Input::get('website');
        $company->tel = Input::get('tel');
        $company->email = Input::get('email');
        $company->zip = Input::get('zip');
        $company->city = Input::get('city');
        $company->pob = Input::get('pob');
        $company->industry = Input::get('industry');
        $company->customer_name = Input::get('customer_name');

        $file = Input::file('company_logo');
        $destinationPath = public_path() . '\companylogos\\';
        $filename = $file->getClientOriginalName();

        Input::file('company_logo')->move($destinationPath, $filename);
        
        $company->company_logo = '/companylogos/' . $filename;

        DB::table('company')->insert(
            [
                'company_name' => $company->customer_name,
                'industry' => $company->industry,
                'pob' => $company->pob,
                'city' => $company->city,
                'zip' => $company->zip,
                'email' => $company->email,
                'tel' => $company->tel,
                'website' => $company->website,
                'location' => $company->location,
                'mobile_number' => $company->mobile_number,
                'company_img' => $company->company_logo
            ]
        );

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        return view('companies/edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        $company->mobile_number = Input::get('mobile_number');
        $company->location = Input::get('location');
        $company->website = Input::get('website');
        $company->tel = Input::get('tel');
        $company->email = Input::get('email');
        $company->zip = Input::get('zip');
        $company->city = Input::get('city');
        $company->pob = Input::get('pob');
        $company->industry = Input::get('industry');
        $company->company_name = Input::get('customer_name');
        $company->where('company_id', '=', $company->company_id)->first()->update($request->all());
        if ($file = Input::file('company_logo')) {
            $destinationPath = public_path() . '\companylogos\\';
            $filename = $file->getClientOriginalName();
            Input::file('company_logo')->move($destinationPath, $filename);
            $company->company_img = '/companylogos/' . $filename;
        }
        $company->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
