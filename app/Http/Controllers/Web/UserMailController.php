<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Activity2;
use Vanguard\Http\Controllers\Controller;
use Vanguard\Http\Requests\UserMailRequest;
use Vanguard\Mail\UserMail;

class UserMailController extends Controller
{
    public function index()
    {
        return view('usermail');
    }

    public function send(UserMailRequest $request)
    {
        $path = null;
        if (request()->hasFile('file')) {
            $path = request()->file('file')->storeAs('public/user_documents', request()->file('file')->getClientOriginalName());
            $path = \Storage::url($path);
        }
        $type = request('type');

        \Vanguard\UserMail::create([
            'user_id' => auth()->user()->id,
            'title' => request('title'),
            'description' => request('description'),
            'type' => $type,
        ]);

        if ($type == 'Reimbursement Request') {
            Activity2::create([
                'date_time' => time(),
                'user_id' => auth()->user()->id,
                'action_type_id' => Activity2::reimburse_expense_email_sent,
            ]);
        }

        if ($type == 'Cash Advance') {
            Activity2::create([
                'date_time' => time(),
                'user_id' => auth()->user()->id,
                'action_type_id' => Activity2::cash_advance_email_sent,
            ]);
        }

        if ($type == 'Id Letter Request') {
            Activity2::create([
                'date_time' => time(),
                'user_id' => auth()->user()->id,
                'action_type_id' => Activity2::ID_letter_request_sent,
            ]);
        }
        \Mail::to('andreysych@gmail.com')->send(new UserMail(request('title'), request('description'), $type, url($path)));

        return redirect('mail')->with('status', 'You successfully sended a mail.');
    }
}
