<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;

class ProjectController extends Controller
{
    private $project;

    public function __construct(ProjectService $project)
    {
        $this->project = $project;
    }

    public function index(Request $request)
    {
        $projects = $this->project->getProjects(0, $request->sort, $request->sort_type, $request->stage);

        $title = 'Projects';

        $comments = $this->project->getComments($projects);

        return view('projects', compact('projects', 'title', 'comments'));
    }
}
