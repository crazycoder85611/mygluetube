<?php

namespace Vanguard\Http\Controllers\Web;

use Illuminate\Http\Request;
use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;
use Vanguard\ProjectEntry;
use Redirect;
use Auth;

use Vanguard\Repositories\ProjectRepository;
use Vanguard\Repositories\User\EloquentUser;
use Vanguard\Repositories\ProjectEntryRepository;
use Vanguard\Repositories\ProjectEntriesAgentRepository;
use Vanguard\Services\ProjectService;
use Vanguard\Repositories\ProjectEntriesCommentRepository;

class TaskController extends Controller
{
    private $project;
    private $user;
    private $projectEntry;
    private $projectEntriesAgent;
    private $projectService;
    private $projectEntriesComment;

    public function __construct(
        ProjectRepository $project,
        EloquentUser $user,
        ProjectEntryRepository $projectEntry,
        ProjectEntriesAgentRepository $projectEntriesAgent,
        ProjectService $projectService,
        ProjectEntriesCommentRepository $projectEntriesComment
    )
    {
        $this->project = $project;
        $this->user = $user;
        $this->projectEntry = $projectEntry;
        $this->projectEntriesAgent = $projectEntriesAgent;
        $this->projectService = $projectService;
        $this->projectEntriesComment = $projectEntriesComment;
    }

    public function index()
    {
        return view('task_index', [
            'tasks' => $this->projectEntry->all(),
        ]);
    }

    public function show(Request $request)
    {
        return view('task_show', [
            'task' => $this->projectEntry->findByUserId(Auth::user()->id, $request->task_id),
            'role' => Auth::user()->role->name,
        ]);
    }

    public function create(Request $request)
    {
        return view('task_create', [
            'project'      => $this->project->find($request->project_id),
            'users'        => $this->user->getUsersByStatus('Active', Auth::user()->id),
            'entry_status' => ProjectEntry::STATUS_ARRAY,
        ]);
    }

    public function store(Request $request)
    {

        $request['users'] = $request->input('users', [0]);
        $errors = $this->validatorStore($request->all());

        if ($errors) {
            return Redirect::back()->with('errors', $errors);
        }

            $projectId = $this->projectEntry->create([
                'priority' => 1,
                'entry_type' => 'Task',
                'project_id' => $request->project_id,
                'entry_title' => $request->title,
                'entry_status' => $request->entry_status,
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s'),
                'start_date' => date('Y-m-d'),
                'end_date' => $request->end_date,
                'date_completed' => '0000-00-00 00:00:00',
                'agent_id' => Auth::user()->id,
                'added_by' => 0,
            ]);

            if ($projectId) {

                foreach ($request->users as $user) {
                    $this->projectEntriesAgent->create([
                        'entry_id' => $projectId,
                        'agent_id' => $user,
                        'project_read' => 0,
                    ]);
                    $userData = $this->user->find($user);
                    $this->projectService->sent([
                        'email' => $userData->email,
                        'title' => 'New Task',
                        'body' => 'You have been assigned a task. Login to the system to check',
                        'from' => 'site',
                    ]);
                }
            }

        return Redirect::back()->with('success', 'success');

    }

    private function validatorStore($data)
    {
        $rules = [
            'title'          => 'required|string|min:1|max:255',
            'project_id'     => [
                'required',
                Rule::exists('projects', 'id')->where(function ($query) {})],
            'entry_status'   => Rule::in(ProjectEntry::STATUS_ARRAY),
            'end_date'       => 'required|date',
            'users.*'        => 'required|exists:users,id',
        ];

        $messages = [
            'users.*' => 'The selected users is invalid',
        ];

        $errors = Validator::make($data, $rules, $messages)->errors();

        return $errors->messages();
    }

    public function completed(Request $request)
    {
        $this->projectEntry->update($request->id, ['entry_status' => 'Completed']);

        return Redirect::back();
    }

    public function addComment()
    {
        return view('add_comment_for_task');
    }

    public function createComment(Request $request)
    {
        $errors = $this->validatorComment($request->all());

        if ($errors) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->projectEntriesComment->create([
            'ec_comment' => $request->comment,
            'ec_date'    => date('Y-m-d H:i:s', time()),
            'agent_id'   => Auth::user()->id,
            'entry_id'   => $request->entry_id,
        ]);

        return Redirect::route('task.show', ['task_id' => $request->entry_id]);
    }

    public function validatorComment($data)
    {
        $errors = Validator::make($data, [
            'comment' => 'required|string|min:1|max:255',
            'entry_id'=> 'required|exists:project_entries,entry_id',
        ])->errors();

        return $errors->messages();
    }

    public function delete(Request $request)
    {
        $this->checkRole('ABC');
        $this->projectEntry->delete($request->entry_id);
        $this->projectEntriesAgent->delete($request->entry_id);
        $this->projectEntriesComment->delete($request->entry_id);

        return Redirect::route('task.index');
    }

    public function edit(Request $request)
    {
        $this->checkRole('ABC');
        return view('task_update', [
            'task'         => $this->projectEntry->find($request->task_id),
            'users'        => $this->user->getUsersByStatus('Active', Auth::user()->id),
            'entry_status' => ProjectEntry::STATUS_ARRAY,
        ]);
    }

    public function save(Request $request)
    {
        $this->checkRole('ABC');
        $request['users'] = $request->input('users', [0]);
        $errors = $this->validatorSave($request->all());

        if ($errors) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->projectEntry->update($request->task_id, [
            'entry_title' => $request->title,
            'end_date'    => $request->end_date,
        ]);

        $this->projectEntriesAgent->delete($request->task_id);

        foreach ($request->users as $user) {
            $this->projectEntriesAgent->create([
                'entry_id' => $request->task_id,
                'agent_id' => $user,
                'project_read' => 0,
            ]);
        }

        return Redirect::back()->with('success', 'success');
    }

    private function validatorSave($data)
    {
        $rules = [
            'title'        => 'required|string|min:1|max:255',
            'task_id'      => 'required|exists:project_entries,entry_id',
            'entry_status' => Rule::in(ProjectEntry::STATUS_ARRAY),
            'end_date'     => 'required|date',
            'users.*'      => 'required|exists:users,id',
        ];

        $messages = [
            'users.*' => 'The selected users is invalid',
        ];

        $errors = Validator::make($data, $rules, $messages)->errors();

        return $errors->messages();
    }

    private function checkRole($role)
    {
        if (Auth::user()->role->name != $role) {
            abort(403);
        }
    }
}
