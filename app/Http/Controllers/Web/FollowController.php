<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Vanguard\Services\ProjectService;
use Redirect;
use Vanguard\Activity2;
use Auth;

class FollowController extends Controller
{
    private $project;

    public function __construct(Request $request, ProjectService $project)
    {
        $this->project = $project;
    }

    public function index(Request $request)
    {
        return view('follow', [
            'email' => $this->project->getEmail($request->client_id),
        ]);
    }

    public function sent(Request $request)
    {
        $this->project->sent($request->all());

        $this->project->addActivity(Activity2::PROPOSAL_FOLLOWUP_EMAIL_SENT, $request->all());

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Message sent');
    }

    public function addComment(Request $request)
    {
        return view('add_comment', [
            'old_comments' => $this->project->getCommentsByProjectId($request->service_id),
        ]);
    }

    public function saveComment(Request $request)
    {
        if ($errors = $this->validatorAddComment($request->all())) {
            return Redirect::back()->with('errors', $errors);
        }

        $this->project->addComment($request->all(), Auth::user()->id);

        return Redirect::route('client_projects', ['id' => $request->client_id])
            ->with('message', 'Comment add successfully');
    }

    public function validatorAddComment($data)
    {
        $errors = Validator::make($data, [
            'service_id'=> 'required|exists:projects,id',
            'client_id' => 'required|integer',
            'comment'   => 'string|max:255',
        ])->errors();

        return $errors->messages();
    }
}
