<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class ProjectEntriesComment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'ec_comment',
        'ec_date',
        'agent_id',
        'entry_id',
    ];
}
