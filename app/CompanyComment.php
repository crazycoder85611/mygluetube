<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class CompanyComment extends Model
{
    protected $guarded = [];
}
