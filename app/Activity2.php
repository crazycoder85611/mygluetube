<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Activity2 extends Model
{
    const NEW_SERVICE_CREATED = 1;
    const STATUS_CHANGED_TO_PROPOSAL_REQUESTED = 2;
    const DELIVERABLES_UPDATED = 3;
    const TIMELINE_UPDATED = 4;
    const FINANCIAL_COST_UPDATED = 5;
    const PAYMENT_TERMS_UPDATED = 6;
    const STATUS_CHANGED_TO_SUBMIT_PROPOSAL = 7;
    const PROPOSAL_FOLLOWUP_EMAIL_SENT = 8;
    const STATUS_CHANGED_TO_REVISE_PROPOSAL = 9;
    const STATUS_CHANGED_TO_ACCEPT_PROPOSAL = 10;
    const STATUS_CHANGED_TO_FINAL_CUSTOMER_APPROVAL = 11;
    const STATUS_CHANGED_TO_PROJECT_DELIVERY = 12;
    const STATUS_CHANGED_TO_CLOSE_AND_ARCHIVE = 13;
    const reimburse_expense_email_sent = 14;
    const cash_advance_email_sent = 15;
    const ID_letter_request_sent = 16;
    const documents_uploaded = 17;
    const file_sent_to_client = 18;

    public $table = 'activity';

    public $timestamps = false;

    protected $fillable = [
        'date_time',
        'user_id',
        'action_type_id',
        'client_id',
        'service_id',
    ];
    protected $appends = ['name', 'const', 'date', 'project_title', 'company_name'];

    public function getConstAttribute()
    {
        $fooClass = new \ReflectionClass( get_class($this) );
        $constants = $fooClass->getConstants();
        $constName = null;
        foreach ( $constants as $name => $value )
        {
            if ( $value == $this->action_type_id )
            {
                $constName = $name;
                break;
            }
        }

        return str_replace('_', ' ', $constName);
    }

    public function getDateAttribute()
    {
        return date('d.m.Y H:i', $this->date_time);
    }

    public function getCompanyNameAttribute()
    {
        return $this->client ? $this->client->company_name : '';
    }

    public function getNameAttribute()
    {
        return $this->user ? '<img style="width: 35px;" class="img-circle avatar" src="' . $this->user->present()->avatar . '"/> ' . (($this->user->first_name) ? $this->user->first_name : $this->user->username) : '';
    }

    public function getProjectTitleAttribute()
    {
        return $this->project ? $this->project->title : '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'service_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Company::class, 'client_id', 'company_id');
    }
}
