<?php

namespace Vanguard\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminSendFileMail extends Mailable
{
    use Queueable, SerializesModels;

    public $text;
    public $title;
    public $from1;
    public $path;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $title, $text, $path)
    {
        $this->from1 = $from;
        $this->title = $title;
        $this->text = $text;
        $this->path = $path;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->path == null) {
            return $this->from($this->from1)->subject($this->title)->view('emails.admin_send_file');
        }
        return $this->from($this->from1)->subject($this->title)->view('emails.admin_send_file')->attach($this->path);
    }
}
