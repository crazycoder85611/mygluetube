<?php

namespace Vanguard\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserMail extends Mailable
{
    use Queueable, SerializesModels;

    public $text;
    public $title;
    public $type;
    public $path;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $text, $type, $path = null)
    {
        $this->title = $title;
        $this->text = $text;
        $this->type = $type;
        $this->path = $path;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->path != null) {
            return $this->subject($this->type . ' ' . $this->title)->view('emails.usermail')->attach($this->path);
        }
        return $this->subject($this->type . ' ' . $this->title)->view('emails.usermail');
    }
}
