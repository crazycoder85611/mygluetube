<?php
namespace Vanguard\Console\Commands;
use Illuminate\Console\Command;
use DB;

use Vanguard\Services\ProjectService;

class TaskMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task_messages';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task Messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $project;

    public function __construct(ProjectService $project)
    {
        parent::__construct();

        $this->project = $project;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->project->reminder();

    }
}
