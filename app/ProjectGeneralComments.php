<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class ProjectGeneralComments extends Model
{
    public $timestamps = false;

    protected $table = 'project_general_comments';

    protected $fillable = [
        'comment',
        'logged_in_user_id',
        'project_id',
        'datatime',
    ];
}
