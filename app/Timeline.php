<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    public $table = 'timeline';

    public $timestamps = false;

    protected $fillable = [
        'datetimeadded',
        'service_id',
        'deliverable_id',
        'delivery_date',
    ];
}
