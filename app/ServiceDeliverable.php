<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class ServiceDeliverable extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'deliverable_id',
        'service_id',
        'client_id',
        'date_time',
    ];
}
