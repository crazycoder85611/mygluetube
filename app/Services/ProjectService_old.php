<?php

namespace Vanguard\Services;

use Vanguard\Repositories\ClientRepository;
use Vanguard\Repositories\CompanyRepository;
use Vanguard\Repositories\ContactDetailRepository;
use Vanguard\Repositories\ContactRepository;
use Vanguard\Repositories\ProjectRepository;
use Vanguard\Repositories\ServiceRepository;
use Vanguard\Repositories\DeliverableRepository;
use Vanguard\Repositories\ServiceDeliverableRepository;
use Vanguard\Repositories\TimelineRepository;
use Vanguard\Repositories\ActivityRepository;
use Vanguard\Repositories\WatchListRepository;
use Vanguard\Repositories\ProjectGeneralCommentsRepository;
use Vanguard\Repositories\ProjectEntryRepository;

class ProjectService
{
    private $project;
    private $service;
    private $deliverable;
    private $serviceDeliverable;
    private $timeline;
    private $activity;
    private $client;
    private $company;
    private $contact;
    private $contactDetail;
    private $watchList;
    private $projectGeneralComments;
    private $projectEntry;

    private $countOnePage = 10;

    public function __construct(
        ProjectRepository $project,
        ServiceRepository $service,
        DeliverableRepository $deliverable,
        ServiceDeliverableRepository $serviceDeliverable,
        TimelineRepository $timeline,
        ActivityRepository $activity,
        ClientRepository $client,
        CompanyRepository $company,
        ContactRepository $contact,
        ContactDetailRepository $contactDetail,
        WatchListRepository $watchList,
        ProjectGeneralCommentsRepository $projectGeneralComments,
        ProjectEntryRepository $projectEntry
    )
    {
        $this->project = $project;
        $this->service = $service;
        $this->deliverable = $deliverable;
        $this->serviceDeliverable = $serviceDeliverable;
        $this->timeline = $timeline;
        $this->activity = $activity;
        $this->client = $client;
        $this->company = $company;
        $this->contact = $contact;
        $this->contactDetail = $contactDetail;
        $this->watchList = $watchList;
        $this->projectGeneralComments = $projectGeneralComments;
        $this->projectEntry = $projectEntry;
    }

    public function getProjects($clientId, $sort, $sortType)
    {
        if ($sortType) {
            $sortType = 'asc';
        } else {
            $sortType = 'desc';
        }
        if (!$sort) {
            $sort = 'id';
        }

        return $this->project->getByClientIdSortLimit($clientId, $this->countOnePage, $sort, $sortType);
    }

    public function getProjectsPagination()
    {
        return $this->project->getPagination($this->countOnePage);
    }

    public function getServices()
    {
        return $this->service->all();
    }

    public function createProject($data)
    {
        $result = $this->project->create([
            'title'             => $data['title'],
            'service_id'        => $data['service_id'],
            'client_id'         => $data['client_id'],
            'date_time_created' => time(),
            'payment_terms'     => '',
        ]);

        return $result->id;
    }

    public function getProject($id)
    {
        return $this->project->get($id);
    }

    public function updateProjectStage($id, $data)
    {
        return $this->project->update($id, [
            'stage'     => $data['stage'],
            'client_id' => $data['client_id'],
        ]);
    }

    public function getDeliverables()
    {
        return $this->deliverable->all();
    }

    public function getDeliverablesKeyId()
    {
        return $this->deliverable->all()
            ->keyBy('id');
    }

    public function validationDeliverable($deliverables)
    {
        $messages = '';

        if ($deliverables) {
            $result = $this->deliverable->getWhereIn($deliverables);

            if (count($result) != count($deliverables)) {
                $messages = 'Deliverables is not valid';
            }
        }

        return $messages;
    }

    public function saveServiceDeliverables($data)
    {
        $this->serviceDeliverable->deleteWhereServiceId($data['service_id']);

        if ($data['deliverables']) {
            foreach($data['deliverables'] as $deliverable) {
                $this->serviceDeliverable->create([
                    'deliverable_id' => $deliverable,
                    'service_id'     => $data['service_id'],
                    'client_id'      => $data['client_id'],
                    'date_time'      => time(),
                ]);
            }
        }
    }

    public function getServiceDeliverables($id)
    {
        return $this->serviceDeliverable->getWhereServiceId($id)
            ->keyBy('deliverable_id');
    }

    public function saveTimeline($data)
    {
        if ($data['delivery_date']) {
            foreach ($data['delivery_date'] as $id => $date) {
                if ($date) {
                    $this->timeline->updateOrCreate([
                        'service_id'     => $data['service_id'],
                        'deliverable_id' => $id,
                    ], [
                        'datetimeadded' => time(),
                        'delivery_date' => strtotime($date . ' 00:00:00'),
                    ]);
                }
            }
        }

        $this->project->update($data['service_id'], [
            'final_delivery_date' => ($data['final_delivery_date']) ? strtotime($data['final_delivery_date']) : 0,
            'project_duration'    => $data['project_duration'],
            'client_id'           => $data['client_id'],
        ]);
    }

    public function getTimlineByServiceId($id)
    {
        return $this->timeline->getWhereServiceId($id)->keyBy('deliverable_id');
    }

    public function updateFinancialCost($data)
    {
        return $this->project->update($data['service_id'], [
            'estimate_cost' => $data['estimate_cost'],
            'client_id'     => $data['client_id'],
        ]);
    }

    public function updatePaymentTerms($data)
    {
        return $this->project->update($data['service_id'], [
            'payment_terms' => $data['payment_terms'],
            'client_id'     => $data['client_id'],
        ]);
    }

    public function addActivity($id, $data, $userId)
    {
        $this->activity->create([
            'date_time'      => time(),
            'user_id'        => $userId,
            'action_type_id' => $id,
            'client_id'      => $data['client_id'],
            'service_id'     => $data['service_id'],
        ]);
    }

    public function createDeliverable($data)
    {
        $this->deliverable->create([
            'title'       => $data['title'],
            'dateandtime' => time(),
        ]);
    }

    public function sent($data)
    {
        $to = $data['email'];
        $subject = $data['title'];
        $message = $data['body'];
        $headers = 'From: ' . $data['from'] . "\r\n" .
            'Reply-To: ' . $data['from'] . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        return mail($to, $subject, $message, $headers);
    }

    public function getEmail($id)
    {
        $data = $this->client->get($id);

        $email = '';

        if ($data) {
            $email = $data->email;
        }

        return $email;
    }

    public function getCompaniesOrContacts($type, $companyId, $clientId)
    {
        $data = [];

        if ($type == 'companies') {

            if ($companyId) {
                $data['contacts'] = $this->getContactsById($companyId);
                $data['details'] = $this->getContactDetails($data['contacts']);
            }

            if ($clientId) {
                list($title, $contact_details) = $this->getContactDetail($clientId);
                $data['contact_details'] = $contact_details;
                $data['title'] = $title;
                $data['projects'] = $this->getProjectForInfo($clientId);
            }

            $data['data'] = $this->company->all();
        } else {
            $data['data'] = $this->contact->all();

            if ($clientId) {
                list($title, $contact_details) = $this->getContactDetail($clientId);
                $data['contact_details'] = $contact_details;
                $data['title'] = $title;
                $data['projects'] = $this->getProjectForInfo($clientId);
            }
        }

        return $data;
    }

    public function getContactsById($id)
    {
        return $this->contact->getByCompanyId((int)$id);

    }

    public function getContactDetail($id)
    {

        $contact = $this->contact->get($id);
        $title = '';

        if ($contact) {
            $company = $this->company->get($contact->company_id);

            if ($company) {
                $title = $company->company_name;
            }


        }

        $details = $this->contactDetail->getByContactId((int)$id);

        return [$title, $details];
    }

    public function getContactDetails($data)
    {
        $detailsData = [];

        if ($data) {
            $ids = [];

            foreach ($data as $d) {
                $ids[] = $d->contact_id;
            }

            $details = $this->contactDetail->getInContactId($ids);
            $detailsName = $details->unique('type')
                ->pluck('type');

            if ($details) {

                foreach ($details as $detail) {

                    if (!isset($detailsData[$detail->contact_id])) {

                        foreach ($detailsName as $detailName) {
                            $detailsData[$detail->contact_id][$detailName] = '';
                        }
                    }

                    $detailsData[$detail->contact_id][$detail->type] = $detail->value;
                }
            }
        }

        return $detailsData;
    }

    public function getProjectForInfo($id)
    {
        return $this->project->getByClientId($id);
    }

    public function addWatch($id, $userId)
    {
        $messages = 'Contact is already in watch list';

        if (!$this->watchList->getByContactId($id)) {
            $this->watchList->create([
                'contact_id'        => $id,
                'logged_in_user_id' => $userId,
            ]);
            $messages = 'Contact added to watch list';
        }

        return $messages;
    }

    public function addComment($data, $userId)
    {
        //dd($data);
        $this->projectGeneralComments->create([
            'comment'           => $data['comment'],
            'logged_in_user_id' => $userId,
            'project_id'        => $data['service_id'],
            'datatime'          => time(),
        ]);
    }

    public function getComments($data)
    {
        $ids = [];

        if ($data) {
            foreach ($data as $d) {
                $ids[] = $d->id;
            }
        }
        $dataComments = [];

        $comments = $this->projectGeneralComments->getByProjectIds($ids);

        if ($comments) {

            foreach ($comments as $comment) {

                if (!isset($dataComments[$comment->project_id])) {
                    $dataComments[$comment->project_id] = $comment->comment;
                }
            }
        }
        return $dataComments;
    }

    public function getCommentsByProjectId($id)
    {
        return $this->projectGeneralComments->getByProjectId($id);
    }

    public function reminder()
    {
        $data = $this->projectEntry->getByDateEnd();

        if ($data) {

            foreach ($data as $d) {

                foreach ($d->entriesAgents as $entriesAgent) {
                    $messages = 'Task ' . $d->entry_title . ' is due in 3 days';
                    $field = 'message_three_days';

                    if ($d->end_date == date("Y-m-d", time())) {
                        $messages = 'Task ' . $d->entry_title . ' is due today';
                        $field = 'message_final';
                    }

                    $send = $this->sent([
                        'email' => $entriesAgent->user->email,
                        'title' => 'Task',
                        'body' => $messages,
                        'from' => 'site',
                    ]);

                    if ($send) {
                        $this->projectEntry->updateMessageFlag($d->entry_id, $field);
                    }
                }
            }
        }
    }
}