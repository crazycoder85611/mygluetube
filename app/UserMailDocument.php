<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class UserMailDocument extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
