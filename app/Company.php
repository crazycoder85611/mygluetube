<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $primaryKey = 'company_id';
	public $timestamps = false;
    protected $table = 'company';

    protected $fillable = [
    	'mobile_number',
        'location',
        'website',
        'tel',
        'email',
        'zip',
        'city',
        'pob',
        'industry',
        'company_name',
        'company_img'
    ];
}
