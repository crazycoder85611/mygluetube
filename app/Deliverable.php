<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Deliverable extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'dateandtime',
    ];
}
