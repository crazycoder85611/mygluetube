<?php

namespace Vanguard\Repositories;

use Vanguard\ProjectEntry;

class ProjectEntryRepository
{
    public function all($id)
    {	
        return ProjectEntry::with('entriesAgents')->get();
    }

    public function create($data)
    {
        $project = ProjectEntry::create($data);

        return $project->id;
    }

    public function getByDateEnd()
    {
        return ProjectEntry::with('entriesAgents')->where('end_date', date("Y-m-d", time() + 259200))
            ->where('message_three_days', 0)
            ->orWhere('end_date', date("Y-m-d", time()))
            ->where('message_final', 0)
            ->get();
    }

    public function updateMessageFlag($id, $filed)
    {
        return ProjectEntry::where('entry_id', $id)->update([$filed => 1]);
    }

    public function findByUserId($userId, $entryId)
    {
        return ProjectEntry::with('entriesAgents', 'entriesComments')->where('agent_id', $userId)
            ->where('entry_id', $entryId)
            ->first();
    }

    public function update($id, $data)
    {
        return ProjectEntry::where('entry_id', $id)->update($data);
    }

    public function delete($id)
    {
        return ProjectEntry::where('entry_id', $id)->delete();
    }

    public function find($id)
    {
        return ProjectEntry::with('entriesAgents')->where('entry_id', $id)
            ->first();
    }

    public function getByProjectId($projectId)
    {
        return ProjectEntry::when('entriesComments')->where('project_id', $projectId)->get();
    }
}