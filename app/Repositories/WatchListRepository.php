<?php

namespace Vanguard\Repositories;

use Vanguard\WatchList;

class WatchListRepository
{
    public function create($data)
    {
        return WatchList::create($data);
    }

    public function getByContactId($contactId)
    {
        return WatchList::where('contact_id', $contactId)->first();
    }
}