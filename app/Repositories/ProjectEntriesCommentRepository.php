<?php

namespace Vanguard\Repositories;

use Vanguard\ProjectEntriesComment;

class ProjectEntriesCommentRepository
{
    public function create($data)
    {
        return ProjectEntriesComment::create($data);
    }

    public function delete($id)
    {
        return ProjectEntriesComment::where('entry_id', $id)->delete();
    }
}