<?php

namespace Vanguard\Repositories;

use Vanguard\ProjectGeneralComments;

class ProjectGeneralCommentsRepository
{
    public function getByProjectIds($ids)
    {
        return ProjectGeneralComments::whereIn('project_id', $ids)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function create($data)
    {
        return ProjectGeneralComments::create($data);
    }

    public function getByProjectId($id)
    {
        return ProjectGeneralComments::where('project_id', $id)->get();
    }
}