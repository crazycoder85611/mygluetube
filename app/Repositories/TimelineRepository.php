<?php

namespace Vanguard\Repositories;

use Vanguard\Timeline;

class TimelineRepository
{
    public function updateOrCreate($where, $data)
    {
        return Timeline::updateOrCreate($where, $data);
    }

    public function getWhereServiceId($id)
    {
        return Timeline::where('service_id', $id)->get();
    }
}