<?php

namespace Vanguard\Repositories;

use Vanguard\Service;

class ServiceRepository
{
    public function all()
    {
        return Service::all();
    }
}