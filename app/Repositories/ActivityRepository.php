<?php

namespace Vanguard\Repositories;

use Vanguard\Activity2;

class ActivityRepository
{
    public function create($data)
    {
        return Activity2::create($data);
    }
}