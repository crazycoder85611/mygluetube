<?php

namespace Vanguard\Repositories;

use Vanguard\Contact;

class ContactRepository
{
    public function all()
    {
        return Contact::orderBy('name')->get();
    }

    public function getByCompanyId($id)
    {
        return Contact::where('company_id', $id)->get();
    }

    public function get($id)
    {
        return Contact::where('contact_id', $id)->first();
    }
}