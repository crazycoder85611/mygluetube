<?php

namespace Vanguard\Repositories;

use Vanguard\ContactDetail;

class ContactDetailRepository
{
    public function all()
    {
        return ContactDetail::all();
    }

    public function getByContactId($id)
    {
        return ContactDetail::where('contact_id', $id)->get();
    }

    public function getInContactId($ids)
    {
        return  ContactDetail::whereIn('contact_id', $ids)->get();
    }
}