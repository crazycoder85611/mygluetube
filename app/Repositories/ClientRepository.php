<?php

namespace Vanguard\Repositories;

use Vanguard\Client;

class ClientRepository
{
    public function get($id)
    {
        return Client::find($id);
    }
}