<?php

namespace Vanguard\Repositories;

use Vanguard\ProjectEntriesAgent;

class ProjectEntriesAgentRepository
{
    public function create($data)
    {
        $project = ProjectEntriesAgent::create($data);

        return $project->id;
    }

    public function delete($id)
    {
        return ProjectEntriesAgent::where('entry_id', $id)->delete();
    }
}