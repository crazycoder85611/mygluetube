<?php

namespace Vanguard\Repositories;

use Vanguard\ServiceDeliverable;

class ServiceDeliverableRepository
{
    public function deleteWhereServiceId($id)
    {
        return ServiceDeliverable::where('service_id', $id)->delete();
    }

    public function create($data)
    {
        return ServiceDeliverable::create($data);
    }

    public function getWhereServiceId($id)
    {
        return ServiceDeliverable::where('service_id', $id)->get();
    }
}