<?php

namespace Vanguard\Repositories;

use Vanguard\Company;

class CompanyRepository
{
    public function all()
    {
        return Company::orderBy('company_name')->get();
    }

    public function get($id)
    {
        return Company::where('company_id', $id)->first();
    }
}