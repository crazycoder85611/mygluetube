<?php

namespace Vanguard\Repositories;

use Vanguard\Project;

class ProjectRepository
{
    public function all($limit, $sort, $sortType)
    {
        return Project::orderBy($sort, $sortType)
            ->paginate($limit);
    }

    public function getByClientIdSortLimit($clientId, $limit, $sort, $sortType)
    {
        return Project::where('client_id', $clientId)->orderBy($sort, $sortType)
            ->paginate($limit);
    }

    public function getByClientId($clientId)
    {
        return Project::with('projectEntries')->where('client_id', $clientId)->get();
    }

    public function create($data)
    {
        return Project::create($data);
    }

    public function get($id)
    {
        return Project::find($id);
    }

    public function update($id, $data)
    {
        return Project::where('id', $id)->update($data);
    }

    public function find($id)
    {
        return Project::find($id);
    }
}