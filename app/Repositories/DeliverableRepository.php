<?php

namespace Vanguard\Repositories;

use Vanguard\Deliverable;

class DeliverableRepository
{
    public function all()
    {
        return Deliverable::all();
    }

    public function getWhereIn($ids)
    {
        return Deliverable::whereIn('id', $ids)
            ->get();
    }

    public function create($data)
    {
        return Deliverable::create($data);
    }
}