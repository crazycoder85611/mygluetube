<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'service_id',
        'client_id',
        'date_time_created',
        'payment_terms',
    ];

    public function contact()
    {
        return $this->hasOne('Vanguard\Contact', 'contact_id', 'client_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'client_id', 'company_id');
    }

    public function projectEntries()
    {
        return $this->hasMany('Vanguard\ProjectEntry', 'project_id', 'id');
    }
}
