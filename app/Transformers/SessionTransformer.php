<?php

namespace Vanguard\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Vanguard\User;

class SessionTransformer extends TransformerAbstract
{
    public function transform($session)
    {
        $agent = app('agent');
        $agent->setUserAgent($session->user_agent);

        return [
            'id' => $session->id,
            'user_id' => (int) $session->user_id,
            'ip_address' => $session->ip_address,
            'user_agent' => $session->user_agent,
            'browser' => $agent->browser(),
            'platform' => $agent->platform(),
            'device' => $agent->device(),
            'last_activity' => (string) Carbon::createFromTimestamp($session->last_activity)
        ];
    }
}
