<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class WatchList extends Model
{
    public $timestamps = false;

    protected $table = 'watch_list';

    protected $fillable = [
        'contact_id',
        'logged_in_user_id',
        'type',
    ];
}
