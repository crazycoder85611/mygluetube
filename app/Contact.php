<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $primaryKey = 'contact_id';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'job_title',
        'department',
        'city',
        'department',
        'email',
        'phone_number',
        'mobile_number',
        'location'
    ];

    public function contact_details()
    {
        return $this->hasMany(ContactDetail::class, 'contact_id', 'contact_id');
    }

    public function comments()
    {
        return $this->hasMany(CompanyComment::class, 'company_id', 'contact_id');
    }
}
