<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class ProjectEntry extends Model
{
    const STATUS_NOT_STARTED = 'Not started';
    const STATUS_IN_PROGRESS = 'In progress';
    const STATUS_COMPLETED = 'Completed';
    const STATUS_DELIVER = 'Deliver';
    const STATUS_REVIEW = 'Review';
    const STATUS_QUALITY_CHECK = 'Quality check';

    const STATUS_ARRAY = [
        self::STATUS_NOT_STARTED,
        self::STATUS_IN_PROGRESS,
        self::STATUS_COMPLETED,
        self::STATUS_DELIVER,
        self::STATUS_REVIEW,
        self::STATUS_QUALITY_CHECK,
    ];

    protected $table = 'project_entries';

    public $timestamps = false;

    protected $fillable = [
        'priority',
        'entry_type',
        'project_id',
        'entry_title',
        'entry_status',
        'created',
        'updated',
        'start_date',
        'end_date',
        'date_completed',
        'agent_id',
        'added_by',
        'message_three_days',
        'message_final',
    ];

    public function entriesAgents()
    {
        return $this->hasMany(ProjectEntriesAgent::class, 'entry_id', 'entry_id');
    }

    public function entriesComments()
    {
        return $this->hasMany(ProjectEntriesComment::class, 'entry_id', 'entry_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'agent_id');
    }
}
