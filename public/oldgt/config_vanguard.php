<?php
header('Cache-Control: no-store');
if (PHP_MAJOR_VERSION < 7) die("Minimum PHP version is 7.0. Your version: " . PHP_VERSION . " Update needed. target=\"_blank\"></a>.");

// This should be equal to: PATH_TO_VANGUARD_FOLDER/extra/auth.php
require_once ('../../extra/auth.php');

require_once("../../vendor/autoload.php");


try {

    // Here we just check if user is not 
    // logged in, and in that case we redirect
    // the user to vanguard login page.
    if (Auth::check()===true) {
        $user = Auth::user();
    }

/*
    if (empty($GLOBALS["AppConfig"]->AuthenticatedUser)) {
        throw new Exception("Access denied");
    }
*/

} catch (Exception $e) {
    die($e->getMessage());
}

function configAutoload(\AppConfig & $AppConfig)
{
    $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(dirname(__FILE__) . "/config"));
    RecursiveIteratorIterator::SELF_FIRST;
    foreach ($objects as $name => $object) {
        if (!$object->isDir() && pathinfo($object->getPathname(), PATHINFO_EXTENSION) == "php") {
            $filename = pathinfo($object->getPathname(), PATHINFO_FILENAME);
            $AppConfig->$filename = require($name);
        }
    }
}