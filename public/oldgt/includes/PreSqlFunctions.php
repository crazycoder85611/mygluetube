<?php
require_once"EasyFunctions.php";
$zSERVERNAME = $_SERVER["SERVER_NAME"];
if ($zSERVERNAME =='localhost') $localrunning=true;else $localrunning=false;
//$localrunning=true;

global $HandleErrors;
if ((isset($HandleErrors)) && ($HandleErrors)){
	//require_once dirname(__FILE__)."/ErrorHandler.php";
} 

function GetInsertQueryWithEscape($Table, $array,$ReplaceNow=true,$EscapeString=true){
	
	$comma="";
	$fields="";
	$values="";
	
   foreach ($array as $field=>$value){
	 if ($EscapeString){
		$value=mysql_escape_string($value);
	 }		
		$fields.=$comma."$field";
		$values.=$comma."'$value'";
		$comma=" , ";
	 }
	 
	$sql  = "INSERT INTO $Table ($fields) VALUES ($values) "; 
	  if ($ReplaceNow){
		$sql=str_replace("'now()'","now()",$sql);
	 }
	 
	 
	 return $sql;
	 
}
function GetInsertQuery($Table, $array,$ReplaceNow=true){
 $sql  = "INSERT INTO $Table";

   // implode keys of $array...
   $sql .= " (`".implode("`, `", array_keys($array))."`)";

   // implode values of $array...
   $sql .= " VALUES ('".implode("', '", $array)."') ";
	 if ($ReplaceNow){
		$sql=str_replace("'now()'","now()",$sql);
	 }
	 return $sql;
	 }
	 
function GetUpdateQuery($Table, $array,$where){
 $sql  = "UPDATE $Table SET ";
 $comma="";
   foreach ($array as $field=>$value){
		$sql.=$comma."$field='$value'";
		$comma=",";
	 }
	 
	 
	 if ($where){
		$where=" WHERE ".$where;
	 }
	 $sql.=$where;
	 return $sql;
	 }
function GetUSStatesArray($AddSelect=true){

	if ($AddSelect) $arrstate[""]="Select your state";
    $arrstate["AL"]="ALABAMA";
    $arrstate["AK"]="ALASKA";
    $arrstate["AS"]="AMERICAN SAMOA";
    $arrstate["AZ"]="ARIZONA";
    $arrstate["AR"]="ARKANSAS";
    $arrstate["CA"]="CALIFORNIA";
    $arrstate["CO"]="COLORADO";
    $arrstate["CT"]="CONNECTICUT";
    $arrstate["DE"]="DELAWARE";
    $arrstate["DC"]="Washington DC";
    $arrstate["FM"]="FSM";
    $arrstate["FL"]="FLORIDA";
    $arrstate["GA"]="GEORGIA";
    $arrstate["GU"]="GUAM";
    $arrstate["HI"]="HAWAII";
    $arrstate["ID"]="IDAHO";
    $arrstate["IL"]="ILLINOIS";
    $arrstate["IN"]="INDIANA";
    $arrstate["IA"]="IOWA";
    $arrstate["KS"]="KANSAS";
    $arrstate["KY"]="KENTUCKY";
    $arrstate["LA"]="LOUISIANA";
    $arrstate["ME"]="MAINE";
    $arrstate["MH"]="MARSHALL ISLANDS";
    $arrstate["MD"]="MARYLAND";
    $arrstate["MA"]="MASSACHUSETTS";
    $arrstate["MI"]="MICHIGAN";
    $arrstate["MN"]="MINNESOTA";
    $arrstate["MS"]="MISSISSIPPI";
    $arrstate["MO"]="MISSOURI";
    $arrstate["MT"]="MONTANA";
    $arrstate["NE"]="NEBRASKA";
    $arrstate["NV"]="NEVADA";
    $arrstate["NH"]="NEW HAMPSHIRE";
    $arrstate["NJ"]="NEW JERSEY";
    $arrstate["NM"]="NEW MEXICO";
    $arrstate["NY"]="NEW YORK";
    $arrstate["NC"]="NORTH CAROLINA";
    $arrstate["ND"]="NORTH DAKOTA";
    $arrstate["MP"]="NMI";
    $arrstate["OH"]="OHIO";
    $arrstate["OK"]="OKLAHOMA";
    $arrstate["OR"]="OREGON";
    $arrstate["PW"]="PALAU";
    $arrstate["PA"]="PENNSYLVANIA";
    $arrstate["PR"]="PUERTO RICO";
    $arrstate["RI"]="RHODE ISLAND";
    $arrstate["SC"]="SOUTH CAROLINA";
    $arrstate["SD"]="SOUTH DAKOTA";
    $arrstate["TN"]="TENNESSEE";
    $arrstate["TX"]="TEXAS";
    $arrstate["UT"]="UTAH";
    $arrstate["VT"]="VERMONT";
    $arrstate["VI"]="VIRGIN ISLANDS";
    $arrstate["VA"]="VIRGINIA";
    $arrstate["WA"]="WASHINGTON";
    $arrstate["WV"]="WEST VIRGINIA";
    $arrstate["WI"]="WISCONSIN";
    $arrstate["WY"]="WYOMING";
  
		return $arrstate;
}
function ShowSelect($selectname,$arrOptions,$selected,$id="!UseSameAsName"){
//echo "<br/> $selectname,$selected <br/>";
if ($id=="!UseSameAsName"){
	$id=$selectname;
}
?>
 <select id="<?=$id?>" name="<?=$selectname?>">
<?php


	foreach ($arrOptions as $selectkey=>$selectvalue){		
	if ($selected==$selectkey) $selectshow=" selected "; else $selectshow="";
	?>
		<option value="<?=$selectkey?>" <?=$selectshow?>><?=$selectvalue?></option>
		<?php


	}
	?>
	</select>
	<?php


}
function ShowStateCombo($name,$selectedcode){
$states=GetUSStatesArray();
ShowSelect($name,$states,$selectedcode);
}

function printr ( $object , $name = '' ) {

   echo "<hr/>";
	
   if ($name<>'') print ( 'printr of \'' . $name . '\' : ' ) ;
print ( '<pre>' )  ;
   if ( is_array ( $object ) ) {
       
       print_r ( $object ) ;
       
   } else {
       var_dump ( $object ) ;
   }
	 
print ( '</pre>' ) ;
	echo "<hr/>";
}



/**
 * The letter l (lowercase L) and the number 1
 * have been removed, as they can be mistaken
 * for each other.
 */

function createRandomPassword() {

    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;

    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }

    return $pass;

}
function ShowMonthSelect($selectname,$selected=""){
  
	for ($i=1;$i<=12;$i++){
		$SelectList[$i]=$i;
	}
	ShowSelect($selectname,$SelectList,$selected);
}

function ShowYearSelect($selectname,$selected="",$StartYear=2005,$EndYear=2012){
  
	for ($i=$StartYear;$i<=$EndYear;$i++){
		$SelectList[$i]=$i;
		//echo "<br/> inside showyear $i,$EndYear,$StartYear<br/>";
		
		
	}
	
	ShowSelect($selectname,$SelectList,$selected);
}

function GetQuerySS($url){
//Get QueryString Seperator
	$result= strpos($url,"?");
	if (($result===false) and (!$result)){
		return "?";
}else{
	return "&";
}	
}

function ValidEmail($email) {
  // First, we check that there's one @ symbol, and that the lengths are right
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
    return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
     if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
      return false;
    }
  }  
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
    }
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
        return false;
      }
    }
  }
  return true;
}
function QueryError($sql){
global $localrunning;
	if ($localrunning){
		$msg="Error Running Query <br/>'".$sql."'<br/> Error was <br/>". mysql_error() ;
	}else{
		$msg="Error in Query please contact webmaster";
	}
	echo "<br/> $msg <br/>";
	
}


function enum_select( $table , $field ){ 
        $query = " SHOW COLUMNS FROM `$table` LIKE '$field' "; 
        $result = mysql_query( $query ) or die( 'error getting enum field ' . mysql_error() ); 
        $row = mysql_fetch_array( $result , MYSQL_NUM ); 
        $regex = "/'(.*?)'/"; 
        //$regex = "/'[^"\\\r\n]*(\\.[^"\\\r\n]*)*'/"; 
        preg_match_all( $regex , $row[1], $enum_array ); 
        $enum_fields = $enum_array[1]; 
        return( $enum_fields ); 
}

function GetDuration($TimeGivenInSecands,$time=true,$valueTbc='2') {
	$seconds=$TimeGivenInSecands;
	
	$CurrentTime=time();
	$ActualTimeIs=$CurrentTime - $TimeGivenInSecands;
	$seconds=$ActualTimeIs;
	//echo "<br /> total secand are $seconds <br />";
	//$ThisDay=date("Y-m-d H:i:s",86400);
	//echo "<br /> given date is $ThisDay <br />";
    $periods = array(
        'centuries' => 3155692600,
        'decades' => 315569260,
        'years' => 31556926,
        'months' => 2629743,
        'weeks' => 604800,
        'days' => 86400,
        'hours' => 3600,
        'minutes' => 60,
        'seconds' => 1
    );

    $durations = array();

    foreach ($periods as $period => $seconds_in_period) {
        if ($seconds >= $seconds_in_period) {
            $durations[$period] = floor($seconds / $seconds_in_period);
            $seconds -= $durations[$period] * $seconds_in_period;
        }
    }
  //  printr($durations);
	//exit;
	if(isset($durations['years'])){
		return $durations['years']." years ago";
	}elseif(isset($durations['months'])){
		return $durations['months']." months ago";
	}elseif(isset($durations['weeks'])){
		if($time==false){
			if($durations['weeks']<$valueTbc){
			return "1";
			}
		}
		return $durations['weeks']." weeks ago";
	}elseif(isset($durations['days'])){
		if($time==false){
			return "1";
			}
		return $durations['days']." days ago";
	}elseif(isset($durations['hours'])){
		if($time==false){
			return "1";
			}
		return $durations['hours']." hours ago";
	}elseif(isset($durations['minutes'])){
		if($time==false){
			return "1";
			}
		return $durations['minutes']." minutes ago";
	}elseif(isset($durations['seconds'])){
		if($time==false){
			return "1";
			}
		return $durations['seconds']." seconds ago";
	}
	//printr([$durations]);


    return false;
}
?>