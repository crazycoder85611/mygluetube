<?php
require_once 'dbConnection.php';
require_once "PreSqlFunctions.php";
//echo "<br/> localrunning is '$localrunning' <br/>";
//$localrunning=true;
function GetSingleRow($sql){
global $db_connection;	
//	echo "<br/> sql inside is $sql <br/>";
	$check=ExecuteQuery($sql);	
	$numrows= mysql_num_rows($check);
	if ($numrows=0){
		return array();					
	}
					

	$row=mysql_fetch_assoc($check);
	return $row;

}
function GetAllRows($sql){
global $db_connection;

	$check=ExecuteQuery($sql);	
	$return=array();
	$i=0;
	while ($row = mysql_fetch_assoc($check)) {
		$return[$i]=$row;
		//echo "<br/> Addinng row <br/>";
		$i++;
	}
	return $return;


}

function countRows($sql){
global $db_connection;

	$check2=ExecuteQuery($sql);	
	 $res=mysql_num_rows($check2);
	 
	 
	 
	 
 return $res;


}


function GetColumnPairRows($sql,$keyfield,$valuefield){
global $db_connection;
	$check=mysql_query($sql,$db_connection);	
	$return=array();
	$i=0;
	//echo "<br/> Running sql $sql <br/>";
	while ($row = mysql_fetch_assoc($check)) {
		$key=$row[$keyfield];
		$value=$row[$valuefield];
		//printr($row,"row at $i ");
		//echo "<br/> $key,$value, $keyfield,$valuefield <br/>";
		$return[$key]=$value;
		//echo "<br/> Addinng row <br/>";
		$i++;
	}
	if (!$check){
		QueryError($sql);
	}
	return $return;


}
function ExecuteQuery($sql){
//echo "<br />  sql is [$sql]  <br />";
global $db_connection;global $localrunning;
//$localrunning=true;
$result=mysql_query($sql);
//echo "<br />  Execute Query function  <br />";
//exit;
	if (!$result){		
		echo "<br/> Error running query <br/>";
		
		if ($localrunning){
			$error=mysql_error();			
		  echo "<br/> error is '$error'<br/>\n while running the query $sql <br/>";
			
			}			
		printr(debug_backtrace());
		trigger_error("Error running query",E_USER_ERROR);
	}else{
		//echo "<br/> Successfulling run $sql <br/>";
	}
	if (strtolower(substr($sql,0,6))=="insert")
	{
		$lastid=mysql_insert_id($db_connection);
		return $lastid;
	}
	return $result;
}

/*
function GetQueryDefault($sql,$row=0,$col=0){
	global $localrunning;
	$result=ExecuteQuery($sql);
	//echo "<br/> Getting default for $sql <br/>";
	$result=mysql_result($result,$row,$col);
	//echo "<br/> result is '$result' <br/>";
	if ($result===false){
		if ($localrunning){
			$error=mysql_error();			
		  echo "<br/> Inside GetQueryDefault error is '$error'<br/>\n while running the query $sql <br/>".mysql_error()." <br/>";
			//
			printr(debug_backtrace());
		}else{
			echo "<br/> error in query please contact webmaster <br/>";
		}
	}
	return $result;
}

*/

function GetQueryDefault($sql){
	
	$SingleRow=GetSingleRow($sql);
	if (!$SingleRow){
		return false;
	}else{
		//printr($SingleRow,"Single Row");
		$Keys=array_keys($SingleRow);
		$FirstKey=$Keys[0];
		return $SingleRow[$FirstKey];
	}
}

function mysqlquery_z($sql){
global $db_connection;
$result=mysql_query($sql);
$errordescription=mysql_error();
	if (!$result){
		//echo "<br/> will trigger error <br/>";
		trigger_error("Error [$errordescription] running query [$sql]",E_USER_ERROR);
	}
	
	return $result;
}

function GetLastId(){
	$sql="select last_insert_id()";
	$id=GetQueryDefault($sql);
	return $id;
}
?>