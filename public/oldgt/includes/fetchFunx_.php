<?php
	StartSessionIfNot();
	require_once dirname(__FILE__)."/../config.php";

	require_once dirname(__FILE__)."/SqlFunctions.php";

	/*ExecuteQuery($sql);	
	$data=GetAllRows($sql);
	return $data;
	$data=GetSingleRow($sql);
	return $data;	
	*/
	
// contacts -->
	function contacts($query='',$type='all',$limit=0,$start=0,$sortfield='reminder',$sorttype='desc'){
	$orderfield=array('name'=>'con.name','company'=>'com.company_name','updated'=>'con.updated','ai'=>'con.ai_title','status'=>'con.status','reminder'=>'con.reminder_date');
	
	if($query!=''){
		$sql_contact="select con.*, com.company_name, concat('index.php?q=contact/view/',con.contact_id) as link from contact con, company com where con.company_id=com.company_id and (con.name LIKE '%".$query."%' or com.company_name LIKE '%".$query."%')";
		$sql_mycontact="select con.*, com.company_name, concat('index.php?q=contact/view/',con.contact_id) as link from contact con, company com where con.company_id=com.company_id and con.agent_id='".$_SESSION['uid']."' and (con.name LIKE '%".$query."%' or com.company_name LIKE '%".$query."%')";
	}else{
		$sql_contact="select con.*, com.company_name, concat('index.php?q=contact/view/',con.contact_id) as link from contact con, company com where con.company_id=com.company_id";
		$sql_mycontact="select con.*, com.company_name, concat('index.php?q=contact/view/',con.contact_id) as link from contact con, company com where con.company_id=com.company_id and con.agent_id='".$_SESSION['uid']."'";
	}

	if($type=='my'){
		if($limit!=0)
			$sql=$sql_mycontact.' order by '.$orderfield[$sortfield].' '.$sorttype.' LIMIT '.$start.','.$limit;
		else
			$sql=$sql_mycontact.' order by '.$orderfield[$sortfield].' '.$sorttype;
	}else{
		if($limit!=0)
			$sql=$sql_contact.' order by '.$orderfield[$sortfield].' '.$sorttype.' LIMIT '.$start.','.$limit;
		else
			$sql=$sql_contact.' order by '.$orderfield[$sortfield].' '.$sorttype;
	}

	$data=GetAllRows($sql);
	return $data;
	}

	function getActiveContacts(){
	mysql_query("SET NAMES utf8");
	$sql="select distinct c.name, p.contact_id from contact c, project p where p.contact_id!=0 and p.project_status_id!=3 and p.project_status_id!=4 and p.contact_id=c.contact_id order by name asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function addContact($name,$company_id,$activity_industry,$prospect,$status,$aoi,$rdate,$jobtitle=''){
	mysql_query("SET NAMES utf8");
	$sql="insert contact set name='$name',company_id='$company_id',ai_title='$activity_industry',prospect='$prospect',status='$status',aoi='$aoi', reminder_date='$rdate', agent_id='".$_SESSION['uid']."', job_title='$jobtitle', updated=now()";
	ExecuteQuery($sql);	
	}
	
	function addContactDetails($contact_id,$type,$no,$default){
	mysql_query("SET NAMES utf8");	
	$sql="insert contact_details set contact_id='$contact_id', type='$type', value='$no', default_no='$default'";
	ExecuteQuery($sql);
	}
	
	function addContactImage($img,$id){
	mysql_query("SET NAMES utf8");
	$sql="update contact set contact_img='$img' where contact_id='$id'";
	
	ExecuteQuery($sql);	
	}
	
	function deleteContactImage($img,$id){
	mysql_query("SET NAMES utf8");
	$sql="update contact set contact_img='' where contact_id='$id'";
	
	ExecuteQuery($sql);	
	}
	
	function updateContact($name,$company_id,$activity_industry,$prospect,$status,$aoi,$rdate,$id,$jobtitle=''){
	mysql_query("SET NAMES utf8");
	$sql="update contact set name='$name',company_id='$company_id',ai_title='$activity_industry',prospect='$prospect',status='$status',aoi='$aoi', reminder_date='$rdate',job_title='$jobtitle', updated=now() where contact_id='$id'";
	
	ExecuteQuery($sql);	
	}
	
	function deleteContact($id){
	$sql="delete from contact where contact_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function deleteContactDetails($id){
	
	$sql="delete from contact_details where contact_id='$id'";
	
	ExecuteQuery($sql);	
	
	}
	
	function getAllContactsNoNA(){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact where name!='N/A' order by name asc";
	$data=GetAllRows($sql);
	return $data;
	}
		
	function getContact($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact where contact_id='$id'";
	$data=GetSingleRow($sql);
	return $data;
	}
	
	function getContactDetails($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and contact_id='$id'";
	$data=GetSingleRow($sql);
	return $data;
	}
	
	function getContactsByCompanyID($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id='$id' and con.company_id=com.company_id order by con.name asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getCDValue($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact_details cd where cd.id='$id'";
	$data=GetSingleRow($sql);
	return $data['value'];
	}
	
	function getContactNos($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact_details where contact_id='$id'";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getContactNosByType($id,$type){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact_details where type='$type' and contact_id='$id' order by default_no desc";
	$data=GetAllRows($sql);
	return $data;
	}

	function getDefaultContactNo($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact_details where contact_id='$id' and default_no='1'";
	$data=GetSingleRow($sql);
	return $data;
	}
	
	function getLastContact(){
	mysql_query("SET NAMES utf8");
	$sql="SELECT MAX(contact_id) AS 'contact_id' FROM contact";
	$data=GetSingleRow($sql);
	return $data;	
	}	
	
	function getAllContacts(){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id order by con.reminder_date desc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getAllContactsLimit($limit){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id order by con.reminder_date desc $limit";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getAllMyContacts(){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and con.agent_id='".$_SESSION['uid']."' order by con.reminder_date desc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getRecentContacts($limit=3){
	mysql_query("SET NAMES utf8");
	$sql="select distinct con.name, con.contact_id, com.company_name, com.company_id from contact con, company com, project p where p.contact_id=con.contact_id and  con.company_id=com.company_id order by p.project_id desc limit 0,".$limit;
	$data=GetAllRows($sql);
	return $data;	
	}	

// company -->
	function getActiveCompanies(){
	mysql_query("SET NAMES utf8");
	$sql="select distinct c.company_name, p.company_id from company c, project p where p.company_id!=0 and p.project_status_id!=3 and p.project_status_id!=4 and p.company_id=c.company_id order by company_name asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function addCompany($name,$pob,$zip,$city,$country){
	mysql_query("SET NAMES utf8");
	$sql="insert company set company_name='$name', pob='$pob', zip='$zip', city='$city', country='$country', agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	}
	
	function addCompanyImage($img,$id){
	mysql_query("SET NAMES utf8");
	$sql="update company set company_img='$img' where company_id='$id'";
	
	ExecuteQuery($sql);	
	}
	
	function deleteCompanyImage($img,$id){
	mysql_query("SET NAMES utf8");
	$sql="update company set company_img='' where company_id='$id'";
	
	ExecuteQuery($sql);	
	}
	function updateCompany($name,$pob,$zip,$city,$country,$id){
	mysql_query("SET NAMES utf8");
	$sql="update company set company_name='$name', pob='$pob', zip='$zip', city='$city', country='$country' where company_id='$id'";
	
	ExecuteQuery($sql);	
	}
	
	function getCompanyDetails($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from company where company_id='$id'";
	$data=GetSingleRow($sql);
	return $data;
	}
	
	function getCompanyId($name){
	mysql_query("SET NAMES utf8");
	$sql="select company_id from company where company_name='$name'";
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getCompanyName($id){
	mysql_query("SET NAMES utf8");
	$sql="select company_name from company where company_id='$id'";
	$data=GetSingleRow($sql);
	return $data['company_name'];	
	}
	
	function getAllCompanies(){
	mysql_query("SET NAMES utf8");
	$sql="select * from company";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getAllCompaniesOdrByName(){
	mysql_query("SET NAMES utf8");
	$sql="select * from company order by company_name asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
// activity industry -->
	
	function addActivityIndustry($title){
	mysql_query("SET NAMES utf8");
	$sql="insert activity_industry set title='$title'";
	ExecuteQuery($sql);
	}
	
	function getActivityIndustryId($title){
	mysql_query("SET NAMES utf8");
	$sql="select ai_id from activity_industry where title='$title'";
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getActivityIndustryDetails($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from activity_industry where ai_id='$id'";
	$data=GetSingleRow($sql);
	return $data;
	}
	function getAllActivityIndustry(){
	mysql_query("SET NAMES utf8");
	$sql="select distinct ai_title from contact order by ai_title asc";
	$data=GetAllRows($sql);
	return $data;
	}
	/*function getAllActivityIndustry(){
	mysql_query("SET NAMES utf8");
	$sql="select * from activity_industry";
	$data=GetAllRows($sql);
	return $data;
	}*/
	
// calls -->	
	
	function DialingNow($remarks,$id,$nextstep=''){	
	mysql_query("SET NAMES utf8");	
	$date=date('Y-m-d H:i:s');
	$sql="insert calls set remarks='$remarks',next_step='$nextstep',date=now(), contact_id='$id',agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	
	mysql_query("SET NAMES utf8");
	$sql="update contact set last_call_date=now() where  contact_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function editRemark($id,$remarks,$nextstep=''){
	mysql_query("SET NAMES utf8");
	$sql="update calls set remarks='$remarks',next_step='$nextstep' where call_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function getRemark($id){
	mysql_query("SET NAMES utf8");
	$sql="SELECT * FROM calls where call_id='$id'";	
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getLastCall($id,$sdate='',$edate=''){	
	mysql_query("SET NAMES utf8");	
	
	if($sdate=='' || $edate=='')
		$withdateqry='';
	else
		$withdateqry=" and (date_format(date,'%Y-%m-%d')=date_format(date,'".$sdate."') or date_format(date,'%Y-%m-%d')=date_format(date,'".$edate."') or (date_format(date,'%Y-%m-%d')>date_format(date,'".$sdate."') and date_format(date,'%Y-%m-%d')<date_format(date,'".$edate."')))";
	
	$sql="SELECT MAX(call_id) AS 'call_id' FROM calls where contact_id='$id' ".$withdateqry;
	$data=GetSingleRow($sql);
	
	$sql="SELECT * FROM calls where call_id='".$data['call_id']."'";
	$data=GetSingleRow($sql);
	return $data;
	}
	
	function getAllCalls($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from calls where contact_id='$id' order by date desc";
	$data=GetAllRows($sql);
	return $data;
	}
	function getSalesReport($type='dtl',$field,$sorttype,$limit='',$sdate='',$edate=''){
	if($sdate=='' || $edate=='')
		$withdateqry='';
	else
		$withdateqry=" and (date_format(calls.date,'%Y-%m-%d')=date_format(calls.date,'".$sdate."') or date_format(calls.date,'%Y-%m-%d')=date_format(calls.date,'".$edate."') or (date_format(calls.date,'%Y-%m-%d')>date_format(calls.date,'".$sdate."') and date_format(calls.date,'%Y-%m-%d')<date_format(calls.date,'".$edate."')))";
		
	if($type=='dtl'){
		$orderfield=array('date'=>'calls.date','agent'=>'l.username','company'=>'com.company_name','client'=>'con.name','status'=>'con.status');//and pe.entry_status!='Completed'
		
		if($limit==''){
			$sql="select calls.*, com.company_name, com.company_id, con.name, con.status from calls, contact con, company com, login l where calls.contact_id=con.contact_id and con.company_id=com.company_id and calls.agent_id=l.agent_id ".$withdateqry." order by ".$orderfield[$field]." ".$sorttype;
		}
		else{
			$sql="select calls.*, com.company_name, com.company_id, con.name, con.status from calls, contact con, company com, login l where calls.contact_id=con.contact_id and con.company_id=com.company_id and calls.agent_id=l.agent_id  ".$withdateqry." order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
		}
		
		$allData=GetAllRows($sql);
		return $allData; 
	
	}elseif($type=='summary'){
		$orderfield=array('date'=>'','agent'=>'l.username','company'=>'com.company_name','client'=>'con.name','status'=>'con.status');//and pe.entry_status!='Completed'
		if($limit==''){
			if($orderfield[$field]!=''){
				$sql="select distinct com.company_name, com.company_id, con.name, con.status,con.contact_id from calls, contact con, company com, login l where con.contact_id=calls.contact_id and con.company_id=com.company_id and calls.agent_id=l.agent_id ".$withdateqry." order by ".$orderfield[$field]." ".$sorttype;
			}else{
				$sql="select distinct com.company_name, com.company_id, con.name, con.status,con.contact_id from calls, contact con, company com, login l where con.contact_id=calls.contact_id and con.company_id=com.company_id and calls.agent_id=l.agent_id ".$withdateqry." order by con.name asc";
			}
		}else{
			$sql="select distinct com.company_name, com.company_id, con.name, con.status,con.contact_id from calls, contact con, company com, login l where con.contact_id=calls.contact_id and con.company_id=com.company_id and calls.agent_id=l.agent_id  ".$withdateqry." order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
		}
		$allData=GetAllRows($sql);
		return $allData; 
	}
	
	}
	
// sort contacts -->
	
	function getContacts($search){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and (con.name LIKE '%".$search."%' or com.company_name LIKE '%".$search."%' or con.ai_title LIKE '%".$search."%')";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getAllContactsSortByName(){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id order by con.name asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortContacts($col,$sorttype){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id order by $col $sorttype";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortSearchedContacts($col,$sorttype,$search){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and (con.name LIKE '%".$search."%' or com.company_name LIKE '%".$search."%' or con.ai_title LIKE '%".$search."%') order by $col $sorttype";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getMyContacts($search){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and con.agent_id='".$_SESSION['uid']."' and (con.name LIKE '%".$search."%' or com.company_name LIKE '%".$search."%' or con.ai_title LIKE '%".$search."%')";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function sortMyContacts($col,$sorttype){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and con.agent_id='".$_SESSION['uid']."' order by $col $sorttype";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortSearchedMyContacts($col,$sorttype,$search){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact con, company com where con.company_id=com.company_id and agent_id='".$_SESSION['uid']."' and (con.name LIKE '%".$search."%' or com.company_name LIKE '%".$search."%' or con.ai_title LIKE '%".$search."%') order by $col $sorttype";
	$data=GetAllRows($sql);
	return $data;
	}	
	
	// groups -->

	function addGroup($name,$desc){
	mysql_query("SET NAMES utf8");
	$sql="insert groups set group_name='$name', group_desc='$desc', agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	}
	
	function addGroupMember($gid,$cid,$aid,$cdid){
	mysql_query("SET NAMES utf8");
	$sql="insert members set group_id='$gid', contact_id='$cid', agent_id='$aid', cd_id='$cdid'";
	ExecuteQuery($sql);
	}
	
	function updateGroup($name,$desc,$id){
	mysql_query("SET NAMES utf8");
	$sql="update groups set group_name='$name', group_desc='$desc' where group_id='$id'";
	
	ExecuteQuery($sql);	
	}
	
	function deleteGroup($groupid){
	$sql="delete from groups where group_id='$groupid'";
	ExecuteQuery($sql);	
	}
	
	function deleteGroupMembers($groupid){
	$sql="delete from members where group_id='$groupid'";
	ExecuteQuery($sql);	
	}
	
	function removeMember($mid){
	$sql="delete from members where member_id='$mid'";
	ExecuteQuery($sql);	
	}
	
	function getLastGroup(){
	mysql_query("SET NAMES utf8");
	$sql="SELECT MAX(group_id) AS 'group_id' FROM groups where agent_id='".$_SESSION['uid']."'";
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getAllGroups(){
	mysql_query("SET NAMES utf8");
	$sql="select * from groups where agent_id='".$_SESSION['uid']."' order by group_name";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function searchGroup($val){
	mysql_query("SET NAMES utf8");
	$sql="select * from groups where agent_id='".$_SESSION['uid']."' and group_name LIKE '%$val%' order by group_name";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getGroupDetails($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from groups where agent_id='".$_SESSION['uid']."' and group_id='$id'";
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getNotGroupMembers($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from contact";
	
	$members=getGroupMembers($id);
	foreach($members as $member){
		$sql.=" where ";
		$sql.="(contact_id!='".$member['contact_id']."'";
		$sql.=" or ";
		$sql.="agent_id!='".$member['contact_id']."')";
	}
	
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getNonAMembers($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from login where agent_id!='0' and agent_id!=1";
	
	$members=getGroupMembers($id);
	foreach($members as $member){
		$sql.=" and ";
		$sql.="agent_id!='".$member['agent_id']."'";
	}
	
	$sql.=" order by fullname";
	
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getNonBMembers($id){
	mysql_query("SET NAMES utf8");
	$sql="select c.contact_id, c.name, cd.id, cd.value as 'email', com.company_name from contact c, contact_details cd, company com 
	where c.contact_id=cd.contact_id and c.company_id=com.company_id and cd.type='email' ";
	$sql.=" and ";
	$sql.="c.contact_id!='0'";
	$members=getGroupMembers($id);
	foreach($members as $member){
		$sql.=" and ";
		$sql.="c.contact_id!='".$member['contact_id']."'";
	}
	
	
	$sql.=" order by company_name";
	
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getGroupMembers($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from members where members.group_id='$id'";
	$data=GetAllRows($sql);
	return $data;	
	}

	// mailing list -->

	function addNewsletter($title,$body){
	mysql_query("SET NAMES utf8");
	$date=date('Y-m-d H:i:s');
	$sql="insert newsletter set nl_title='$title', nl_body='$body', nl_created='$date', nl_updated='$date', agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	}
	
	function addRecipient($id,$email){
	mysql_query("SET NAMES utf8");
	$sql="insert newsletter_to set nl_id='$id', nl_email='$email'";
	ExecuteQuery($sql);
	}
	
	function updateNewsletter($title,$body,$id){
	mysql_query("SET NAMES utf8");
	$date=date('Y-m-d H:i:s');
	$sql="update newsletter set nl_title='$title', nl_body='$body', nl_updated='$date' where nl_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function deleteNewsletter($id){
	$sql="delete from newsletter where nl_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function deleteRecipients($id){
	$sql="delete from newsletter_to where nl_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function getLastNewsletter(){
	mysql_query("SET NAMES utf8");
	$sql="SELECT MAX(nl_id) AS 'nl_id' FROM newsletter where agent_id='".$_SESSION['uid']."'";
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getAllNewsletter(){
	mysql_query("SET NAMES utf8");
	$sql="select * from newsletter where agent_id='".$_SESSION['uid']."' order by nl_updated desc";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getAllNewsletterSortbyTitle(){
	mysql_query("SET NAMES utf8");
	$sql="select * from newsletter where agent_id='".$_SESSION['uid']."' order by nl_title asc";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getAllRecipients($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from newsletter_to where nl_id='$id'";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getNewsletterDetails($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from newsletter where nl_id='$id'";
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getRecipientsEmail($id){
	mysql_query("SET NAMES utf8");
	$sql="select distinct nl_email from newsletter_to nlt where nlt.nl_id='$id'";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function newsletterSent($id){
	mysql_query("SET NAMES utf8");
	$date=date('Y-m-d H:i:s');
	$sql="update newsletter set nl_sent='$title' and nl_sent_date='$date' where nl_id='$id'";
	ExecuteQuery($sql);	
	}
	
	
	// Agents [mygt only]-->
	function getAllAgents(){
	mysql_query("SET NAMES utf8");
	$sql="select * from login where agent_id!='1' order by username asc";
	$data=GetAllRows($sql);
	return $data;	
	}function getAllAdmin(){
	mysql_query("SET NAMES utf8");
	$sql="select * from login where super_admin='1' order by username asc";
	$data=GetAllRows($sql);
	return $data;	
	}
	function addAgent($username,$password,$fullname,$email,$ext){
	mysql_query("SET NAMES utf8");
	$sql="insert login set username='$username', password='$password', fullname='$fullname', email='$email', ext='$ext'";
	ExecuteQuery($sql);
	}
	function deleteAgent($id){
	$sql="delete from login where agent_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function getAgentField($id,$field){
	mysql_query("SET NAMES utf8");
	$sql="select $field from login where agent_id='$id'";
	$data=GetSingleRow($sql);
	return $data[$field];
	}
	function getAgentAvatar($agentid,$size=16){
	$ava_filename=getAgentField($agentid,'avatar');
	$filename=($ava_filename)?$ava_filename:'default.png';
	return '<img src="img/avatars/'.$filename.'" width="'.$size.'" height="'.$size.'" class="downer" title="'.getAgentField($agentid,'username').'" />';
	}
	function getAgentFieldCustom($frfield,$id,$field){
	mysql_query("SET NAMES utf8");
	$sql="select $field from login where $frfield='$id'";
	$data=GetSingleRow($sql);
	return $data[$field];
	}
	function getAgentID($id,$field){
	mysql_query("SET NAMES utf8");
	$sql="select agent_id from login where $field='$id'";
	$data=GetSingleRow($sql);
	return $data['agent_id'];
	}
	
	function getAgentFieldUN($username,$field){
	mysql_query("SET NAMES utf8");
	$sql="select $field from login where username='$username'";
	$data=GetSingleRow($sql);
	return $data[$field];
	}
	
	function updateAgent($fullname,$email,$ext){
	mysql_query("SET NAMES utf8");
	$sql="update login set fullname='$fullname', email='$email', ext='$ext' where agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	
	function LastLogin(){
	mysql_query("SET NAMES utf8");
	$sql="update login set logged_date='".date('Y-m-d H:i:s')."' where agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	// <--
	
	// Agents [integration with chat and hd]-->
	function addNewAgent($fields){
		if(!checkDuplicateUsername(strip_tags($fields['username']))){
			$date=date('Y-m-d H:i:s');	
			//chat -->
			$sql=' SET op2op='._db_input(isset($fields['ch_chatmode'])?1:0).
					 ',rateme='._db_input(isset($fields['ch_ratemode'])?1:0).
					 ',login='._db_input(strip_tags($fields['username'])).
					 ',name='._db_input(strip_tags($fields['firstname']).' '.strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']);
		
			if($fields['password'])
				$sql.=',password='._db_input(md5($fields['password']));
			
			$sql='INSERT INTO chat_admin '.$sql.',created='._db_input(time());
			ExecuteQuery($sql);
			$chatid=selectLastRow('chat_admin','userID');
			
			foreach($fields['ch_dept'] as $key=>$value){
				$ins='INSERT INTO chatuserdeptlist SET userID='._db_input($chatid).',deptID='._db_input($value);
				ExecuteQuery($ins);
			}
			//<-- chat
			//hd -->
			$sql2=' SET updated='._db_input($date).
					 ',isadmin='._db_input(($fields['hd_account_type'])?1:0).
					 ',isactive='._db_input(($fields['hd_account_status'])?1:0).
					 ',isvisible='._db_input(isset($fields['hd_dl'])?1:0).
					 ',onvacation='._db_input(isset($fields['hd_vacation'])?1:0).
					 ',dept_id='._db_input($fields['hd_dept']).
					 ',group_id='._db_input($fields['hd_ug']).
					 ',username='._db_input(strip_tags($fields['username'])).
					 ',firstname='._db_input(strip_tags($fields['firstname'])).
					 ',lastname='._db_input(strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']).
					 ',phone='._db_input($fields['phone']).
					 ',phone_ext='._db_input($fields['phone_ext']).
					 ',change_passwd=0'.
					 ',signature='._db_input(strip_tags($fields['hd_signature']));
		
			if($fields['password'])
				$sql2.=',passwd='._db_input(md5($fields['password']));
							
			$sql2='INSERT INTO ost_staff '.$sql2.',created='._db_input($date);
			ExecuteQuery($sql2);
			$hdid=selectLastRow('ost_staff','staff_id');
			//<-- hd
			// gt-->
			$sql3=' SET updated='._db_input($date).
					 ',userID='._db_input($chatid).
					 ',staff_id='._db_input($hdid).
					 ',username='._db_input(strip_tags($fields['username'])).
					 ',firstname='._db_input(strip_tags($fields['firstname'])).
					 ',lastname='._db_input(strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']).
					 ',telephone='._db_input($fields['phone']).
					 ',ext='._db_input($fields['phone_ext']);
		
			if($fields['password'])
				$sql3.=',password='._db_input($fields['password']);
				
			if(isset($fields['change_pass']))
				$sql3.=',change_passwd=1';
				
			$sql3='INSERT INTO login '.$sql3.',created='._db_input($date);
			ExecuteQuery($sql3);
			//<--gt
			
			return true;
		}
		
		return false;
	}
	function editAgent($id,$fields){
		if(checkAgentIdExists($id)){
			$chatid=getAgentField($id,'userID');
			$hdid=getAgentField($id,'staff_id');
			
			$date=date('Y-m-d H:i:s');	
			//chat -->
			$sql=' SET op2op='._db_input(isset($fields['ch_chatmode'])?1:0).
					 ',rateme='._db_input(isset($fields['ch_ratemode'])?1:0).
					 ',name='._db_input(strip_tags($fields['firstname']).' '.strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']);
		
			if($fields['password'])
				$sql.=',password='._db_input(md5($fields['password']));
			
			$sql='UPDATE chat_admin '.$sql.' where userID='._db_input($chatid);
			ExecuteQuery($sql);
			
			$del="delete from chatuserdeptlist where userID='".$chatid."'";
			ExecuteQuery($del);	//delete previous
			
			foreach($fields['ch_dept'] as $key=>$value){
				
				$ins='INSERT INTO chatuserdeptlist SET userID='._db_input($chatid).',deptID='._db_input($value);
				ExecuteQuery($ins);
			}
			//<-- chat
			//hd -->
			$sql2=' SET updated='._db_input($date).
					 ',isadmin='._db_input(($fields['hd_account_type'])?1:0).
					 ',isactive='._db_input(($fields['hd_account_status'])?1:0).
					 ',isvisible='._db_input(isset($fields['hd_dl'])?1:0).
					 ',onvacation='._db_input(isset($fields['hd_vacation'])?1:0).
					 ',dept_id='._db_input($fields['hd_dept']).
					 ',group_id='._db_input($fields['hd_ug']).
					 ',firstname='._db_input(strip_tags($fields['firstname'])).
					 ',lastname='._db_input(strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']).
					 ',phone='._db_input($fields['phone']).
					 ',phone_ext='._db_input($fields['phone_ext']).
					 ',signature='._db_input(strip_tags($fields['hd_signature']));
		
			if($fields['password'])
				$sql2.=',passwd='._db_input(md5($fields['password']));
							
			$sql2='UPDATE ost_staff '.$sql2.' where staff_id='._db_input($hdid);
			ExecuteQuery($sql2);
			//<-- hd
			// gt-->
			$sql3=' SET updated='._db_input($date).
					 ',firstname='._db_input(strip_tags($fields['firstname'])).
					 ',lastname='._db_input(strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']).
					 ',telephone='._db_input($fields['phone']).
					 ',ext='._db_input($fields['phone_ext']);
		
			if($fields['password'])
				$sql3.=',password='._db_input($fields['password']);
				
			if(isset($fields['change_pass']))
				$sql3.=',change_passwd=1';
				
			$sql3='UPDATE login '.$sql3.' where agent_id='._db_input($id);
			ExecuteQuery($sql3);
			//<--gt
			
			return true;
		}
		
		return false;
	}
	function editMyProf($id,$fields){
		if(checkAgentIdExists($id)){
			$chatid=getAgentField($id,'userID');
			$hdid=getAgentField($id,'staff_id');
			
			$date=date('Y-m-d H:i:s');	
			//chat -->
			$sql=' SET name='._db_input(strip_tags($fields['firstname']).' '.strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']);
		
			if($fields['password'])
				$sql.=',password='._db_input(md5($fields['password']));
			
			$sql='UPDATE chat_admin '.$sql.' where userID='._db_input($chatid);
			ExecuteQuery($sql);
			
			//<-- chat
			//hd -->
			$sql2=' SET updated='._db_input($date).
					 ',firstname='._db_input(strip_tags($fields['firstname'])).
					 ',lastname='._db_input(strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']).
					 ',phone='._db_input($fields['phone']).
					 ',phone_ext='._db_input($fields['phone_ext']);
		
			if($fields['password'])
				$sql2.=',passwd='._db_input(md5($fields['password']));
							
			$sql2='UPDATE ost_staff '.$sql2.' where staff_id='._db_input($hdid);
			ExecuteQuery($sql2);
			//<-- hd
			// gt-->
			
			$target_path = "img/avatars/";
			$ext="";$addtoquery='';
			if ($_FILES["avatar"]["type"] == "image/gif")
				$ext=".gif";
			if ($_FILES["avatar"]["type"] == "image/jpeg")
				$ext=".jpg";
			if ($_FILES["avatar"]["type"] == "image/png" )
				$ext=".png";
			$imgname=basename( $_SESSION['uid'].$ext);				
			//print_r($_FILES);
			$target_path = $target_path.$imgname;
			if ($ext!=""){
				if(move_uploaded_file($_FILES['avatar']['tmp_name'], $target_path)) {
					$addtoquery.=", avatar='".$imgname."'";
				}
			}
			$sql3=' SET updated='._db_input($date).
					 ',firstname='._db_input(strip_tags($fields['firstname'])).
					 ',lastname='._db_input(strip_tags($fields['lastname'])).
					 ',email='._db_input($fields['email']).
					 ',telephone='._db_input($fields['phone']).
					 ',ext='._db_input($fields['phone_ext']).$addtoquery;
		
			if($fields['password'])
				$sql3.=',password='._db_input($fields['password']);
				
			$sql3='UPDATE login '.$sql3.' where agent_id='._db_input($id);
			ExecuteQuery($sql3);
			//<--gt
			
			return true;
		}
		
		return false;
	}
	function getAgentInfo($mygtid){
	$sql="select gt.*, ost.group_id,ost.dept_id,ost.isactive,ost.isadmin,ost.isvisible,ost.onvacation, ch.rateme,ch.op2op,ch.console_refresh,ch.aspID from login gt,ost_staff ost,chat_admin ch where gt.agent_id='$mygtid' and gt.staff_id=ost.staff_id and gt.userID=ch.userID";
	$data=GetSingleRow($sql);
	return $data;
	}
	function getAgentChatDepts($gtid){
	$sql="select deptID from login l,chatuserdeptlist chdl where l.agent_id='$gtid' and l.userID=chdl.userID";
	$data=GetAllRows($sql);
	return $data;	
	}
	function getChatDept(){
	$sql="select name,deptID from chatdepartments where visible=1";
	$data=GetAllRows($sql);
	return $data;	
	}
	function getHDDept(){
	$sql="select dept_id,dept_name from ost_department where ispublic=1";
	$data=GetAllRows($sql);
	return $data;	
	}
	function getHDUserGroups(){
	$sql="select group_id,group_name from ost_groups where group_enabled=1";
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function CHAT_AdminUsers_get_UserDepartments($userid ){
		if ( $userid == "" ){
			return false ;
		}
		$query = "SELECT * FROM chatdepartments, chatuserdeptlist WHERE chatdepartments.deptID = chatuserdeptlist.deptID AND chatuserdeptlist.userID = $userid ORDER BY chatdepartments.name ASC" ;
		$data=GetAllRows($query);
		
		return $data;
	}
	function CHAT_AdminASP_get_ASPInfo($aspID ){
		$query = "SELECT * FROM chat_asp WHERE aspID = '$aspID'" ;
		$data=GetSingleRow($query);
		return $data;
	}
	function chat_updateAgentStatus($userid,$status){
		$query = "UPDATE chat_admin SET available_status = $status WHERE userID = $userid" ;
		ExecuteQuery($query);	
	}
	
	function CHAT_AdminUsers_update_UserValue($userid,$tbl_name,$value )
	{
		if ( ( $userid == "" ) || ( $tbl_name == "" )){
			return false ;
		}
		$userid = database_mysql_quote( $userid );
		$tbl_name = database_mysql_quote( $tbl_name );
		$value = database_mysql_quote( $value );

		$query = "UPDATE chat_admin SET $tbl_name = '$value' WHERE userID = $userid" ;
		database_mysql_query( $dbh, $query ) ;
		
		ExecuteQuery($query);
	}
	
	// <--
	
	// Working Hours -->
	function getWH($whid){
		$query = "SELECT * FROM work_hours WHERE whid = '".$whid."' and agent_id='".$_SESSION['uid']."'" ;
		$data=GetSingleRow($query);
		return $data;
	}
	
	function chkIncompleteWH($taskid, $whid=''){
		if($whid!='') $whidval=" and whid='".$whid."'";
		else $whidval='';
		$query = "SELECT * FROM work_hours WHERE task_id = '".$taskid."' and agent_id='".$_SESSION['uid']."' and (wh_edate='0000-00-00 00:00:00'or wh_edate IS NULL) ".$whidval;
		$data=GetSingleRow($query);
		
		if($data['whid']!='') return true;
		else return false;
	}
	
	function getAllWH($col='updated',$sort='desc', $col1='task',$sort1='asc',$col2='',$sort2='asc'){
	mysql_query("SET NAMES utf8");
	$orderfield=array('task'=>'pe.entry_title','sdate'=>'wh.wh_sdate','edate'=>'wh.wh_edate','updated'=>'wh.updated','whrs'=>'totalhrs','proj'=>'p.project_title');
	
	if($col2!=''){
	$sql="select wh.*, (UNIX_TIMESTAMP(wh.wh_edate)-UNIX_TIMESTAMP(wh.wh_sdate))/3600 as totalhrs, concat('index.php?q=project/view/',pe.project_id,'/task/view/',pe.entry_id) as tasklink, pe.entry_title as task, p.project_title as proj,pe.project_id as projid, concat('index.php?q=project/view/',pe.project_id) as projlink from work_hours wh, project_entries pe, project p where wh.agent_id='".$_SESSION['uid']."' and wh.task_id=pe.entry_id and pe.project_id=p.project_id order by ".$orderfield[$col2]." ".$sort2.", ".$orderfield[$col1]." ".$sort1.", ".$orderfield[$col]." ".$sort;
	}else{
	$sql="select wh.*, (UNIX_TIMESTAMP(wh.wh_edate)-UNIX_TIMESTAMP(wh.wh_sdate))/3600 as totalhrs, concat('index.php?q=project/view/',pe.project_id,'/task/view/',pe.entry_id) as tasklink, pe.entry_title as task, p.project_title as proj,pe.project_id as projid, concat('index.php?q=project/view/',pe.project_id) as projlink from work_hours wh, project_entries pe, project p where wh.agent_id='".$_SESSION['uid']."' and wh.task_id=pe.entry_id and pe.project_id=p.project_id order by ".$orderfield[$col1]." ".$sort1.", ".$orderfield[$col]." ".$sort;
	}
		
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getWHSummary($col='updated',$sort='desc', $col1='task',$sort1='asc',$col2='',$sort2='asc'){
	mysql_query("SET NAMES utf8");
	$orderfield=array('task'=>'pe.entry_title','sdate'=>'wh.wh_sdate','edate'=>'wh.wh_edate','updated'=>'last_updated','whrs'=>'total','proj'=>'p.project_title');
	
	if($col2!=''){
	$sql="select MAX(wh.updated) as last_updated, SUM(round((UNIX_TIMESTAMP(wh.wh_edate)-UNIX_TIMESTAMP(wh.wh_sdate))/3600,4)) as total, concat('index.php?q=project/view/',pe.project_id,'/task/view/',pe.entry_id) as tasklink, pe.entry_title as task, p.project_title as proj,pe.project_id as projid, concat('index.php?q=project/view/',pe.project_id) as projlink from work_hours wh, project_entries pe, project p where wh.agent_id='".$_SESSION['uid']."' and wh.task_id=pe.entry_id and pe.project_id=p.project_id GROUP BY task order by ".$orderfield[$col2]." ".$sort2.", ".$orderfield[$col1]." ".$sort1.", ".$orderfield[$col]." ".$sort;
	}else{
	$sql="select MAX(wh.updated) as last_updated, SUM(round((UNIX_TIMESTAMP(wh.wh_edate)-UNIX_TIMESTAMP(wh.wh_sdate))/3600,4)) as total, concat('index.php?q=project/view/',pe.project_id,'/task/view/',pe.entry_id) as tasklink, pe.entry_title as task, p.project_title as proj,pe.project_id as projid, concat('index.php?q=project/view/',pe.project_id) as projlink from work_hours wh, project_entries pe, project p where wh.agent_id='".$_SESSION['uid']."' and wh.task_id=pe.entry_id and pe.project_id=p.project_id GROUP BY task order by ".$orderfield[$col1]." ".$sort1.", ".$orderfield[$col]." ".$sort;
	}
	//print $sql;
	$data=GetAllRows($sql);
	return $data;	
	}
	
	function getTotalWH($id,$type='task'){
		if($type=='proj'){
			$sql="select SUM(round((UNIX_TIMESTAMP(wh.wh_edate)-UNIX_TIMESTAMP(wh.wh_sdate))/3600,4)) as gentotal from work_hours wh, project_entries pe where wh.task_id=pe.entry_id and pe.project_id='".$id."' and wh.agent_id='".$_SESSION['uid']."'";
		}else{
			$sql="select SUM(round((UNIX_TIMESTAMP(wh.wh_edate)-UNIX_TIMESTAMP(wh.wh_sdate))/3600,4)) as gentotal from work_hours wh where wh.task_id='".$id."' and wh.agent_id='".$_SESSION['uid']."'";
		}
		
		$data=GetSingleRow($sql);
		return $data['gentotal'];
	}
	
	function getWHbyCompany($id){
	mysql_query("SET NAMES utf8");
	$sql="select * from man_hours where company_id='$id' order by mh_edate desc";
	$data=GetAllRows($sql);
	return $data;	
	}
	function addWH($taskid,$sdate,$edate){
		mysql_query("SET NAMES utf8");
		$sql="insert work_hours set agent_id='".$_SESSION['uid']."', wh_sdate='".$sdate."', wh_edate='".$edate."', task_id='".$taskid."', updated=now(), created=now()";
		ExecuteQuery($sql);
	}
	function updateWH($taskid,$sdate,$edate,$whid){
		mysql_query("SET NAMES utf8");
		$sql="update work_hours set wh_sdate='".$sdate."', wh_edate='".$edate."', task_id='".$taskid."', updated=now() where whid='".$whid."' and agent_id='".$_SESSION['uid']."'";
		ExecuteQuery($sql);
	}
	function deleteWH($id){
	$sql="delete from work_hours where agent_id='".$_SESSION['uid']."' and whid='$id'";
	ExecuteQuery($sql);	
	}
	
	function startWorkHours($taskid){
		mysql_query("SET NAMES utf8");
		$sql="insert work_hours set agent_id='".$_SESSION['uid']."', wh_sdate=now(), task_id='".$taskid."', updated=now(), created=now()";
		ExecuteQuery($sql);
	}
	function endWorkHours($taskid,$whid=''){
		mysql_query("SET NAMES utf8");
		
		if($whid!='')
			$sql="update work_hours set wh_edate=now(), task_id='".$taskid."', updated=now() where whid='".$whid."' and agent_id='".$_SESSION['uid']."'";
		else
			$sql="update work_hours set wh_edate=now(), updated=now() where task_id='".$taskid."' and  (wh_edate='0000-00-00 00:00:00' or wh_edate IS NULL) and agent_id='".$_SESSION['uid']."'";
		ExecuteQuery($sql);
	}
	// <--
	
	function updatePassword($newPass){
	mysql_query("SET NAMES utf8");
	$sql="update login set password='$newPass' where agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	
	function getReminders(){
	mysql_query("SET NAMES utf8");
	$date=date('Y-m-d');
	$sql="select * from contact con, company com where con.company_id=com.company_id and con.reminder_date>='$date' and con.status!='' order by con.reminder_date asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	// Projects -->
	function addProject($project_title, $company, $start_date, $end_date, $contact, $type, $project_status,$comment) {
	$sql="insert project set project_id='$project_id', project_title= '$project_title' , company_id='$company', start_date='$start_date', end_date= '$end_date', contact_id= '$contact', project_type_id='$type', project_status_id='$project_status', project_comments='$comment', date_created=NOW(),date_updated=NOW()";
	ExecuteQuery($sql);	
	}
	function assignStaffToTask($taskid, $agent_id) {
		$sql="insert project_entries_agents set entry_id='$taskid', agent_id= '$agent_id'";	
		ExecuteQuery($sql);	
	}
	function addProjectHeads($project_id, $agent_id) {
	$sql="insert project_agents set project_id='$project_id', agent_id= '$agent_id'";	
	ExecuteQuery($sql);	
	}
	function getProjectHeads($project_id) {
	$sql="select * from project_agents where project_id='$project_id'";	
	$allData=GetAllRows($sql);
	return $allData; }
	
	
	function getTaskAssignedStaff($taskid) {
	$sql="select pea.*, l.firstname,l.lastname from project_entries_agents pea, login l where pea.entry_id='$taskid' and pea.agent_id=l.agent_id order by firstname asc";	
	$allData=GetAllRows($sql);
	return $allData; 
	}
	
	function getOtherStaffsInvolved($project_id){
	$projhead_sql="select distinct l.agent_id as agent_id,l.firstname as firstname, l.lastname as lastname from project_agents pa, login l where pa.project_id='$project_id' and pa.agent_id=l.agent_id";
	$taskstaff_sql="select distinct l.agent_id as agent_id,l.firstname as firstname, l.lastname as lastname from project_entries pe, project_entries_agents pea, login l where pea.entry_id=pe.entry_id and pe.project_id='$project_id' and pea.agent_id=l.agent_id";
	
	$sql=$projhead_sql." UNION ".$taskstaff_sql." order by firstname asc";
	$allData=GetAllRows($sql);
	return $allData; 
	}
	
	function isMyTask($taskid,$agentid) {
	$sql="select * from project_entries_agents pea where pea.entry_id='".$taskid."' and pea.agent_id='".$agentid."'";	
	$data=GetSingleRow($sql);
	
	if(intval($data['agent_id'])>0)
		return true;
	else
		return false;
	}
	
	function selectAllProjects($orderby='') {
	mysql_query("SET NAMES utf8");
	if($orderby=='title')
		$sql="select * from project where project_status_id!=3 order by project_title asc";
	else
		$sql="select * from project where project_status_id!=3 order by project_id desc";
	$allData=GetAllRows($sql);
	return $allData; }
	
	function selectAllActiveProjects() {
	mysql_query("SET NAMES utf8");
	$sql="select * from project where project_status_id='1' or project_status_id='2' order by date_updated desc";
	$allData=GetAllRows($sql);
	return $allData; }
	
	function selectAllInactiveProjects() {
	mysql_query("SET NAMES utf8");
	$sql="select * from project where project_status_id='4' or project_status_id='5' or project_status_id='6' order by date_updated desc";
	$allData=GetAllRows($sql);
	return $allData; }
	
	function getProjectDetails($project_id) {
	$sql="select * from project where project_id='$project_id'";
	$data=GetSingleRow($sql);
	return $data; }
	
	function GettheStatus($statusid) {
	$sql="select project_status from project_status where project_status_id='$statusid'";
	$data=GetSingleRow($sql);
	return $data; }
	
	function GettheType($type_id) {
	$sql="select project_type from project_type where project_type_id='$type_id'";
	$data=GetSingleRow($sql);
	return $data; }
	
	function getprojectEntries ($project_id) { 
	$sql="select * from project_entries where project_id='$project_id' order by entry_id desc";
	$allData=GetAllRows($sql);
	return $allData; }
	
	
	function getprojectFiles($entry_id){
	$sql="select * from project_files where entry_id='$entry_id'";
	$data=GetSingleRow($sql);
	return $data; }
	
	function getEntryText ($entry_id) {
	$sql="select * from entry_text where entry_id='$entry_id'";
	$data=GetSingleRow($sql);
	return $data; }
	
	function addProjectEntry($project_id, $entry_id, $entry_type, $entry_title, $entry_status, $end_date, $userid) {
	$sql="insert project_entries set project_id='$project_id', entry_id= '$entry_id' , entry_type='$entry_type', entry_title='$entry_title', entry_status= '$entry_status', start_date= NOW(), end_date= '$end_date', created=NOW(),updated=NOW(), added_by='".$_SESSION['uid']."'";	
	ExecuteQuery($sql);	 }
	
	function addEntrytext($text, $entry_id) {
	$sql="insert entry_text set text= '$text', entry_id= '$entry_id'";	
	ExecuteQuery($sql);	 }
	
	function selectMaxEntryid ($project_id) {
	$sql= "SELECT MAX(entry_id) as mymax from project_entries";
	$data=GetSingleRow($sql);
	return $data; } 
	
	function addFiles ($file_name, $entry_id) {
	$sql="insert project_files set file_name= '$file_name', entry_id= '$entry_id'";	
	ExecuteQuery($sql);	 }

	
	function getEntryDetails($entry_id) {
	$sql="select pe.*, et.text from project_entries pe, entry_text et  where pe.entry_id='$entry_id' and pe.entry_id=et.entry_id";
	$data=GetSingleRow($sql);
	return $data; }
	
	function modifyEntry($entry_title, $entry_type, $text, $user, $entry_status, $start_date, $end_date, $entry_id) {
	$sql="update project_entries pe ,entry_text et set et.text='$text',pe.entry_title='$entry_title', pe.entry_type= '$entry_type', pe.agent_id= '$user', pe.entry_status= '$entry_status', pe.end_date= '$end_date', updated=NOW() where pe.entry_id='$entry_id' and pe.entry_id=et.entry_id";
	ExecuteQuery($sql); }	 
	
	function selectEntryStatus () {
	$sql="select * from entry_status";
	$allData=GetAllRows($sql);
	return $allData; }
	
	function selectAllTypes (){
	$sql="select * from project_type";
	$allData=GetAllRows($sql);
	return $allData;
	} 
	
	function selectAllStatus (){
	$sql="select * from project_status";
	$allData=GetAllRows($sql);
	return $allData; 
	}	
	
	function deleteProjectentry($entry_id){
	$sql="delete from project_entries where entry_id='$entry_id'";
	ExecuteQuery($sql); 
	deleteEntryText($entry_id);
	}
	function deleteTaskAssignedStaffs($taskid) {
	$sql="delete from project_entries_agents where entry_id='$taskid'";
	ExecuteQuery($sql); }
	function deleteProjectHeads($project_id) {
	$sql="delete from project_agents where project_id='$project_id'";
	ExecuteQuery($sql); }
	
	function deleteProjectDetails($project_id) {
	$sql="delete from project where project_id='$project_id'";
	ExecuteQuery($sql); 
	deleteProjectHeads($project_id);}
	
	function deleteProjectFiles($project_id) {
	$sql="delete from project_files where project_id='$project_id'";
	ExecuteQuery($sql); }
	
	function deleteProjectText($project_id) {
	$sql="delete from entry_text where project_id='$project_id'";
	ExecuteQuery($sql); }
	
	function deleteFileComment($uc_id) {
	$sql="delete from project_upfiles_comments where uc_id='$uc_id'";
	ExecuteQuery($sql); }
	
	function deleteEntryComment($ec_id) {
	$sql="delete from project_entries_comments where ec_id='$ec_id'";
	ExecuteQuery($sql); }
	
	function deleteEntryText($id) {
	$sql="delete from entry_text where entry_id='$id'";
	ExecuteQuery($sql); }

	function selectAllCompletedProject (){
	$sql="select * from project p, company c, project_status ps, project_type pt where  p.project_status_id=ps.project_status_id and p.company_id=c.company_id and p.project_type_id=pt.project_type_id and p.project_status_id=3 order by date_updated desc";
	$allData=GetAllRows($sql);
	return $allData; }
	
	function selectAllUncompletedProject (){
	$sql="select * from project p, company c, project_status ps, project_type pt where  p.project_status_id=ps.project_status_id and p.company_id=c.company_id and p.project_type_id=pt.project_type_id and p.project_status_id!=3 order by project_id desc";
	$allData=GetAllRows($sql);
	return $allData; }
	
	function modifyProject($project_title, $company, $type, $project_status, $start_date, $end_date, $contact, $comments, $project_id){
	$sql="update project set project_title='$project_title', company_id= '$company', project_type_id= '$type', project_status_id= '$project_status', start_date= '$start_date', end_date= '$end_date', contact_id= '$contact', project_comments='$comments', date_updated=NOW() where project_id= '$project_id'";
	ExecuteQuery($sql); }
	
	function addRenewalDateForProject($project_id, $renewal_date, $reminder_date=''){
	$sql="update project set renewal_date='$renewal_date', reminder_date= '$reminder_date' where project_id= '$project_id'";
	ExecuteQuery($sql);
	}
	
	function addRenewalDateLogForProject($project_id,$project_title,$project_status, $renewal_date, $reminder_date=''){
	$sql="insert project_renewal_log set project_id='$project_id', project_title= '$project_title', project_status='$project_status', renewal_date='$renewal_date',reminder_date='$reminder_date',added_by='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	
	}
	function addRenewalDateProjForCalendar($project_id, $renewal_date, $reminder_date=''){
	if($renewal_date!='' && $renewal_date!='0000-00-00' && $renewal_date!='0000-00-00 00:00:00'){
	$sql="insert event set project_id='$project_id', event_type='PR',event_date='$renewal_date',agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	}
	
	if($reminder_date!='' && $reminder_date!='0000-00-00' && $reminder_date!='0000-00-00 00:00:00'){
	$sql="insert event set project_id='$project_id', event_date='$reminder_date',event_type='PRR',agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	}
	
	}
	function deleteRenewalDateProjForCalendar($project_id){
	$sql="delete from event where project_id='$project_id' and (event_type='PR' or event_type='PRR')";
	ExecuteQuery($sql);
	
	if($reminder_date!='' && $reminder_date!='0000-00-00' && $reminder_date!='0000-00-00 00:00:00'){
	$sql="insert event set project_id='$project_id', event_date='$reminder_date',added_by='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	
	}
	}
	
	
	
	function Projects($project_id,$agent_id){
	$sql="update project_agents set project_read='1' where project_id= '$project_id' and agent_id='$agent_id'";
	ExecuteQuery($sql); }
	
	function ProjectRead($project_id,$agent_id){
	$sql="update project_agents set project_read='1' where project_id= '$project_id' and agent_id='$agent_id'";
	ExecuteQuery($sql); }
	
	function TaskRead($taskid,$agent_id){
	$sql="update project_entries_agents set project_read='1' where entry_id= '$taskid' and agent_id='$agent_id'";
	ExecuteQuery($sql); }
	
	function chkProjectRead($project_id,$agent_id){
	$sql="select project_read from project_agents where project_id= '$project_id' and agent_id='$agent_id'";
	$data=GetSingleRow($sql);
	return $data['project_read'];	
	}	
	
	function ProjectReread($project_id){
	$sql="update project_agents set project_read='0' where project_id='$project_id'";
	ExecuteQuery($sql); }
	
	function TaskReread($taskid){
	$sql="update project_entries_agents set project_read='0' where entry_id='$taskid'";
	ExecuteQuery($sql); }
	
	function getLastProject(){
	mysql_query("SET NAMES utf8");
	$sql="SELECT MAX(project_id) AS 'project_id' FROM project";
	$data=GetSingleRow($sql);
	return $data;	
	}
	function MyProjects($limit=''){
	if($_SESSION['uid']==1){
		if($limit=='')
			$sql="select * from project p, project_status ps where p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by date_updated desc";
		else
			$sql="select * from project p, project_status ps where p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by date_updated desc limit 0,".$limit;
	}else{
		if($limit=='')
			$sql="select * from project p, project_agents pa, project_status ps where p.project_id=pa.project_id and pa.agent_id='".$_SESSION['uid']."' and p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by date_updated desc";
		else
			$sql="select * from project p, project_agents pa, project_status ps where p.project_id=pa.project_id and pa.agent_id='".$_SESSION['uid']."' and p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by date_updated desc limit 0,".$limit;
	}
	$allData=GetAllRows($sql);
	return $allData; }
	
	function SortProjects($dis='all',$field='updated',$sorttype='desc',$limit=''){
	$orderfield=array('title'=>'p.project_title','company'=>'c.company_name','created'=>'p.date_created','due'=>'p.end_date','updated'=>'p.date_updated','status'=>'ps.project_status');
	
	if($limit==''){
		if($dis=='active')
			$sql="select p.*,ps.project_status,c.company_name from project p, project_status ps, company c where p.company_id=c.company_id and p.project_status_id=ps.project_status_id and (p.project_status_id='1' or p.project_status_id='2' or p.project_status_id='8' or p.project_status_id='7' or p.project_status_id='9' or p.project_status_id='10') order by ".$orderfield[$field]." ".$sorttype;
		else if($dis=='inactive')
			$sql="select p.*,ps.project_status,c.company_name from project p, project_status ps, company c where p.company_id=c.company_id and p.project_status_id=ps.project_status_id and (p.project_status_id='4' or p.project_status_id='5' or p.project_status_id='6') order by ".$orderfield[$field]." ".$sorttype;
		else if($dis=='complete')			
			$sql="select p.*,ps.project_status,c.company_name from project p, project_status ps, company c where p.company_id=c.company_id and p.project_status_id=ps.project_status_id and p.project_status_id=3 order by ".$orderfield[$field]." ".$sorttype;
		else
			$sql="select p.*,ps.project_status,c.company_name from project p, project_status ps, company c where p.company_id=c.company_id and p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by ".$orderfield[$field]." ".$sorttype;
			
	}else{
		$sql="select * from project p, project_agents pa, project_status ps where p.project_id=pa.project_id and pa.agent_id='".$_SESSION['uid']."' and p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
	}
	
	$allData=GetAllRows($sql);
	return $allData; }
	
	function SortMyProjects($field,$sorttype,$limit=''){
	$orderfield=array('end_date'=>'p.end_date','title'=>'p.project_title','updated'=>'p.date_updated','status'=>'ps.project_status');
	if($_SESSION['uid']==1){
		if($limit=='')
			$sql="select * from project p, project_status ps where p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by ".$orderfield[$field]." ".$sorttype;
		else
			$sql="select * from project p, project_status ps where p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
	}else{
		if($limit=='')
			$sql="select * from project p, project_agents pa, project_status ps where p.project_id=pa.project_id and pa.agent_id='".$_SESSION['uid']."' and p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by ".$orderfield[$field]." ".$sorttype;
		else
			$sql="select * from project p, project_agents pa, project_status ps where p.project_id=pa.project_id and pa.agent_id='".$_SESSION['uid']."' and p.project_status_id=ps.project_status_id and p.project_status_id!=3 order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
	}
	$allData=GetAllRows($sql);
	return $allData; }
	
	function ProjectArchive($project_id, $type='') {
	$ptype=array('complete'=>", p.project_status_id='3'",'cancel'=>", p.project_status_id='4'",'activate'=>", p.project_status_id='2'");
	
	$sql="update project p set date_updated=NOW()".$ptype[$type]." where p.project_id='$project_id'";
	ExecuteQuery($sql); }	
	
	function MyTasks(){//and pe.entry_status!='Completed'
	$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, project_entries_agents pea, entry_text et, company com where pe.project_id=p.project_id and pea.agent_id='".$_SESSION['uid']."' and pe.entry_id=pea.entry_id and pea.agent_id=a.agent_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and p.company_id=com.company_id order by pe.updated desc";
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function SortMyTasks($field,$sorttype){
	$orderfield=array('end_date'=>'pe.end_date','entry_title'=>'pe.entry_title','agent'=>'a.username','entry_status'=>'pe.entry_status','updated'=>'pe.updated','client'=>'com.company_name','proj'=>'p.project_title');//and pe.entry_status!='Completed'
	$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, project_entries_agents pea, entry_text et, company com where pe.project_id=p.project_id and pea.agent_id='".$_SESSION['uid']."' and pe.entry_id=pea.entry_id and pea.agent_id=a.agent_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and p.company_id=com.company_id order by ".$orderfield[$field]." ".$sorttype;
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function MyUncompletedTasks($limit=''){//and pe.entry_status!='Completed'
	if($limit=='')
		$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, project_entries_agents pea, entry_text et, company com where pe.project_id=p.project_id and pea.agent_id='".$_SESSION['uid']."' and pe.entry_id=pea.entry_id and pea.agent_id=a.agent_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and p.company_id=com.company_id and pe.entry_status!='Completed' order by pe.updated desc";
	else
		$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, project_entries_agents pea, entry_text et, company com where pe.project_id=p.project_id and pea.agent_id='".$_SESSION['uid']."' and pe.entry_id=pea.entry_id and pea.agent_id=a.agent_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and p.company_id=com.company_id and pe.entry_status!='Completed' order by pe.updated desc limit 0,".$limit;
		
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function SortMyUncompletedTasks($field,$sorttype,$limit=''){
	$orderfield=array('end_date'=>'pe.end_date','entry_title'=>'pe.entry_title','agent'=>'a.username','entry_status'=>'pe.entry_status','updated'=>'pe.updated','client'=>'com.company_name','proj'=>'p.project_title');//and pe.entry_status!='Completed'
	if($limit=='')
		$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, project_entries_agents pea, entry_text et, company com where pe.project_id=p.project_id and pea.agent_id='".$_SESSION['uid']."' and pe.entry_id=pea.entry_id and pea.agent_id=a.agent_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and p.company_id=com.company_id and pe.entry_status!='Completed'  order by ".$orderfield[$field]." ".$sorttype;
	else
		$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, project_entries_agents pea, entry_text et, company com where pe.project_id=p.project_id and pea.agent_id='".$_SESSION['uid']."' and pe.entry_id=pea.entry_id and pea.agent_id=a.agent_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and p.company_id=com.company_id and pe.entry_status!='Completed'  order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function AllTasks(){
	$sql="select p.*,pe.*,et.*,a.agent_id,a.username,com.company_name from login a, project p, project_entries pe, entry_text et, company com where p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.agent_id=a.agent_id and pe.entry_type='Task' and p.company_id=com.company_id order by pe.start_date asc";
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function AllUncompletedTasks($limit=''){
	//and pe.entry_status!='Completed'
	if($limit=='')
		$sql="select p.*,pe.*,et.*,com.company_name from project p, project_entries pe, entry_text et, company com where p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_status!='Completed' and pe.entry_type='Task' and p.company_id=com.company_id order by pe.created desc, pe.updated desc";
	else
		$sql="select p.*,pe.*,et.*,com.company_name from project p, project_entries pe, entry_text et, company com where p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_status!='Completed' and pe.entry_type='Task' and p.company_id=com.company_id order by pe.created desc, pe.updated desc limit 0,".$limit;
		
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function SortAllTasks($field,$sorttype){
	$orderfield=array('end_date'=>'pe.end_date','entry_title'=>'pe.entry_title','entry_status'=>'pe.entry_status','updated'=>'pe.updated'); //and pe.entry_status!='Completed'
	$sql="select p.*,pe.*,et.* from project p, project_entries pe, entry_text et where p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_type='Task'  order by ".$orderfield[$field]." ".$sorttype;
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function SortUncompletedTasks($field,$sorttype,$limit=''){
	$orderfield=array('end_date'=>'pe.end_date','entry_title'=>'pe.entry_title','entry_status'=>'pe.entry_status','updated'=>'pe.updated','client'=>'com.company_name','proj'=>'p.project_title'); //and pe.entry_status!='Completed'
	if($limit=='')
		$sql="select p.*,pe.*,et.*,com.company_name from project p, project_entries pe, entry_text et, company com where p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and pe.entry_status!='Completed' and p.company_id=com.company_id order by ".$orderfield[$field]." ".$sorttype;
	else
		$sql="select p.*,pe.*,et.*,com.company_name from project p, project_entries pe, entry_text et, company com where p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and pe.entry_status!='Completed' and p.company_id=com.company_id order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
		
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function SortUncompletedTasksByAgent($field,$sorttype,$limit=''){
	$orderfield=array('end_date'=>'pe.end_date','entry_title'=>'pe.entry_title','entry_status'=>'pe.entry_status','updated'=>'pe.updated','client'=>'com.company_name','proj'=>'p.project_title','agent'=>'l.firstname'); //and pe.entry_status!='Completed'
	if($limit=='')
		$sql="select p.*,pe.entry_id,pe.entry_type,pe.project_id,pe.entry_title,pe.entry_status,pe.created,pe.updated,pe.start_date,pe.end_date,pe.added_by,et.*,com.company_name, pea.agent_id from project p, project_entries pe, project_entries_agents pea, entry_text et, company com, login l where pea.entry_id=pe.entry_id and pea.agent_id=l.agent_id and p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and pe.entry_status!='Completed' and p.company_id=com.company_id order by ".$orderfield[$field]." ".$sorttype;
	else
		$sql="select p.*,pe.entry_id,pe.entry_type,pe.project_id,pe.entry_title,pe.entry_status,pe.created,pe.updated,pe.start_date,pe.end_date,pe.added_by,et.*,com.company_name, pea.agent_id from project p, project_entries pe, project_entries_agents pea, entry_text et, company com, login l where pea.entry_id=pe.entry_id and pea.agent_id=l.agent_id and p.project_id=pe.project_id and pe.entry_id=et.entry_id and pe.entry_type='Task' and pe.entry_status!='Completed' and p.company_id=com.company_id order by ".$orderfield[$field]." ".$sorttype.' limit 0,'.$limit;
		
	$allData=GetAllRows($sql);
	return $allData; 
	}
	function addProjectFile($project_id, $filename,$title,$desc,$type,$size) {
	$sql="insert project_upfiles set project_id='$project_id', project_upfile_name= '$filename', upfile_title='$title', upfile_desc='$desc',upfile_type='$type',upfile_created=NOW(),upfile_size='$size',added_by='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	
	function updateProjectFile($id, $title,$desc) {
	$sql="update project_upfiles set upfile_title='$title', upfile_desc='$desc' where project_upfile_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function addProjectAttachedFile($project_id, $filename) {
	$sql="insert project_upfiles set project_id='$project_id', project_upfile_name= '$filename'";	
	ExecuteQuery($sql);	
	}
	
	function getProjectAttachedFiles($project_id) {
	$sql="select * from project_upfiles where project_id='$project_id'";
	$allData=GetAllRows($sql);
	return $allData;	
	}
	
	function getAllProjectAttachedFiles($dtype='all') {
	if($dtype=='my')
		$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and added_by='".$_SESSION['uid']."' order by upfile_created desc";
	elseif(getAgentField($dtype,'username')!='')
		$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and added_by='".$dtype."' order by upfile_created desc";
	else
		$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id order by pu.upfile_created desc";
	$allData=GetAllRows($sql);
	return $allData;	
	}
	
	function sortAllProjectAttachedFiles($field,$sorttype='asc',$dtype='all') {
	$orderfield=array('desc'=>'pu.upfile_desc','title'=>'pu.upfile_title','created'=>'pu.upfile_created','addedby'=>'pu.added_by','filetype'=>'pu.upfile_type','proj'=>'p.project_title');
	
	if($dtype=='my')
		$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and added_by='".$_SESSION['uid']."' order by ".$orderfield[$field]." ".$sorttype;
	elseif(getAgentField($dtype,'username')!='')
		$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and added_by='".$dtype."' order by ".$orderfield[$field]." ".$sorttype;
	else
		$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id order by ".$orderfield[$field]." ".$sorttype;
	
	$allData=GetAllRows($sql);
	return $allData;	
	}
	
	
	function addFileComments($id,$comment) {
	$sql="insert project_upfiles_comments set uc_comment='$comment', uc_date=NOW(),project_upfile_id='$id', agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	function getFileCommentDetails($id) {
	$sql="select * from project_upfiles_comments where uc_id='$id'";	
	$data=GetSingleRow($sql);
	return $data;	
	}
	function addEntryComments($id,$comment) {
	$sql="insert project_entries_comments set ec_comment='$comment', ec_date=NOW(),entry_id='$id', agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	function getEntryCommentDetails($id) {
	$sql="select * from project_entries_comments where ec_id='$id'";	
	$data=GetSingleRow($sql);
	return $data;	
	}
	function getProjectComments($id) {
	$sql="select * from project_entries where entry_type='Comment' and project_id='$id' order by created desc";
	$allData=GetAllRows($sql);
	return $allData;	
	}
	function getProjectFileComments($id) {
	$sql="select * from project_upfiles_comments where project_upfile_id='$id' order by uc_date desc";
	$allData=GetAllRows($sql);
	return $allData;	
	}
	function getProjectEntryComments($id) {
	$sql="select * from project_entries_comments where entry_id='$id' order by ec_date desc";
	$allData=GetAllRows($sql);
	return $allData;	
	}
	function getRecentFiles($limit='') {
		if($limit==''){
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id order by upfile_created desc";
		}
		else {
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id order by upfile_created desc limit 0, ".$limit;
		}
		
		$allData=GetAllRows($sql);
		return $allData;	
	}
	function sortRecentFiles($field,$sorttype='asc',$limit='') {
		$orderfield=array('addedby'=>'pu.added_by','posted'=>'pu.upfile_created','title'=>'pu.upfile_title','project'=>'p.project_title');
		if($limit==''){
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id order by ".$orderfield[$field]." ".$sorttype;
		}
		else {
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id order by ".$orderfield[$field]." ".$sorttype." limit 0, ".$limit;
		}
		
		$allData=GetAllRows($sql);
		return $allData;	
	}
	function getMyRecentFiles($limit='') {
		if($limit==''){
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and pu.added_by='".$_SESSION['uid']."' order by upfile_created desc";
		}
		else {
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and pu.added_by='".$_SESSION['uid']."' order by upfile_created desc limit 0, $limit";
		}
		
		$allData=GetAllRows($sql);
		return $allData;	
	}
	function sortMyRecentFiles($field,$sorttype='asc',$limit='') {
		$orderfield=array('addedby'=>'pu.added_by','posted'=>'pu.upfile_created','title'=>'pu.upfile_title','project'=>'p.project_title');
		if($limit==''){
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and pu.added_by='".$_SESSION['uid']."' order by ".$orderfield[$field]." ".$sorttype;
		}
		else {
			$sql="select pu.*, p.project_title from project_upfiles pu, project p where pu.project_id=p.project_id and pu.added_by='".$_SESSION['uid']."' order by ".$orderfield[$field]." ".$sorttype." limit 0, ".$limit;
		}
		
		$allData=GetAllRows($sql);
		return $allData;	
	}
	function getProjectAttached($project_id,$entrytype='all') {
	$filesql="select f.project_upfile_id as id, f.upfile_type as filetype,'File' as type, f.upfile_title as title,f.upfile_desc as description, date_format(f.upfile_created,'%Y-%m-%d %H:%i:%s') as created, '<em>-none-</em>' as due, f.added_by as added_by, 0 as agent_id from project_upfiles f where f.project_id='$project_id'";
	$tasksql="select pe.entry_id as id,NULL as filetype,pe.entry_type as type, pe.entry_title as title, et.text as description, date_format(pe.start_date,'%Y-%m-%d %H:%i:%s') as created,date_format(pe.end_date,'%Y-%m-%d %H:%i:%s') as due, pe.added_by as added_by, pe.agent_id as agent_id from project_entries pe, entry_text et where pe.project_id='$project_id' and et.entry_id=pe.entry_id and pe.entry_type!='Comment'";
	
	if($entrytype=='all')
		$sql=$filesql." UNION ".$tasksql." order by created desc";
	elseif($entrytype=='file')
		$sql=$filesql." order by created desc";
	elseif($entrytype=='task')
		$sql=$tasksql." order by created desc";
	else return false;
	
	$allData=GetAllRows($sql);
	return $allData;	
	}
	
	function sortProjectAttached($project_id,$field,$sorttype='asc',$entrytype='all'){
	$orderfield=array('desc'=>'description','title'=>'title','created'=>'created','due'=>'due','staff'=>'agent_id','addedby'=>'added_by');
	
	if($orderfield[$field]!=''){
	$filesql="select f.project_upfile_id as id, f.upfile_type as filetype,'File' as type, f.upfile_title as title,f.upfile_desc as description, date_format(f.upfile_created,'%Y-%m-%d %H:%i:%s') as created, '<em>-none-</em>' as due, f.added_by as added_by, 0 as agent_id from project_upfiles f where f.project_id='$project_id'";
	$tasksql="select pe.entry_id as id,NULL as filetype,pe.entry_type as type, pe.entry_title as title, et.text as description, date_format(pe.start_date,'%Y-%m-%d %H:%i:%s') as created,date_format(pe.end_date,'%Y-%m-%d %H:%i:%s') as due, pe.added_by as added_by, pe.agent_id as agent_id from project_entries pe, entry_text et where pe.project_id='$project_id' and et.entry_id=pe.entry_id and pe.entry_type!='Comment'";
	
	if($entrytype=='all')
		$sql=$filesql." UNION ".$tasksql." order by ".$orderfield[$field]." ".$sorttype;
	elseif($entrytype=='file')
		$sql=$filesql." order by ".$orderfield[$field]." ".$sorttype;
	elseif($entrytype=='task')
		$sql=$tasksql." order by ".$orderfield[$field]." ".$sorttype;
	else return false;
	
	$allData=GetAllRows($sql);
	return $allData;	
	}else return false;
	}
	
	function getProjectAttachedFileDetails($id) {
	$sql="select * from project_upfiles where project_upfile_id='$id'";	
	$data=GetSingleRow($sql);
	return $data;	
	}
	
	function getProjectAttachedFileName($id) {
	$sql="select * from project_upfiles where project_upfile_id='$id'";
	$data=GetSingleRow($sql);
	return $data['project_upfile_name'];
	}
	
	function deleteProjectAttachedFile($id) {
	$sql="delete from project_upfiles where project_upfile_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function changeProjectStatus($statusid,$project_id) {
	$sql="update project set project_status_id='$statusid',date_updated=now() where project_id='$project_id'";
	ExecuteQuery($sql); }
	
	function changeEntryStatus($status,$entryid) {
	$sql="update project_entries set entry_status='$status',updated=now() where entry_id='$entryid'";
	ExecuteQuery($sql); }
	
	
	// <--
	
	//Notifications-->
	
	function getNotifications($type='all',$sorttype='desc',$limit=''){
	mysql_query("SET NAMES utf8");
	if($type=='unread'){
		if($limit!='')
			$sql="select * from notifications where agent_id='".$_SESSION['uid']."' and notify_hide=0 order by notify_datetime ".$sorttype." limit 0,".$limit;
		else
			$sql="select * from notifications where agent_id='".$_SESSION['uid']."' and notify_hide=0 order by notify_datetime ".$sorttype;
	}else{
		if($limit!='')
			$sql="select * from notifications where agent_id='".$_SESSION['uid']."' order by notify_datetime ".$sorttype." limit 0,".$limit;
		else
			$sql="select * from notifications where agent_id='".$_SESSION['uid']."' order by notify_datetime ".$sorttype;
	}
		//print $sql;
	$data=GetAllRows($sql);
	return $data;
	}
	
	/*function closeNotification($notify_id){
	$sql="update notifications set notify_hide='1' where notify_id= '$notify_id'";
	ExecuteQuery($sql); }
	*/
	
	function stickNotification($notify_id){
	$sql="update notifications set notify_up='1' where agent_id='".$_SESSION['uid']."' and notify_id= '$notify_id'";
	ExecuteQuery($sql); }
	
	function getunstickNotifications(){
	mysql_query("SET NAMES utf8");
	$sql="select * from notifications where agent_id='".$_SESSION['uid']."' and notify_hide=0 and notify_up=0 order by notify_datetime asc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function unstickAllNotification(){
	$sql="update notifications set notify_up='0' where agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql); }
	
	function addNotification($agent_id,$val,$type='normal'){	
	$typeval=array('normal','new_proj','mod_proj','new_task','mod_task','new_contact','new_file','mod_file','new_comment');
	if(!in_array($type,$typeval))
		$type='normal';
	if($type!='normal' && $type!='new_comment')
		$sql="insert notifications set agent_id='".$agent_id."', notify_nid='".$val."', notify_datetime=now(), notify_type='".$type."'";
	else
		$sql="insert notifications set agent_id='".$agent_id."', notify_text='".$val."', notify_datetime=now(), notify_type='".$type."'";
		
	ExecuteQuery($sql);
	}
	function deleteNotification($id){
	$sql="delete from notifications where agent_id='".$_SESSION['uid']."' and notify_id= '$id'";
	ExecuteQuery($sql);	
	}
	function deleteMyNotification(){
	$sql="delete from notifications where agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	
	}
	function closeNotification($id){
	$sql="update notifications set notify_hide=1 where agent_id='".$_SESSION['uid']."' and notify_id= '$id'";
	ExecuteQuery($sql);	
	}
	function getNotificationDtl($id) {
	$sql="select * from notifications where notify_id='$id'";
	$data=GetSingleRow($sql);
	return $data;
	}
	function getNotificationText($id){
		$notfy=getNotificationDtl($id);
		$type=$notfy['notify_type'];
		$val=($type=='normal' || $type=='new_comment')? $notfy['notify_text']:$notfy['notify_nid'];		
		$txt='';
		
		if($type=='new_proj'){
			$data=getProjectDetails($val);
			$txt='You have been assigned to a project, <a href="index.php?q=project/view/'.$val.'">'.trim($data['project_title']).'</a>.';
		}elseif($type=='mod_proj'){
			$data=getProjectDetails($val);
			$txt='The project, <a href="index.php?q=project/view/'.$val.'">'.trim($data['project_title']).'</a>, has been updated.';
		}elseif($type=='new_task'){
			$data=getEntryDetails($val);
			$txt='You have been assigned to a task, <a href="index.php?q=project/view/'.$data['project_id'].'/task/view/'.$val.'">'.trim($data['entry_title']).'</a>.';
		}elseif($type=='mod_task'){
			$data=getEntryDetails($val);
			$txt='The task, <a href="index.php?q=project/view/'.$data['project_id'].'/task/view/'.$val.'">'.trim($data['entry_title']).'</a>, has been updated.';
		}elseif($type=='new_contact'){
			$data=getContactDetails($val);
			$txt='<a href="index.php?q=contact/view/'.$val.'">'.trim($data['name']).'</a> is added to the contact list.';
		}elseif($type=='new_file'){
			$data=getProjectAttachedFileDetails($val);
			$projd=getProjectDetails($data['project_id']);
			$name=getAgentField($data['added_by'],'firstname').' '.getAgentField($data['added_by'],'lastname');
			$txt='<strong>'.trim($name).'</strong> added a ';
			$txt.='<a href="index.php?q=project/view/'.$data['project_id'].'/file/view/'.$val.'">file</a> on the ';
			$txt.='project, <a href="index.php?q=project/view/'.$data['project_id'].'">'.trim($projd['project_title']).'</a>.';
		}elseif($type=='mod_file'){
			$data=getProjectAttachedFileDetails($val);
			$txt='The file, <a href="index.php?q=project/view/'.$data['project_id'].'/file/view/'.$val.'">'.trim($data['upfile_title']).'</a>, has been updated.';
		}elseif($type=='new_comment'){
			$txt=$val;
		}else{
			$txt=$val;
		}
		
		return $txt;
	}
	
	// <--
	
	// EMAILS-->
	function getContactEmails($search){
	mysql_query("SET NAMES utf8");
	$sql="select email as 'value', fullname as 'name' from login where agent_id!=1 and (email LIKE '%".$search."%' or fullname LIKE '%".$search."%') LIMIT 0,20
	  UNION select value, name from contact c, contact_details cd where c.contact_id=cd.contact_id and cd.type='email' and (cd.value LIKE '%".$search."%' or c.name LIKE '%".$search."%') LIMIT 0,20";
	  
	$data=GetAllRows($sql);
	return $data;
	}
	
	// <--
	function getAllContactEmails(){
	mysql_query("SET NAMES utf8");
	$sql="select c.contact_id, c.name, cd.id, cd.value as 'email', com.company_name from contact c, contact_details cd, company com where c.contact_id=cd.contact_id and c.company_id=com.company_id and cd.type='email' order by company_name";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function getAllIssues(){
	mysql_query("SET NAMES utf8");
	$sql1="select 'T' as type, t.ticket_id as id, t.ticketID as tid, t.subject as subject, t.name as from_name, t.email as from_email, t.dept_id as deptID, d.dept_name as deptName, (DATEDIFF(t.created,'1970-01-01 00:00:00')*86400) as created, t.staff_id as agentID from ost_ticket t, ost_department d where t.dept_id=d.dept_id and t.status='open'";
	$sql2="select 'C' as type, ct.requestID as id, NULL as tid, ct.question as subject, ct.from_screen_name as from_name, NULL as from_email, ct.deptID as deptID, cd.name as deptName, ct.created as created, ct.userID as agentID from chatrequests ct, chatdepartments cd where ct.deptID!=0 and ct.deptID=cd.deptID";
	$sql3="select 'T' as type, t.ticket_id as id, t.ticketID as tid, t.subject as subject, t.name as from_name, t.email as from_email, t.dept_id as deptID, d.dept_name as deptName, (DATEDIFF(t.created,'1970-01-01 00:00:00')*86400) as created, t.staff_id as agentID from ost_ticket t, ost_department d where t.dept_id=d.dept_id and t.status='open' and t.staff_id='".getAgentField($_SESSION['uid'],'staff_id')."'";
	
	if($_SESSION['sadmin']) 
		$sql=$sql1." UNION ".$sql2." order by created desc";
	else
		$sql=$sql3." UNION ".$sql2." order by created desc";		
	
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortAllIssues($field,$sorttype){
	mysql_query("SET NAMES utf8");
	$orderfield=array('agent'=>'firstname','name'=>'from_name','dept'=>'deptName','subj'=>'subject','date'=>'created','type'=>'type');
	
	$sql1="select 'T' as type, t.ticket_id as id, t.ticketID as tid, t.subject as subject, t.name as from_name, t.email as from_email, t.dept_id as deptID, d.dept_name as deptName, (DATEDIFF(t.created,'1970-01-01 00:00:00')*86400) as created, t.staff_id as agentID, NULL as firstname from ost_ticket t, ost_department d where t.dept_id=d.dept_id and t.status='open' and t.staff_id=0";
	$sql2="select 'T' as type, t.ticket_id as id, t.ticketID as tid, t.subject as subject, t.name as from_name, t.email as from_email, t.dept_id as deptID, d.dept_name as deptName, (DATEDIFF(t.created,'1970-01-01 00:00:00')*86400) as created, t.staff_id as agentID, l.firstname as firstname  from ost_ticket t, ost_department d, login l where t.dept_id=d.dept_id and t.status='open' and t.staff_id!=0 and t.staff_id=l.staff_id";
	$sql3="select 'C' as type, ct.requestID as id, NULL as tid, ct.question as subject, ct.from_screen_name as from_name, NULL as from_email, ct.deptID as deptID, cd.name as deptName, ct.created as created, ct.userID as agentID, l.firstname as firstname from chatrequests ct, chatdepartments cd, login l where ct.deptID!=0 and ct.deptID=cd.deptID and ct.userID=l.userID";
	$sql4="select 'T' as type, t.ticket_id as id, t.ticketID as tid, t.subject as subject, t.name as from_name, t.email as from_email, t.dept_id as deptID, d.dept_name as deptName, (DATEDIFF(t.created,'1970-01-01 00:00:00')*86400) as created, t.staff_id as agentID, l.firstname as firstname  from ost_ticket t, ost_department d, login l where t.dept_id=d.dept_id and t.status='open' and t.staff_id='".getAgentField($_SESSION['uid'],'staff_id')."' and t.staff_id=l.staff_id";
	
	if($_SESSION['sadmin'])
		$sql=$sql1.' UNION '.$sql2.' UNION '.$sql3." order by ".$orderfield[$field]." $sorttype";
	else
		$sql=$sql4.' UNION '.$sql3." order by ".$orderfield[$field]." $sorttype";

	$data=GetAllRows($sql);
	return $data;
	}
	
	// ostickets-->
	function getAllTickets(){
	mysql_query("SET NAMES utf8");
	$sql="select t.*, d.dept_id, d.dept_name from ost_ticket t, ost_department d where t.dept_id=d.dept_id and t.status='open' order by t.created desc";
	  
	$data=GetAllRows($sql);
	return $data;
	}
	function getMyTickets(){
	mysql_query("SET NAMES utf8");
	$sql="select t.*, d.dept_id, d.dept_name from ost_ticket t, ost_department d where t.dept_id=d.dept_id and t.status='open' and t.staff_id='".getAgentFieldCustom('agent_id',$_SESSION['uid'],'staff_id')."' order by t.created desc";
	  
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortAllTickets($field,$sorttype){
	mysql_query("SET NAMES utf8");
	$orderfield=array('agent'=>'firstname','name'=>'name','dept'=>'dept_name','subj'=>'subject','date'=>'created');
	$sql="select t.*, d.dept_id, d.dept_name, NULL as firstname from ost_ticket t, ost_department d where t.dept_id=d.dept_id and t.status='open' and t.staff_id=0	UNION select t.*, d.dept_id, d.dept_name, l.firstname as firstname from ost_ticket t, ost_department d, login l where t.dept_id=d.dept_id and t.status='open' and t.staff_id!=0 and t.staff_id=l.staff_id	order by ".$orderfield[$field]." $sorttype";
	  
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortMyTickets($field,$sorttype){
	mysql_query("SET NAMES utf8");
	$orderfield=array('agent'=>'l.firstname','name'=>'t.name','dept'=>'d.dept_name','subj'=>'t.subject','date'=>'t.created');
	$sql="select t.*, d.dept_id, d.dept_name, l.firstname from ost_ticket t, ost_department d, login l where t.dept_id=d.dept_id and t.status='open' and t.staff_id='".getAgentFieldCustom('agent_id',$_SESSION['uid'],'staff_id')."' and t.staff_id=l.staff_id order by ".$orderfield[$field]." $sorttype";
	 
	$data=GetAllRows($sql);
	return $data;
	}
	
	//<--
	
	//date format converter -->
	function dateFormat($value,$format=1,$type='DT'){
		$datetime=explode(' ',$value);
		$time=explode(':',$datetime[1]);
		$date=explode('-',$datetime[0]);
		
		if($format==1){// Sun 18/02
			$dateformat=date("D j/m", mktime(0, 0, 0, $date[1], $date[2], $date[0]));
		}
		else if($format==2){
			if($type=='D' || $type=='DT'){
				$calculation = ((mktime (0,0,0,$date[1],$date[2],$date[0]) - mktime (0,0,0,date('m'),date('d'),date('Y')))/3600);
				$hours = $calculation;
				$days  = $hours/24;
				if($days>1)$dateformat="in ".$days." days";
				else if($days==1)$dateformat="Tomorrow";
				else if($days==0)$dateformat='Today';
				else if($days==-1) $dateformat="Yesterday";
				else {
					$num=floor(abs($days));
					$time1 = strtotime($value);
					$dateformats=dateAgo($time1);
					$dateformat=$dateformats['text'];
				}
			}
		}else if($format==3){// Feb 18, 1990
				$dateformat=date("M j, Y", mktime(0, 0, 0, $date[1], $date[2], $date[0]));
		}else if($format==4){
			if($time[0]!=0)
				$dateformat=date("l - F j, Y, g:ia", mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
			else
				$dateformat=date("l - F j, Y", mktime(0, 0, 0, $date[1], $date[2], $date[0]));
		}else if($format==5){
			$dateformat=date("D - M j, Y, g:ia", mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
		}else if($format==6){
			if($time[0]!=0)
				$dateformat=date("j M Y, g:ia", mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
			else
				$dateformat=date("j M Y", mktime(0, 0, 0, $date[1], $date[2], $date[0]));
		}else if($format==7){
			if($type=='D' || $type=='DT'){
				$calculation = ((mktime (0,0,0,$date[1],$date[2],$date[0]) - mktime (0,0,0,date('m'),date('d'),date('Y')))/3600);
				$hours = $calculation;
				$days  = $hours/24;
				if($days>1)$dateformat="in ".$days." days";
				else if($days==1)$dateformat="Tomorrow";
				else if($days==0){
					$now = strtotime(date('Y-m-d H:i:s'));
					$time1 = strtotime($value);
					//$dateformats=dateAgo($time1);
					$hours=($now-$time1)/3600;
					$dateformat=($datetime[1]!='')?hoursFormat($hours,1).' ago':'Today';
				}
				else if($days==-1) $dateformat="Yesterday, ".date("g:ia", mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
				else if($days>-8){
					$dateformat=date("D", mktime(0, 0, 0, $date[1], $date[2], $date[0])).' at '.date("g:ia", mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
				}else {
					$dateformat=date("j-M-y, D", mktime(0, 0, 0, $date[1], $date[2], $date[0]));
				}
			}
		}else if($format==8){// Feb 18, 1990
			$calculation = ((mktime (0,0,0,$date[1],$date[2],$date[0]) - mktime (0,0,0,date('m'),date('d'),date('Y')))/3600);
			$hours = $calculation;
			$days  = $hours/24;
			
			if($days==0)
				$dateformat='Today';
			elseif($days==-1)
				$dateformat='Yesterday';
			else
				$dateformat=date("M j, Y", mktime(0, 0, 0, $date[1], $date[2], $date[0]));
		}else if($format==9){// Time only
			$dateformat=date("g:ia", mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
		}
		
		return $dateformat;
	}
	function dateAgo($time1){
		$time2 = time();
		$diff = array('years' => 0, 'months' => 0, 'weeks' => 0, 'days' => 0, 'hours' => 0, 'minutes' => 0, 'seconds' => 0); 
		foreach(array('years','months','weeks','days','hours','minutes','seconds') as $unit) {
			while(true) {
				$next = strtotime("+1 $unit", $time1);
				if($next < $time2) {
					$time1 = $next;
					$diff[$unit]++;
				}
				else {
					break;
				}
			}
		}
		if ($diff['years']!=0)
			$dateformat=($diff['years'])." year".($diff['years']==1?'':'s')." ago";
		elseif($diff['months']!=0)
			$dateformat=($diff['months'])." month".($diff['months']==1?'':'s')." ago";
		elseif($diff['weeks']!=0)
			$dateformat=($diff['weeks'])." week".($diff['weeks']==1?'':'s')." ago";
		elseif($diff['days']!=0)
			$dateformat=($diff['days'])." day".($diff['days']==1?'':'s')." ago";
		elseif($diff['hours']!=0)
			$dateformat=($diff['hours'])." hour".($diff['hours']==1?'':'s')." ago";
		elseif($diff['minutes']!=0)
			$dateformat=($diff['minutes'])." min ago";
		elseif($diff['seconds']!=0)
			$dateformat=($diff['seconds'])." sec ago";
	
		return array('text'=>$dateformat,'datearray'=>$diff);
	}
	//<--
	
	// hours converter->

	function hoursFormat($hours,$type='default'){
		$format='';
		
		if($type==1){
			if($hours>=1){
				if(floor($hours)==1)
					$format='1 hour';
				else
					$format=floor($hours).' hours';
			}else{
				$min=($hours-floor($hours))*60;
				if(floor($min)<$min){
					$sec=($min-floor($min))*60;
					if($min>=1){
						if(floor($min)==1)
							$format='1 minute';
						else
							$format=(floor($min)).' minutes';
					}else{
						if(floor($sec)==1)
							$format='1 second';
						else
							$format=(($sec)).' seconds';
					}
				}else{
					$format=($min).' minute';
				}
			}
		}else{
			if($hours>=1){
				if(floor($hours)<$hours){
					$min=($hours-floor($hours))*60;
					if(floor($min)<$min){
						$sec=($min-floor($min))*60;
						$format=floor($hours).'h '.(floor($min)).'m '.(($sec)).'s';
					}else{
						$format=floor($hours).'h '.(($min)).'m';
					}
				}else{
					$format=($hours).' hr';
				}
			}else{
				$min=($hours-floor($hours))*60;
				if(floor($min)<$min){
					$sec=($min-floor($min))*60;
					if($min>=1){
						$format=(floor($min)).'m '.(($sec)).'s';
					}else{
						$format=(($sec)).'s';
					}
				}else{
					$format=($min).'m';
				}
			}
		}
		   
		return $format;
	} 
	//<--
	
	function _db_input($text){
		return $output="'".$text."'";
	}
	
	function selectLastRow($table,$id) {
	$sql= "SELECT MAX($id) as id from $table";
	$data=GetSingleRow($sql);
	return $data['id']; }
	
	function checkDuplicateUsername($username) {
	$sql= "SELECT * from login where username='".$username."'";
	$data=GetSingleRow($sql);
	return $data['agent_id']; }
	
	function checkAgentIdExists($id) {
	// and agent_id!=1
	$sql= "SELECT * from login where agent_id='".$id."'";
	$data=GetSingleRow($sql);
	return $data['agent_id']; }
	
	function isOnlineChat($id) {
	$sql= "SELECT * from login l, chat_admin c where l.agent_id='".$id."' and c.userID=l.userID";
	$data=GetSingleRow($sql);
	return $data['available_status'];}
	
	function getChatTranscripts(){
	$sql="select * from chattranscripts ct where ct.deptID!=0 order by created desc";
	$data=GetAllRows($sql);
	return $data;
	}
	function getChatRequests(){
	$sql="select ct.*, cd.name from chatrequests ct, chatdepartments cd where ct.deptID!=0 and ct.deptID=cd.deptID order by created desc";
	$data=GetAllRows($sql);
	return $data;
	}
	
	function sortChatRequests($field,$sorttype){
	$orderfield=array('agent'=>'l.firstname','dept'=>'cd.name','subj'=>'ct.question','date'=>'ct.created');
	$sql="select ct.*, cd.name,l.firstname  from chatrequests ct, chatdepartments cd, login l where ct.deptID!=0 and ct.deptID=cd.deptID and ct.userID=l.userID order by ".$orderfield[$field]." $sorttype";
	$data=GetAllRows($sql);
	return $data;
	}
	function getChatVisitors(){
	$sql="select cs.* from chatsessions cs, chatrequests cr where cs.sessionID=cr.sessionID and cr.deptID!=0 order by created desc";
	$data=GetAllRows($sql);
	return $data;
	}

	
	function addEvent($date,$type,$title,$desc,$cid){
	$sql="insert event set event_date='$date',contact_id='$cid',event_desc='$desc',event_title='$title',event_type='$type',agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);
	}
	
	function getMyEvents(){
	if($_SESSION['uid']){
		$sql="select * from event ev order by ev.event_date desc";
	}else{
		$sql="select * from event ev where ev.agent_id='".$_SESSION['uid']."' order by ev.event_date desc";
	}
		$data=GetAllRows($sql);
	return $data;
	}
	
	function deleteEvent($id){
	$sql="delete from event where event_id='$id'";
	ExecuteQuery($sql);	
	}
	
	function getFileTypeImage($type,$size=16){
		//$fext = strtolower(substr(strrchr($fname,"."),1));
		$filetypeimgs=array(
		'text/plain'=>array('image'=>'txt_file.png','title'=>'Text'),
		'text/css'=>array('image'=>'css_file.png','title'=>'CSS'),
		'text/html'=>array('image'=>'html_file.png','title'=>'HTML'),
		'text/js'=>array('image'=>'js_file.png','title'=>'JS'),
		//'application/octet-stream'=>'psd_file.png',
		'application/zip'=>array('image'=>'zip_file.png','title'=>'ZIP'),
		'application/pdf'=>array('image'=>'pdf_file.png','title'=>'PDF'),
		'application/msword'=>array('image'=>'doc.png','title'=>'MS Word'),
		'application/vnd.ms-excel'=>array('image'=>'file_excel.png','title'=>'MS Excel'),
		'application/vnd.ms-powerpoint'=>array('image'=>'ppt_file.png','title'=>'MS Powerpoint'),
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=>array('image'=>'docx.png','title'=>'MS Word'),
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'=>array('image'=>'xlsx.png','title'=>'MS Excel'),
		'image/gif'=>array('image'=>'gif_file.png','title'=>'GIF'),
		'image/bmp'=>array('image'=>'bmp_file.png','title'=>'BMP'),
		'image/jpeg'=>array('image'=>'jpg_file.png','title'=>'JPEG'),
		'audio/mpeg'=>array('image'=>'mp3_file.png','title'=>'MP3'),
		'audio/x-wav'=>array('image'=>'wav_file.png','title'=>'WAV')
		//'video/mpeg'=>'mpg_file.png',
		//'video/quicktime'=>'mov_file.png',
		//'video/x-msvideo'=>'avi_file.png'
		);
		if($filetypeimgs[$type]){
			$a=$filetypeimgs[$type];
			$typeimg=$a['image'];
			$title=' title="'.$a['title'].'"';
		}else{
			$typeimg='default.png';
			$title='';
		}
		
        return '<img src="img/filetype/'.$typeimg.'" height="'.$size.'" width="'.$size.'"'.$title.'>';
	}
	
	function getFileTypeImageName($type){
		//$fext = strtolower(substr(strrchr($fname,"."),1));
		$filetypeimgs=array(
		'text/plain'=>array('image'=>'txt_file.png','title'=>'Text'),
		'text/css'=>array('image'=>'css_file.png','title'=>'CSS'),
		'text/html'=>array('image'=>'html_file.png','title'=>'HTML'),
		'text/js'=>array('image'=>'js_file.png','title'=>'JS'),
		//'application/octet-stream'=>'psd_file.png',
		'application/zip'=>array('image'=>'zip_file.png','title'=>'ZIP'),
		'application/pdf'=>array('image'=>'pdf_file.png','title'=>'PDF'),
		'application/msword'=>array('image'=>'doc.png','title'=>'MS Word'),
		'application/vnd.ms-excel'=>array('image'=>'file_excel.png','title'=>'MS Excel'),
		'application/vnd.ms-powerpoint'=>array('image'=>'ppt_file.png','title'=>'MS Powerpoint'),
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=>array('image'=>'docx.png','title'=>'MS Word'),
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'=>array('image'=>'xlsx.png','title'=>'MS Excel'),
		'image/gif'=>array('image'=>'gif_file.png','title'=>'GIF'),
		'image/bmp'=>array('image'=>'bmp_file.png','title'=>'BMP'),
		'image/jpeg'=>array('image'=>'jpg_file.png','title'=>'JPEG'),
		'audio/mpeg'=>array('image'=>'mp3_file.png','title'=>'MP3'),
		'audio/x-wav'=>array('image'=>'wav_file.png','title'=>'WAV')
		//'video/mpeg'=>'mpg_file.png',
		//'video/quicktime'=>'mov_file.png',
		//'video/x-msvideo'=>'avi_file.png'
		);
		if($filetypeimgs[$type]){
			$a=$filetypeimgs[$type];
			$typeimg=$a['image'];
			$title=$a['title'];
		}else{
			$typeimg='default.png';
			$title='';
		}
		
        return array('filename'=>$typeimg,'title'=>$title);
	}
	
//ROW COLOR-->
	function getRowColor(){
	mysql_query("SET NAMES utf8");
	$sql="select * from row_color where agent_id='".$_SESSION['uid']."'";
	$data=GetSingleRow($sql);
	return $data; }
	
	function updateRowColor($type,$interval,$val){
	//where agent_id='".$_SESSION['uid']."'
	$sql="update row_color set color_type='$type', color_interval='$interval', color_val='$val'";
	ExecuteQuery($sql); }
	
	function addRowColor($type,$interval,$val){
	$sql="insert row_color set color_type='$type', color_interval='$interval', color_val='$val', agent_id='".$_SESSION['uid']."'";
	ExecuteQuery($sql);	}
	
	function randomColor(){
	$rand = mt_rand(0x000000, 0xffffff); // generate a random number between 0 and 0xffffff
	$rand = dechex($rand & 0xffffff); // make sure we're not over 0xffffff, which shouldn't happen anyway
	$rand = str_pad($rand, 6, '0', STR_PAD_LEFT); // add zeroes in front of the generated string
	
	return '#'.$rand;
	}
//<--

	function hasAccess($type,$id){
		if(!$_SESSION['sadmin']){
			if($type=='project'){
				$users=getProjectHeads($id);
				foreach ($users as $data) {
					if($_SESSION['uid']==$data['agent_id']){
						return true;
					}
				}
			}
			elseif($type=='project-comment'){
				$entry=getEntryDetails($id);
				$users=getProjectHeads($entry['project_id']);
				
				if($_SESSION['uid']==$entry['added_by'])
					return true;
				
				foreach ($users as $data) {
					if($_SESSION['uid']==$data['agent_id']){
						return true;
					}
				}
			}
			elseif($type=='project-task'){
				$entry=getEntryDetails($id);
				$users=getProjectHeads($entry['project_id']);
				//$asusers=getTaskAssignedStaff($id);
				
				if($_SESSION['uid']==$entry['added_by'])
					return true;
				
				foreach ($users as $data) {
					if($_SESSION['uid']==$data['agent_id']){
						return true;
					}
				}
				
				/*foreach ($asusers as $data) {
					if($_SESSION['uid']==$data['agent_id']){
						return true;
					}
				}*/
			}
			elseif($type=='project-task-comment'){
				$ecomment=getEntryCommentDetails($id);
				
				if($_SESSION['uid']==$ecomment['agent_id'])
					return true;
				
			}
			elseif($type=='project-file'){
				$file=getProjectAttachedFileDetails($id);
				$users=getProjectHeads($file['project_id']);
				
				if($_SESSION['uid']==$file['added_by'])
					return true;
				
				foreach ($users as $data) {
					if($_SESSION['uid']==$data['agent_id']){
						return true;
					}
				}
			}
			elseif($type=='project-file-comment'){
				$filecomment=getFileCommentDetails($id);
				$file=getProjectAttachedFileDetails($filecomment['project_upfile_id']);
				$users=getProjectHeads($file['project_id']);
				
				if($_SESSION['uid']==$filecomment['agent_id'])
					return true;
					
				if($_SESSION['uid']==$file['added_by'])
					return true;
				
				foreach ($users as $data) {
					if($_SESSION['uid']==$data['agent_id']){
						return true;
					}
				}
			}
		}
		else{
			return true;
		}
	
		return false;
	}
	
	function chkProjectDeadline($days=0){
	$sql="SELECT p.*, ps.project_status from project p, project_status ps where (p.project_status_id=1 or p.project_status_id=2) and p.project_status_id=ps.project_status_id and datediff(p.end_date,NOW())=".$days;
	$data=GetAllRows($sql);
	return $data;
	}
	
	function chkTaskDeadline($days=0){
	$sql="SELECT pe.*,p.project_title,et.text from project_entries pe, project p, entry_text et where pe.project_id=p.project_id and (pe.entry_status='Not Started' or pe.entry_status='In Progress') and pe.entry_type='Task' and pe.entry_id=et.entry_id and datediff(pe.end_date,NOW())=".$days;
	$data=GetAllRows($sql);
	return $data;
	}
	function WordLimiter($text,$limit=15){
		$explode = explode(' ',$text);
		$string  = '';
		   
		$dots = '...';
		if(count($explode) <= $limit){
			$dots = '';
		}
		for($i=0;$i<$limit;$i++){
			if($i) $string.=" ";
			$string .= $explode[$i];
		}
		   
		return $string.$dots;
	}

	function search($query,$type='all',$limit=0,$start=0){
	$sql_proj="select p.project_title as title, p.project_comments as description, p.date_updated as updated, concat('index.php?q=project/view/',p.project_id) as link 
	from project p where 
	(p.project_title LIKE '%".$query."%' or p.project_comments LIKE '%".$query."%') ";
	
	$sql_task="select pe.entry_title as title, et.text as description, pe.updated as updated, concat('index.php?q=project/view/',pe.project_id,'/task/view/',pe.entry_id) as link 
	from project_entries pe, entry_text et where pe.entry_id=et.entry_id and pe.entry_type='Task' and
	(pe.entry_title LIKE '%".$query."%' or et.text LIKE '%".$query."%') ";
	
	$sql_contact="select c.name as title, concat(com.company_name) as description, NULL as updated, concat('index.php?q=contact/view/',c.contact_id) as link 
	from contact c, company com where c.company_id=com.company_id and
	(c.name LIKE '%".$query."%' or com.company_name LIKE '%".$query."%') ";
	
	if($type=='proj'){
		if($limit!=0)
			$sql=$sql_proj.' order by updated desc LIMIT '.$start.','.$limit;
		else
			$sql=$sql_proj;
	}elseif($type=='task'){
		if($limit!=0)
			$sql=$sql_task.' order by updated desc LIMIT '.$start.','.$limit;
		else
			$sql=$sql_task;
	}elseif($type=='contact'){
		if($limit!=0)
			$sql=$sql_contact.' order by updated desc LIMIT '.$start.','.$limit;
		else
			$sql=$sql_contact;
	}else{
		if($limit!=0)
			$sql=$sql_proj.' UNION '.$sql_task.' UNION '.$sql_contact.' order by updated desc LIMIT '.$start.','.$limit;
		else
			$sql=$sql_proj.' UNION '.$sql_task.' UNION '.$sql_contact;
	}	
	
	$data=GetAllRows($sql);
	return $data;
	}
	
	function ext_str_ireplace($findme, $replacewith, $subject)	{
		 // Replaces $findme in $subject with $replacewith
		 // Ignores the case and do keep the original capitalization by using $1 in $replacewith
		 // Required: PHP 5
	
		 $rest = $subject;
		 $result = '';
	
		 while (stripos($rest, $findme) !== false) {
			  $pos = stripos($rest, $findme);
	
			  // Remove the wanted string from $rest and append it to $result
			  $result .= substr($rest, 0, $pos);
			  $rest = substr($rest, $pos, strlen($rest)-$pos);
	
			  // Remove the wanted string from $rest and place it correctly into $result
			  $result .= str_replace('$1', substr($rest, 0, strlen($findme)), $replacewith);
			  $rest = substr($rest, strlen($findme), strlen($rest)-strlen($findme));
		 }
	
		 // After the last match, append the rest
		 $result .= $rest;
	
		 return $result;
	}

	?>