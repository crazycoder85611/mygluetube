$(function() {
    token = $("meta[name='csrf-token']").attr('content');

    $(window).resize(function(){
        sidebar();
    });

    sidebar();
    hideAlert();
    selectProject();
    backPageProjects();

    $('#final_delivery').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.delivery_date').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd'
    });

    loadInfo();
    loadContactDetails();
    backToContacts();
    addWatch();

    // show messaage "Success" 
    var sess = '<%= Session["message"] %>';
    if( sess !== 'default')
    {
        $(".alert-success").css("margin-top","55px");
        $(".alert-success").css("text-align","center");        
    }
});

function sidebar() {
    if ($('.sidebar').length) {
        var body_height = $('body').height();
        var buttons_height = $('.sidebar_buttons').height();
        $('.sidebar ul').css('height', body_height - buttons_height);
    }
}

window.token = '';

var hideAlertSec = 1000;

function hideAlert() {
    return;
    if ($('.alert').is(':visible')) {
        setTimeout(function() {
            $('.alert').fadeOut(500);
            $('.alert').remove();
        }, hideAlertSec)
    }
}

function selectProject() {
    $(document).on('change', '.project_checkbox', function() {

        if ($('.project_checkbox:checked').length > 0) {
            $('.project_checkbox').prop('checked', false);
            $(this).prop('checked', true);
            changeUrl($(this).val());
            $('.select_project').removeClass('disabled');
        } else {
            $('.project_checkbox').removeAttr('disabled');
            $('.select_project').addClass('disabled');
        }
    });
}

function backPageProjects() {
    if ($('.project_checkbox:checked').length > 0) {
        changeUrl($('.project_checkbox:checked').val());
        $('.select_project').removeClass('disabled');
    }
}

function changeUrl(id) {
    var links = $('.select_project');
    var countLinks = $('.select_project').length;

    for (var i = 0; i < countLinks; i++) {
        var href = links.eq(i).attr('href');

        if (href) {
            var href = href.split('&');
            var newHref = href[0];

            if (href.length == 2) {
                newHref = href[0] + '&' + href[1];
            }

            links.eq(i).attr('href', newHref + '&service_id=' + id);
        }
    }
}

function loadInfo() {
    $(document).on('click', '.info_click', function() {
        var current = $(this);
        var href = current.attr('href');

        $(".info_click").removeClass('active');
        $(this).addClass("active");

        $.post(href, {_token: token}, function(json) {
            var block = $('.info_block');
            block.empty();

            if (json.html) {
                block.append(json.html);
            }

            $('.back_contacts').remove();

            var id = current.attr('data-id');

            if (current.attr('data-type') == 'companies' || current.attr('data-type') == 'group_company') {
                history.pushState('', '', '?company_id=' + id);
            } else {
                history.pushState('', '', '?client_id=' + id);
            }
        }, 'json');

        return false;
    });
}

function loadContactDetails() {
    $(document).on('click', '.load_contact_details', function() {
        var current = $(this);
        var href = current.attr('href');
        $.post(href, {_token: token}, function(json) {

            if (json.html) {
                $('.details_show').hide();
                $('.info_block').append(json.html);
            }

            history.pushState('', '', location.search + '&client_id=' + current.attr('data-id'));
        }, 'json');

        return false;
    });
}

function backToContacts() {
    $(document).on('click', '.back_contacts', function() {
        $('#contact_details_block').remove();
        $('.details_show').show();
        history.back();
    });
}

function addWatch() {
    $(document).on('click', '.add_watch', function() {
        var current = $(this);
        var href = current.attr('href');
        $.post(href, {_token: token}, function(json) {
            var html = '<div class="alert alert-success">' + json.messages + '</div>';
            $('#contact_details_block').prepend(html);
        }, 'json');

        return false;
    });
}

